﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using HoloGen.IO.Convertors.Import;
using HoloGen.UI.Utils;
using HoloGen.Utils;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;

namespace HoloGen.UI.Menus.Commands
{
    /// <summary>
    /// HoloGen menu command
    /// </summary>
    public class ImportMatFileCommand : AbstractImportCommand
    {
        public override string Name => Resources.Properties.Resources.ImportMatFileCommand_ImportCarpenterMatFile;

        public override string ToolTip => Resources.Properties.Resources.ImportMatFileCommand_ImportAMatFileFormattedForJoelCarpenterSProgramMaintainedByGeorgeGorden;
        
        private static int _currentFileIndex = 1;
        private static int _currentTargetIndex = 1;
        private static int _currentIlluminationIndex = 1;
        private static int _currentRegionIndex = 1;
        private static int _currentInitialIndex = 1;

        public override void Execute(object parameter)
        {
            _currentTargetIndex = 1;
            _currentIlluminationIndex = 1;
            _currentRegionIndex = 1;
            _currentInitialIndex = 1;

            var dialog = new OpenFileDialog
            {
                Filter = FileTypes.MatlabFileFilter + "|" +
                         FileTypes.AllFileFilter
            };
            Debug.Assert(FileUtils.LastFileDialogLocation.Directory != null, "FileUtils.LastFileDialogLocation.Directory != null");
            dialog.InitialDirectory = FileUtils.LastFileDialogLocation.Directory.FullName;

            var result = dialog.ShowDialog();
            if (result.HasValue && result.Value)
            {
                var file = new FileInfo(dialog.FileName);
                if (file.Exists)
                {
                    if (file.Length > 5000000) // 5 MB
                    {
                        var setting = Context.ApplicationSettings.GeneralSettingsPage.WarningSettingsFolder.ShowLargeMatImportWarning;

                        if (setting.Value)
                        {
                            // Warning: Not async coz I'm to lazy to implement a ddelegate command structure
                            var cont = ((MetroWindow) Application.Current.MainWindow).ShowModalMessageExternal(
                                Resources.Properties.Resources.AbstractSaveAsFileCommand_Execute_Warning,
                                Resources.Properties.Resources.ImportMatFileCommand_Execute_WarningHoloGenUsesExternalLibrariesForImportingMatFilesTheseCanBeVerySlowForLargeFilesWouldYouLikeToContinue,
                                MessageDialogStyle.AffirmativeAndNegativeAndSingleAuxiliary,
                                new MetroDialogSettings
                                {
                                    FirstAuxiliaryButtonText = Resources.Properties.Resources.ImportMatFileCommand_Execute_DonTShowAgain
                                });
                            if (cont == MessageDialogResult.Negative)
                                return;
                            if (cont == MessageDialogResult.FirstAuxiliary) setting.Value = false;
                        }
                    }

                    using (new WaitCursor())
                    {
                        var task = Task.Run(() =>
                        {
                            var timporter = new MatImporter();
                            var terror = timporter.Import(file);
                            return new Tuple<string, MatImporter>(terror, timporter);
                        });

                        task.Wait();

                        var res = task.Result;
                        var error = res.Item1;
                        var importer = res.Item2;

                        if (error == null || error != "")
                        {
                            ((MetroWindow) Application.Current.MainWindow).ShowMessageAsync(
                                Resources.Properties.Resources.AbstractSaveAsFileCommand_Execute_Error,
                                error);
                        }
                        else
                        {
                            string baseName = file.Directory + "\\Imported_" + _currentFileIndex++.ToString("D3") + "_";
                            foreach (var img in importer.TargetImages) 
                                Open(img, new FileInfo(baseName + "Target_" + _currentTargetIndex++.ToString("D3") + ".hfimg"), false);
                            foreach (var img in importer.IlluminationImages) 
                                Open(img, new FileInfo(baseName + "Illumination_" + (_currentIlluminationIndex++).ToString("D3") + ".hfimg"), false);
                            foreach (var img in importer.RegionImages) 
                                Open(img, new FileInfo(baseName + "Region_" + (_currentRegionIndex++).ToString("D3") + ".hfimg"), false);
                            foreach (var img in importer.InitialImages) 
                                Open(img, new FileInfo(baseName + "Initial_" + (_currentInitialIndex++).ToString("D3") + ".hfimg"), false);
                            
                            FileUtils.LastFileDialogLocation = file;
                        }
                    }
                }
                else
                {
                    ((MetroWindow) Application.Current.MainWindow).ShowMessageAsync(
                        Resources.Properties.Resources.AbstractSaveAsFileCommand_Execute_Error,
                        Resources.Properties.Resources.AbstractOpenFileCommand_Execute_FailedToLoadTheFile);
                }
            }
        }
    }
}