﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

using HoloGen.Hierarchy.CommandTypes;

namespace HoloGen.UI.Menus.Commands
{
    /// <summary>
    /// HoloGen menu command
    /// </summary>
    public abstract class AbstractMenuCommand : Command<IMenuWindow>
    {
        private readonly IMenuWindow _oldContext;

        protected AbstractMenuCommand()
        {
            PropertyChanged += Context_PropertyChanged;
            if (Context != null)
            {
                Context.PropertyChanged += Selected_PropertyChanged;
                _oldContext = Context;
            }

            FireCanExecuteChanged();
        }

        protected AbstractMenuCommand(IMenuWindow context) : base(context)
        {
            PropertyChanged += Context_PropertyChanged;
            if (Context != null)
            {
                Context.PropertyChanged += Selected_PropertyChanged;
                _oldContext = Context;
            }

            FireCanExecuteChanged();
        }

        private void Selected_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(IMenuWindow.Selected)) FireCanExecuteChanged();
        }

        private void Context_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(Context))
            {
                if (_oldContext != null)
                    _oldContext.PropertyChanged -= Selected_PropertyChanged;
                Context.PropertyChanged += Selected_PropertyChanged;
                FireCanExecuteChanged();
            }
        }
    }
}