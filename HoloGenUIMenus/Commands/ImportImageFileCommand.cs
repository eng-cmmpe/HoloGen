﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Numerics;
using System.Windows;
using HoloGen.Image;
using HoloGen.UI.Utils;
using HoloGen.Utils;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;

namespace HoloGen.UI.Menus.Commands
{
    /// <summary>
    /// HoloGen menu command
    /// </summary>
    public class ImportImageFileCommand : AbstractImportCommand
    {
        public override string Name => Resources.Properties.Resources.ImportImageFileCommand_ImportFromImageFile;

        public override string ToolTip => Resources.Properties.Resources.ImportImageFileCommand_ImportAnImageFileAndConvertItToAHoloGenFormatTheImageWillBeInternallyConvertedToBitmapBeforeTheSumOfTheRGBValuesAreUsedAsTheRealPartsOfTheCreatedHoloGenComplexImage;
        
        private static int _currentFileIndex = 1;

        public override void Execute(object parameter)
        {
            var dialog = new OpenFileDialog
            {
                Filter = FileTypes.AllImagesFileFilter + "|" +
                         FileTypes.BmpFileFilter + "|" +
                         FileTypes.GifFileFilter + "|" +
                         FileTypes.JpgFileFilter + "|" +
                         FileTypes.PngFileFilter + "|" +
                         FileTypes.TffFileFilter + "|" +
                         FileTypes.AllFileFilter
            };
            Debug.Assert(FileUtils.LastFileDialogLocation.Directory != null, "FileUtils.LastFileDialogLocation.Directory != null");
            dialog.InitialDirectory = FileUtils.LastFileDialogLocation.Directory.FullName;

            var result = dialog.ShowDialog();
            if (result.HasValue && result.Value)
            {
                var file = new FileInfo(dialog.FileName);
                if (file.Exists)
                    using (new WaitCursor())
                    {
                        Bitmap bitmap;
                        try
                        {
                            var image = System.Drawing.Image.FromFile(file.FullName);
                            bitmap = new Bitmap(image);
                        }
                        catch (Exception)
                        {
                            ((MetroWindow) Application.Current.MainWindow).ShowMessageAsync(
                                Resources.Properties.Resources.AbstractSaveAsFileCommand_Execute_Error,
                                Resources.Properties.Resources.ImportImageFileCommand_Execute_ErrorWindowsFailedToParseTheImageContents);
                            return;
                        }

                        var size = bitmap.Size;

                        var bitmapData = bitmap.LockBits(
                            new Rectangle(0, 0, size.Width, size.Height),
                            ImageLockMode.ReadWrite,
                            PixelFormat.Format24bppRgb
                        );
                        var bitmapBytes = new byte[Math.Abs(bitmapData.Stride) * bitmapData.Height];
                        System.Runtime.InteropServices.Marshal.Copy(bitmapData.Scan0, bitmapBytes, 0, Math.Abs(bitmapData.Stride) * bitmapData.Height);

                        var values = new Complex[size.Height, size.Width];

                        long location = 0;
                        for (var y = 0; y < size.Height; y++)
                        for (var x = 0; x < size.Width; x++)
                        {
                            // Values from https://en.wikipedia.org/wiki/Grayscale
                            // WARNING: GDI+ Uses the order blue, green, red!
                            var norm = (bitmapBytes[location + 2] * 30 + bitmapBytes[location + 1] * 59 + bitmapBytes[location] * 11) / 100.0;
                            values[y, x] = new Complex(norm, 0);

                            location += 3;
                        }

                        bitmap.UnlockBits(bitmapData);
                        Open(
                            new ComplexImage(values), 
                            new FileInfo(file.Directory + "\\Imported_" + (_currentFileIndex++).ToString("D3") + ".hfimg"));

                        FileUtils.LastFileDialogLocation = file;
                    }
                else
                    ((MetroWindow) Application.Current.MainWindow).ShowMessageAsync(
                        Resources.Properties.Resources.AbstractSaveAsFileCommand_Execute_Error,
                        Resources.Properties.Resources.AbstractOpenFileCommand_Execute_FailedToLoadTheFile);
            }
        }
    }
}