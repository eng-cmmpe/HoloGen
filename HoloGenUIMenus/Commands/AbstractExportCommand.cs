﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

using System.Diagnostics;
using System.IO;
using System.Windows;
using HoloGen.UI.Utils;
using HoloGen.Utils;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;

namespace HoloGen.UI.Menus.Commands
{
    /// <summary>
    /// HoloGen menu command
    /// </summary>
    public abstract class AbstractExportCommand<T> : AbstractMenuCommand
        where T : class, ICanExport, ITabViewModel
    {
        protected AbstractExportCommand(IMenuWindow context) : base(context)
        {
        }

        protected AbstractExportCommand()
        {
        }

        public abstract string Filter(T tab);
        public abstract string Export(FileInfo file, T tab);

        public override void Execute(object parameter)
        {
            var dialog = new SaveFileDialog();

            var mainWindowViewModel = TabHandlerFramework.GetActiveHandler();

            var tab = mainWindowViewModel.Selected as T;
            Debug.Assert(tab != null);

            if (tab is ICanSave save)
            {
                if (save.FileLocation.Exists)
                {
                    Debug.Assert(save.FileLocation.Directory != null, "save.FileLocation.Directory != null");
                    dialog.InitialDirectory = save.FileLocation.Directory.FullName;
                    dialog.FileName = Path.GetFileNameWithoutExtension(save.FileLocation.Name);
                }
                else if (FileUtils.LastFileDialogLocation.Directory != null && FileUtils.LastFileDialogLocation.Directory.Exists)
                {
                    dialog.InitialDirectory = FileUtils.LastFileDialogLocation.FullName;
                }
            }

            dialog.OverwritePrompt = false;

            dialog.Filter = Filter(tab);

            var result = dialog.ShowDialog();

            if (result.HasValue && result.Value)
            {
                var file = new FileInfo(dialog.FileName);

                if (file.Exists)
                {
                    // Warning: Not async coz I'm to lazy to implement a ddelegate command structure
                    var cont = ((MetroWindow) Application.Current.MainWindow).ShowModalMessageExternal(
                        Resources.Properties.Resources.AbstractSaveAsFileCommand_Execute_Warning,
                        Resources.Properties.Resources.AbstractSaveAsFileCommand_Execute_FileAlreadyExistsWouldYouLikeToOverwriteIt,
                        MessageDialogStyle.AffirmativeAndNegative);
                    if (cont == MessageDialogResult.Negative) return;
                }

                Export(file, tab);

                FileUtils.LastFileDialogLocation = file;

                Context?.HideMenu();
            }
        }
    }
}