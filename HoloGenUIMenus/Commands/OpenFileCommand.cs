﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

using System.Diagnostics;
using System.IO;
using System.Windows;
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.Image;
using HoloGen.IO;
using HoloGen.Options;
using HoloGen.UI.Utils;
using HoloGen.Utils;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;

namespace HoloGen.UI.Menus.Commands
{
    /// <summary>
    /// HoloGen menu command
    /// </summary>
    public class OpenFileCommand : AbstractMenuCommand
    {
        public OpenFileCommand()
        {
        }

        public OpenFileCommand(IMenuWindow context) : base(context)
        {
        }

        public override string Name => Resources.Properties.Resources.OpenFileCommand_OpenFile;

        public override string ToolTip => Resources.Properties.Resources.OpenFileCommand_OpenAHologramSetupFileOrPreviousResultImage;

        public override void Execute(object parameter)
        {
            var mainWindowViewModel = TabHandlerFramework.GetActiveHandler();

            var dialog = new OpenFileDialog
            {
                Filter = FileTypes.CombinedFileFilter + "|" +
                         FileTypes.SetupFileFilter + "|" +
                         FileTypes.BatchFileFilter + "|" +
                         FileTypes.ImageFileFilter + "|" +
                         FileTypes.AllFileFilter
            };
            Debug.Assert(FileUtils.LastFileDialogLocation.Directory != null, "FileUtils.LastFileDialogLocation.Directory != null");
            dialog.InitialDirectory = FileUtils.LastFileDialogLocation.Directory.FullName;

            var result = dialog.ShowDialog();
            if (result.HasValue && result.Value)
            {
                var file = new FileInfo(dialog.FileName);
                if (file.Exists)
                {
                    string error;
                    HierarchySaveable loaded;

                    if (file.Extension == FileTypes.SetupFileExtension)
                    {
                        var res = new JsonOptionsFileLoader().Load(file);
                        error = res.Item1;
                        loaded = res.Item2;
                    }
                    else if (file.Extension == FileTypes.ImageFileExtension)
                    {
                        var res = new JsonImageFileLoader().Load(file);
                        error = res.Item1;
                        loaded = res.Item2;
                    }
                    else if (file.Extension == FileTypes.BatchFileExtension)
                    {
                        var res = new JsonBatchFileLoader().Load(file);
                        error = res.Item1;
                        loaded = res.Item2;
                    }
                    else
                    {
                        ((MetroWindow) Application.Current.MainWindow).ShowMessageAsync(
                            Resources.Properties.Resources.AbstractSaveAsFileCommand_Execute_Error,
                            Resources.Properties.Resources.OpenFileCommand_Execute_ErrorUnknownFileExtension);
                        return;
                    }

                    if (error == null || error != "")
                        ((MetroWindow) Application.Current.MainWindow).ShowMessageAsync(
                            Resources.Properties.Resources.AbstractSaveAsFileCommand_Execute_Error,
                            error);
                    else if (file.Extension == FileTypes.SetupFileExtension)
                        mainWindowViewModel.AddTab((OptionsRoot) loaded, file);
                    else if (file.Extension == FileTypes.ImageFileExtension)
                        mainWindowViewModel.AddTab((ComplexImage) loaded, file);
                    else if (file.Extension == FileTypes.BatchFileExtension)
                        mainWindowViewModel.AddTab((BatchDataObject) loaded, file);
                    else
                        ((MetroWindow) Application.Current.MainWindow).ShowMessageAsync(
                            Resources.Properties.Resources.AbstractSaveAsFileCommand_Execute_Error,
                            Resources.Properties.Resources.OpenFileCommand_Execute_ErrorUnknownFileExtension);

                    FileUtils.LastFileDialogLocation = file;
                }
                else
                {
                    ((MetroWindow) Application.Current.MainWindow).ShowMessageAsync(
                        Resources.Properties.Resources.AbstractSaveAsFileCommand_Execute_Error,
                        Resources.Properties.Resources.AbstractOpenFileCommand_Execute_FailedToLoadTheFile);
                }
            }
        }
    }
}