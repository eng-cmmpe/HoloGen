﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.OptionTypes;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using HoloGen.UI.Utils.Commands;

namespace HoloGen.UI.OptionEditors
{
    /// <summary>
    /// User interface editor for <see cref="IntegerOption" />s.
    /// </summary>
    public partial class IntegerEditor : UserControl, INotifyPropertyChanged, IDataErrorInfo
    {
        private ICommand _clearCommand;
        private IntegerOption _option;

        public IntegerEditor()
        {
            InitializeComponent();

            Loaded += Editor_Loaded;
        }

        public int Value
        {
            get => _option?.Value ?? 0;
            set
            {
                _option.Value = value;
                OnPropertyChanged(nameof(Value));
            }
        }

        public ICommand ClearCommand => _clearCommand ?? (_clearCommand = new SimpleCommand
        {
            CanExecuteDelegate = x => true,
            ExecuteDelegate = x => _option.Reset()
        });

        public string Error => throw new NotImplementedException();

        public string this[string columnName] => _option != null && !_option.Valid ? _option.Error[0] : null;

        public event PropertyChangedEventHandler PropertyChanged;

        private void Editor_Loaded(object sender, RoutedEventArgs e)
        {
            // Somewhat hacky way to find the view model given WPF's idiosyncratic
            // hierarchical data template structure
            _option = (IntegerOption) DataContext;
            OnPropertyChanged(nameof(Value));
            _option.PropertyChanged += Option_PropertyChanged;
        }

        private void Option_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(nameof(Value));
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}