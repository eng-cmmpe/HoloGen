﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.OptionTypes;
using MahApps.Metro.Controls;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Linq;
using NHotkey;
using NHotkey.Wpf;
using System.Windows.Input;
using System.Diagnostics;

namespace HoloGen.UI.OptionEditors
{
    /// <summary>
    /// User interface editor for <see cref="PathListOption" />s.
    /// </summary>
    public partial class PathListEditor : UserControl, INotifyPropertyChanged, IDataErrorInfo
    {
        private readonly HotKey _hotKey = new HotKey(Key.Delete);
        private PathListOption _option;

        public PathListEditor()
        {
            InitializeComponent();

            Loaded += Editor_Loaded;
            GotFocus += PathListEditor_GotFocus;
            LostFocus += PathListEditor_LostFocus;
        }

        public bool ViewButtonEnabled => _option != null && _option.Valid;

        public bool AddButtonEnabled =>
            _option != null;

        public bool RemoveButtonEnabled =>
            _option != null && _option.Value.Count > 0 && SubEditor.SelectedItems.Count >= 1;

        public bool MoveUpButtonEnabled =>
            _option != null && _option.Value.Count > 1 && SubEditor.SelectedItems.Count == 1 && SubEditor.SelectedIndex + 1 < _option.Value.Count;

        public bool MoveDownButtonEnabled =>
            _option != null && _option.Value.Count > 1 && SubEditor.SelectedItems.Count == 1 && SubEditor.SelectedIndex > 0;

        public bool MoveTopButtonEnabled =>
            _option != null && _option.Value.Count > 1 && SubEditor.SelectedItems.Count == 1 && SubEditor.SelectedIndex + 1 < _option.Value.Count;

        public bool MoveBottomButtonEnabled =>
            _option != null && _option.Value.Count > 1 && SubEditor.SelectedItems.Count == 1 && SubEditor.SelectedIndex > 0;

        public bool SortAlphabeticalDownButtonEnabled =>
            _option != null && _option.Value.Count > 1;

        public bool SortAlphabeticalUpButtonEnabled =>
            _option != null && _option.Value.Count > 1;

        public bool OpenFolderButtonEnabled =>
            _option != null && _option.Value.Count > 0 && SubEditor.SelectedItems.Count == 1;

        public string Error => string.Empty;

        public string this[string columnName] => _option?.Error[0];

        public event PropertyChangedEventHandler PropertyChanged;

        private void Editor_Loaded(object sender, RoutedEventArgs e)
        {
            // Somewhat hacky way to find the view model given WPF's idiosyncratic
            // hierarchical data template structure
            _option = (PathListOption) DataContext;
            OnPropertyChanged("Value");
            _option.PropertyChanged += Option_PropertyChanged;

            OnPropertyChanged("AddButtonEnabled");
            OnPropertyChanged("RemoveButtonEnabled");
            OnPropertyChanged("MoveUpButtonEnabled");
            OnPropertyChanged("MoveDownButtonEnabled");
            OnPropertyChanged("MoveTopButtonEnabled");
            OnPropertyChanged("MoveBottomButtonEnabled");
            OnPropertyChanged("SortAlphabeticalDownButtonEnabled");
            OnPropertyChanged("SortAlphabeticalUpButtonEnabled");
            OnPropertyChanged("OpenFolderButtonEnabled");
        }

        private void Option_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged("Value");

            OnPropertyChanged("AddButtonEnabled");
            OnPropertyChanged("RemoveButtonEnabled");
            OnPropertyChanged("MoveUpButtonEnabled");
            OnPropertyChanged("MoveDownButtonEnabled");
            OnPropertyChanged("MoveTopButtonEnabled");
            OnPropertyChanged("MoveBottomButtonEnabled");
            OnPropertyChanged("SortAlphabeticalDownButtonEnabled");
            OnPropertyChanged("SortAlphabeticalUpButtonEnabled");
            OnPropertyChanged("OpenFolderButtonEnabled");
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void PathListEditor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OnPropertyChanged("AddButtonEnabled");
            OnPropertyChanged("RemoveButtonEnabled");
            OnPropertyChanged("MoveUpButtonEnabled");
            OnPropertyChanged("MoveDownButtonEnabled");
            OnPropertyChanged("MoveTopButtonEnabled");
            OnPropertyChanged("MoveBottomButtonEnabled");
            OnPropertyChanged("SortAlphabeticalDownButtonEnabled");
            OnPropertyChanged("SortAlphabeticalUpButtonEnabled");
            OnPropertyChanged("OpenFolderButtonEnabled");
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog
            {
                EnsurePathExists = true,
                EnsureFileExists = true,
                AllowNonFileSystemItems = false,
                Multiselect = true
            };
            var filterParts = _option.Filter.Split('|');
            for (var i = 0; i < filterParts.Length; i += 2) dialog.Filters.Add(new CommonFileDialogFilter(filterParts[i + 0], filterParts[i + 1]));
            var result = dialog.ShowDialog();
            if (result == CommonFileDialogResult.Ok)
                foreach (var file in dialog.FileNames)
                    _option.Value.Add(new FileInfo(file));
            OnPropertyChanged("Value");
            OnPropertyChanged("Valid");
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            var itemsToRemove = new ObservableCollection<FileInfo>();

            foreach (FileInfo item in SubEditor.SelectedItems) itemsToRemove.Add(item);
            foreach (var item in itemsToRemove) _option.Value.Remove(item);
            OnPropertyChanged("Value");
            OnPropertyChanged("Valid");
        }

        private void MoveUpButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedIndex = SubEditor.SelectedIndex;

            if (selectedIndex + 1 < _option.Value.Count)
            {
                var itemToMoveDown = _option.Value[selectedIndex];
                _option.Value.RemoveAt(selectedIndex);
                _option.Value.Insert(selectedIndex + 1, itemToMoveDown);
                SubEditor.SelectedIndex = selectedIndex + 1;
            }
        }

        private void MoveDownButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedIndex = SubEditor.SelectedIndex;

            if (selectedIndex > 0)
            {
                var itemToMoveUp = _option.Value[selectedIndex];
                _option.Value.RemoveAt(selectedIndex);
                _option.Value.Insert(selectedIndex - 1, itemToMoveUp);
                SubEditor.SelectedIndex = selectedIndex - 1;
            }
        }

        private void MoveTopButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedIndex = SubEditor.SelectedIndex;
            var lastIndex = _option.Value.Count - 1;

            if (selectedIndex + 1 < _option.Value.Count)
            {
                var itemToMoveDown = _option.Value[selectedIndex];
                _option.Value.RemoveAt(selectedIndex);
                _option.Value.Insert(lastIndex, itemToMoveDown);
                SubEditor.SelectedIndex = lastIndex;
            }
        }

        private void MoveBottomButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedIndex = SubEditor.SelectedIndex;

            if (selectedIndex > 0)
            {
                var itemToMoveUp = _option.Value[selectedIndex];
                _option.Value.RemoveAt(selectedIndex);
                _option.Value.Insert(0, itemToMoveUp);
                SubEditor.SelectedIndex = 0;
            }
        }

        private void SortAlphabeticalDownButton_Click(object sender, RoutedEventArgs e)
        {
            _option.Value = new ObservableCollection<FileInfo>(_option.Value.OrderBy(f => f.FullName));
        }

        private void SortAlphabeticalUpButton_Click(object sender, RoutedEventArgs e)
        {
            _option.Value = new ObservableCollection<FileInfo>(_option.Value.OrderByDescending(f => f.FullName));
        }

        private void OpenFolderButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var proc = new ProcessStartInfo
                {
                    FileName = ((FileInfo) SubEditor.SelectedItem).DirectoryName ?? throw new InvalidOperationException(),
                    Verb = "open"
                };
                Process.Start(proc);
            }
            catch (Exception)
            {
                // Ignore errors
            }
        }

        private void OnHotKey(object sender, HotkeyEventArgs e)
        {
            var itemsToRemove = new ObservableCollection<FileInfo>();

            foreach (FileInfo item in SubEditor.SelectedItems) itemsToRemove.Add(item);
            foreach (var item in itemsToRemove) _option.Value.Remove(item);
            OnPropertyChanged("Value");
            OnPropertyChanged("Valid");
            e.Handled = true;
        }

        private void PathListEditor_LostFocus(object sender, RoutedEventArgs e)
        {
            HotkeyManager.Current.Remove("delete");
        }

        private void PathListEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            HotkeyManager.Current.AddOrReplace("delete", _hotKey.Key, _hotKey.ModifierKeys, OnHotKey);
        }
    }
}