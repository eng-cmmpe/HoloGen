﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using Newtonsoft.Json;
using System.ComponentModel;

namespace HoloGen.SLMControl.Options
{
    /// <summary>
    /// Page containing the SLMControl options.
    /// </summary>
    public sealed class SLMControlOptions : HierarchyRoot
    {
        public SLMControlOptions()
        {
            PropertyChanged += OptionsRoot_PropertyChanged;
            UpdateErrors();
            SLMOutputPage.SLMControlOutputFolder.SetResolutionParameters(
                SLMParamsPage.SLMControlParamsFolder.SLMResolutionX,
                SLMParamsPage.SLMControlParamsFolder.SLMResolutionY,
                SLMParamsPage.SLMControlParamsFolder.BitRateTypeOption);
        }

        public SLMParamsPage SLMParamsPage { get; } = new SLMParamsPage();
        public SLMFilesPage SLMFilesPage { get; } = new SLMFilesPage();
        public SLMOutputPage SLMOutputPage { get; } = new SLMOutputPage();

        public override string Name => Resources.Properties.Resources.SLMControlOptions_SLMControllerOptions;

        public override string ToolTip => Resources.Properties.Resources.SLMControlOptions_OptionsForTheCMMPESLMController;

        [JsonIgnore]
        public override bool Valid => Error.Count == 0;

        private void OptionsRoot_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Valid") UpdateErrors();
        }

        private void UpdateErrors()
        {
            var errors = "";
            var first = true;
            foreach (var error in Error)
            {
                if (!first)
                    errors += "\n";
                else
                    first = false;
                errors += error;
            }

            SLMOutputPage.SLMControlErrorFolder.ErrorText.Value = errors;
        }
    }
}