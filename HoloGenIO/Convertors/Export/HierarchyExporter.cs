﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using ClosedXML.Excel;
using HoloGen.Hierarchy.Hierarchy;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HoloGen.IO.Convertors.Export
{
    /// <summary>
    /// Handle exporting images to an excel format
    /// </summary>
    public class HierarchyExporter
    {
        public string Export(FileInfo file, HierarchyRoot options)
        {
            var opt = new List<HierarchyRoot> {options};
            return Export(file, opt);
        }

        public string Export(FileInfo file, List<HierarchyRoot> options, string prefix = "ExportedOptions")
        {
            var flat = options.ConvertAll(
                option => option.Flatten().Distinct().OrderBy(o => o.Name).ToList());

            var workbook = new XLWorkbook();
            var worksheet = workbook.Worksheets.Add(GetUniqueName(workbook, prefix));

            for (var row = 0; row < flat[0].Count; row++)
            {
                worksheet.Cell(row + 1, 1).Value = flat[0][row].Name;
                worksheet.Cell(row + 1, 2).Value = flat[0][row].BindingPath;
                worksheet.Cell(row + 1, 3).Value = flat[0][row].ToolTip;

                for (var column = 0; column < flat.Count; column++) worksheet.Cell(row + 1, column + 4).Value = flat[column][row].ConvertToString();
            }

            workbook.SaveAs(file.FullName);

            return "";
        }

        public static string GetUniqueName(XLWorkbook workbook, string prefix)
        {
            var lastUsedFileNo = 1;
            string name = null;

            if (!IsNameValid(prefix)) prefix = "ExportedOptions";

            while (name == null)
            {
                name = prefix + "_" + lastUsedFileNo.ToString("D3");
                lastUsedFileNo++;
                if (workbook.TryGetWorksheet(name, out _)) name = null;
            }

            // TODO: Check for overrun

            return name;
        }

        private static bool IsNameValid(string prefix)
        {
            return !string.IsNullOrEmpty(prefix) && prefix.IndexOfAny(Path.GetInvalidFileNameChars()) < 0;
        }
    }
}