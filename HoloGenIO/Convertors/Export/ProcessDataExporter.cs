﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using ClosedXML.Excel;
using csmatio.types;
using System;
using System.Collections.Generic;
using System.IO;
using HoloGen.Alg.Base.Managed;

namespace HoloGen.IO.Convertors.Export
{
    /// <summary>
    /// Handle exporting images to a *.mat format
    /// </summary>
    public class ProcessDataExporter
    {
        public string Export(FileInfo file, List<List<Tuple<DateTime, Dictionary<MetricType, float>>>> metricscollection)
        {
            var toSave = new List<MLArray>();

            var metricsMet = new Dictionary<MetricType, int>();
            var maxMetricsColumn = 2;

            var workbook = new XLWorkbook();
            foreach (var allmetrics in metricscollection)
            {
                var worksheet = workbook.Worksheets.Add(GetUniqueName(workbook, "Metrics"));

                for (var row = 0; row < allmetrics.Count; ++row)
                {
                    var datetime = allmetrics[row].Item1;
                    var metrics = allmetrics[row].Item2;
                    worksheet.Cell(row + 2, 1).Value = datetime;
                    foreach (var metric in metrics)
                    {
                        if (!metricsMet.ContainsKey(metric.Key))
                        {
                            metricsMet.Add(metric.Key, maxMetricsColumn);
                            worksheet.Cell(1, maxMetricsColumn).Value = metric.Key;
                            maxMetricsColumn++;
                        }

                        worksheet.Cell(row + 2, metricsMet[metric.Key]).Value = metric.Value;
                    }
                }
            }

            workbook.SaveAs(file.FullName);

            return "";
        }

        public static string GetUniqueName(XLWorkbook workbook, string prefix)
        {
            var lastUsedFileNo = 1;
            string name = null;

            if (!IsNameValid(prefix)) prefix = "ExportedOptions";

            while (name == null)
            {
                name = prefix + "_" + lastUsedFileNo.ToString("D3");
                lastUsedFileNo++;
                if (workbook.TryGetWorksheet(name, out _)) name = null;
            }

            // TODO: Check for overrun

            return name;
        }

        private static bool IsNameValid(string prefix)
        {
            return !string.IsNullOrEmpty(prefix) && prefix.IndexOfAny(Path.GetInvalidFileNameChars()) < 0;
        }
    }
}