﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using csmatio.io;
using csmatio.types;
using HoloGen.Image;
using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;

namespace HoloGen.IO.Convertors.Import
{
    /// <summary>
    /// Handle exporting images to a *.mat format
    /// </summary>
    public class MatImporter
    {
        public List<ComplexImage> IlluminationImages = new List<ComplexImage>();
        public List<ComplexImage> InitialImages = new List<ComplexImage>();
        public List<ComplexImage> RegionImages = new List<ComplexImage>();
        public List<ComplexImage> TargetImages = new List<ComplexImage>();

        public string Import(FileInfo file)
        {
            if (file == null) return Resources.Properties.Resources.JsonFileLoader_Load_ErrorNullInputFile;

            if (!file.Exists) return Resources.Properties.Resources.JsonFileLoader_Load_ErrorFileDoesNotExist;

            // c.f. https://sourceforge.net/projects/csmatio/files/

            try
            {
                var mfr = new MatFileReader(file.FullName);

                string error;
                MLSingle arrayData;

                if (mfr.Content.ContainsKey("TARGET"))
                {
                    arrayData = mfr.Content["TARGET"] as MLSingle;
                    if (arrayData == null) return Resources.Properties.Resources.MatImporter_Convert3DArray_ErrorNullFieldOrFieldInIncorrectFormat;
                    if (arrayData.Dimensions.Length == 2)
                    {
                        error = Convert2DArray(arrayData, ref TargetImages);
                    }
                    else if (arrayData.Dimensions.Length == 3)
                    {
                        error = Convert3DArray(arrayData, ref TargetImages);
                    }
                    else
                    {
                        return Resources.Properties.Resources.MatImporter_Convert3DArray_ErrorIncorrectNumberOfArrayDimensionsInField;
                    }

                    if (error == null || error != "") return error;
                }
                else
                {
                    return Resources.Properties.Resources.MatImporter_Import_ErrorFieldTARGETWasNotPresent;
                }

                if (mfr.Content.ContainsKey("ILLUMINATION"))
                {
                    arrayData = mfr.Content["ILLUMINATION"] as MLSingle;
                    error = Convert2DArray(arrayData, ref IlluminationImages);
                    if (error == null || error != "") return error;
                }

                if (mfr.Content.ContainsKey("HOLOGRAM"))
                {
                    arrayData = mfr.Content["HOLOGRAM"] as MLSingle;
                    error = Convert3DArray(arrayData, ref InitialImages);
                    if (error == null || error != "") return error;
                }

                if (mfr.Content.ContainsKey("POINTS_of_INTEREST"))
                {
                    arrayData = mfr.Content["POINTS_of_INTEREST"] as MLSingle;
                    error = Convert2DArray(arrayData, ref RegionImages);
                    if (error == null || error != "") return error;
                }

                arrayData?.GetArray();
            }
            catch (Exception)
            {
                return Resources.Properties.Resources.MatImporter_Import_ErrorCouldnTParseFile;
            }

            return "";
        }

        private string Convert3DArray(MLSingle input, ref List<ComplexImage> output)
        {
            var result = new Complex[input.Dimensions[1], input.Dimensions[2]];

            if (input.NDimensions != 3) return Resources.Properties.Resources.MatImporter_Convert3DArray_ErrorIncorrectNumberOfArrayDimensionsInField;

            for (var h = 0; h < input.Dimensions[0]; h++)
            {
                for (var i = 0; i < input.Dimensions[1]; i++)
                for (var j = 0; j < input.Dimensions[2]; j++)
                    if (input.IsComplex)
                        result[i, j] = new Complex(input.GetReal(i, j), input.GetImaginary(i, j));
                    else
                        result[i, j] = new Complex(input.GetReal(i, j), 0);

                output.Add(new ComplexImage(result));
            }

            return "";
        }

        private string Convert2DArray(MLSingle input, ref List<ComplexImage> output)
        {
            var result = new Complex[input.Dimensions[0], input.Dimensions[1]];

            if (input.NDimensions != 2) return Resources.Properties.Resources.MatImporter_Convert3DArray_ErrorIncorrectNumberOfArrayDimensionsInField;

            for (var i = 0; i < input.Dimensions[0]; i++)
            for (var j = 0; j < input.Dimensions[1]; j++)
                if (input.IsComplex)
                    result[i, j] = new Complex(input.GetReal(i, j), input.GetImaginary(i, j));
                else
                    result[i, j] = new Complex(input.GetReal(i, j), 0);

            output.Add(new ComplexImage(result));

            return "";
        }

        private int[,] Slice(int[,,] input, int level)
        {
            var result = new int[input.GetLength(1), input.GetLength(2)];

            for (var i = 0; i < input.GetLength(1); i++)
            for (var j = 0; j < input.GetLength(2); j++)
                result[i, j] = input[level, i, j];

            return result;
        }
    }
}