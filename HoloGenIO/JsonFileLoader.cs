﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.Serial;
using System;
using System.IO;

namespace HoloGen.IO
{
    /// <summary>
    /// Loader for files in a JSON format
    /// </summary>
    public class JsonFileLoader<S, T> : FileLoader<S, T>
        where S : HierarchySaveable, new()
        where T : HierarchyVersion, new()
    {
        public override Tuple<string, S> Load(FileInfo file)
        {
            if (file == null) return new Tuple<string, S>(Resources.Properties.Resources.JsonFileLoader_Load_ErrorNullInputFile, null);

            if (!file.Exists) return new Tuple<string, S>(Resources.Properties.Resources.JsonFileLoader_Load_ErrorFileDoesNotExist, null);

            string output;

            try
            {
                output = File.ReadAllText(file.FullName);
            }
            catch (PathTooLongException)
            {
                return new Tuple<string, S>(Resources.Properties.Resources.JsonFileLoader_Load_ErrorUnableToLoadFileFilePathIsTooLong, null);
            }
            catch (DirectoryNotFoundException)
            {
                return new Tuple<string, S>(Resources.Properties.Resources.JsonFileLoader_Load_ErrorUnableToLoadFileDirectoryNotFound, null);
            }
            catch (UnauthorizedAccessException)
            {
                return new Tuple<string, S>(Resources.Properties.Resources.JsonFileLoader_Load_ErrorUnableToLoadFileInadequateFileAccessPermissions, null);
            }
            catch (Exception)
            {
                return new Tuple<string, S>(Resources.Properties.Resources.JsonFileLoader_Load_ErrorUnableToLoadFile, null);
            }

            if (output == "") return new Tuple<string, S>(Resources.Properties.Resources.JsonFileLoader_Load_ErrorFileAppearsToBeEmpty, null);

            var deserialiser = new JsonDeserialiser<S, T>();

            var res = deserialiser.Deserialise(output);

            var resStat = res.Item1;
            var resRet = res.Item2;

            switch (resStat)
            {
                case DeserialiseResult.Success:
                    return new Tuple<string, S>("", resRet);
                case DeserialiseResult.BadFormat:
                    return new Tuple<string, S>(Resources.Properties.Resources.JsonFileLoader_Load_ErrorFileIsInAnIncorrectFormat, null);
                case DeserialiseResult.NoSuchFile:
                    return new Tuple<string, S>(Resources.Properties.Resources.JsonFileLoader_Load_ErrorFileDoesNotExist, null);
                case DeserialiseResult.FileFromEarlierVersion:
                    return new Tuple<string, S>(Resources.Properties.Resources.JsonFileLoader_Load_ErrorFileIsFromAnEarlierVersionOfTeApplicationAnUpdateWasNotPossible, null);
                case DeserialiseResult.FileFromLaterVersion:
                    return new Tuple<string, S>(Resources.Properties.Resources.JsonFileLoader_Load_ErrorFileIsFromALaterVersionOfTheApplication, null);
                case DeserialiseResult.UnknownError:
                    return new Tuple<string, S>(Resources.Properties.Resources.JsonFileLoader_Load_ErrorAnUnknownErrorOccured, null);
                default:
                    return new Tuple<string, S>(Resources.Properties.Resources.JsonFileLoader_Load_ErrorAnUnknownErrorOccured, null);
            }
        }
    }
}