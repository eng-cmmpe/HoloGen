\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Credits}{1}{section.2}
\contentsline {section}{\numberline {3}Getting Started}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Installation}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Menus}{4}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Setup}{5}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Image Viewer}{11}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Running a setup file}{11}{subsection.3.5}
\contentsline {section}{\numberline {4}How Do I?}{11}{section.4}
\contentsline {subsection}{\numberline {4.1}Change the Language}{11}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Import and Export Data}{15}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Generate a Hologram?}{17}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}View Metadata}{17}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Batch Processing}{17}{subsection.4.5}
\contentsline {section}{\numberline {5}Further Information}{19}{section.5}
\contentsline {subsection}{\numberline {5.1}Contributing}{19}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Learn More}{19}{subsection.5.2}
