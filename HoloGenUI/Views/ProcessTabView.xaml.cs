﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.UI.Process.Monitor.ViewModels;
using HoloGen.UI.ViewModels;
using HoloGen.Utils;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace HoloGen.UI.Views
{
    /// <summary>
    /// Interaction logic for ProcessTabView.xaml
    /// </summary>
    public partial class ProcessTabView : UserControl, INotifyPropertyChanged
    {
        private readonly StatusMessage _statusMessage = new StatusMessage();

        private bool _show;
        private ProcessTabViewModel _viewModel;

        public ProcessTabView()
        {
            InitializeComponent();

            Loaded += ProcessTabView_Loaded;
            IsVisibleChanged += ProcessTabView_IsVisibleChanged;

            SetShowMessage(true);
        }

        public ProcessTabViewModel ViewModel
        {
            get => _viewModel;
            set => SetProperty(ref _viewModel, value);
        }

        private void ProcessTabView_IsVisibleChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            if (ViewModel != null) SetShowMessage(IsVisible);
        }

        public void SetShowMessage(bool show)
        {
            if (_show != show)
            {
                _show = show;
                ConsoleTextBox.Dispatcher.BeginInvoke(_show
                    ? () => { StatusMessageFramework.Messages.Add(_statusMessage); }
                    : new Action(() => { StatusMessageFramework.Messages.Remove(_statusMessage); }));
            }
        }

        private void ProcessTabView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
        }

        private void ProcessTabView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            // Somewhat hacky way to find the view model given WPF's idiosyncratic
            // hierarchical data template structure
            if (DataContext != null)
            {
                var model = DataContext as ProcessTabViewModel;
                if ((model != null) & (ViewModel == null))
                {
                    ViewModel = model;
                    ViewModel.SetLogger(Log);
                    ViewModel.Start();
                }
            }
        }

        private void HamburgerMenuControl_OnItemClick(object sender, ItemClickEventArgs e)
        {
            ViewModel.ProcessViewModel = (ProcessViewModel) e.ClickedItem;
        }

        private void Log(string message)
        {
            var timedMessage = Time.Timestamp() + message;
            Debug.WriteLine(timedMessage);
            ConsoleTextBox.Dispatcher.BeginInvoke(new Action(
                () =>
                {
                    ConsoleTextBox.AppendText(timedMessage + "\n");
                    ConsoleTextBox.ScrollToEnd();
                    _statusMessage.Message = timedMessage;
                }));

            LogFileHandle.Instance.Log(timedMessage);
        }

        #region copied from ObservableObject

        /// <summary>
        /// Sets the property.
        /// </summary>
        /// <returns><c>true</c>, if property was set, <c>false</c> otherwise.</returns>
        /// <param name="backingStore">Backing store.</param>
        /// <param name="value">Value.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="onChanged">On changed.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        protected bool SetProperty<T>(
            ref T backingStore, T value,
            [CallerMemberName] string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        /// <summary>
        /// Occurs when property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises the property changed event.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;

            changed?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}