﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Image;
using HoloGen.IO.Convertors.Export;
using HoloGen.Options;
using HoloGen.UI.Process.Monitor.ViewModels;
using HoloGen.UI.Utils;
using HoloGen.Utils;
using MahApps.Metro.IconPacks;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using System.Windows;

namespace HoloGen.UI.ViewModels
{
    /// <summary>
    /// HoloGen tab viewmodel
    /// </summary>
    public class ProcessTabViewModel : AbstractTabViewModel<object>, ICanExportExcel
    {
        private ProcessViewModel _processViewModel;

        private ObservableCollection<ProcessViewModel> _processViewModels = new ObservableCollection<ProcessViewModel>();

        private ProcessViewModel _runningProcessViewModel;

        private bool _showingMultiProcesses;
        private bool _showingSingleProcess;

        private Action<string> _logger = s => { };

        public ProcessTabViewModel(OptionsRoot options)
            : base(null)
        {
            ProcessViewModel = new ProcessViewModel(options, OnCompletion(options));
            ProcessViewModels.Add(ProcessViewModel);

            ShowingSingleProcess = true;
            ShowingMultiProcesses = false;
        }

        public ProcessTabViewModel(List<OptionsRoot> options)
            : base(null)
        {
            var i = 1;
            foreach (var option in options)
            {
                var model = new ProcessViewModel((OptionsRoot) option.Clone(), OnCompletion(option)) {ID = i++};
                model.Name = Resources.Properties.Resources.ProcessTabViewModel_ProcessTabViewModel_Process_ + model.ID.ToString("D3");
                model.Icon = QueuedIconFromInt(model.ID);
                ProcessViewModels.Add(model);
            }

            ProcessViewModel = ProcessViewModels[0];

            ShowingSingleProcess = false;
            ShowingMultiProcesses = true;
        }

        public bool ShowingSingleProcess
        {
            get => _showingSingleProcess;
            private set => SetProperty(ref _showingSingleProcess, value);
        }

        public bool ShowingMultiProcesses
        {
            get => _showingMultiProcesses;
            set => SetProperty(ref _showingMultiProcesses, value);
        }

        public ProcessViewModel ProcessViewModel
        {
            get => _processViewModel;
            set => SetProperty(ref _processViewModel, value);
        }

        public ProcessViewModel RunningProcessViewModel
        {
            get => _runningProcessViewModel;
            set => SetProperty(ref _runningProcessViewModel, value);
        }

        public ObservableCollection<ProcessViewModel> ProcessViewModels
        {
            get => _processViewModels;
            set => SetProperty(ref _processViewModels, value);
        }

        public override string Name => CanCurrentlyExport ? Resources.Properties.Resources.ProcessTabViewModel_Name_CompletedProcess : Resources.Properties.Resources.ProcessTabViewModel_Name_RunningProcess;

        public string ExcelFileFilter => FileTypes.ExcelFileFilter;

        public bool CanCurrentlyExport { get; private set; }

        public string ExportExcel(FileInfo file)
        {
            var metrics = ProcessViewModels.ToList().ConvertAll(proc => proc.AllMetrics);
            var task = Task.Run(() =>
            {
                var exporter = new ProcessDataExporter();
                var terror = exporter.Export(file, metrics);
                return new Tuple<string, ProcessDataExporter>(terror, exporter);
            });

            task.Wait();

            var res = task.Result;
            return res.Item1;
        }

        public void Start()
        {
            RunningProcessViewModel = ProcessViewModels[0];
            RunningProcessViewModel.Icon = new PackIconMaterial {Kind = PackIconMaterialKind.RunFast};
            RunningProcessViewModel.StartProcess(_logger);
        }

        private static int _currentFileIndex = 1;

        public Action<Task<Complex[,]>> OnCompletion(OptionsRoot options)
        {
            return task =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action<Complex[,]>(s =>
                    {
                        OnPropertyChanged(nameof(Name));
                        if (s != null) TabHandlerFramework.GetActiveHandler().AddTab(
                            new ComplexImage(s, options),
                            new FileInfo(options.OutputPage.RunFolder.RunFile.Value.FullName + "\\Untitled_" + (_currentFileIndex++).ToString("D3") + ".hfimg"),
                            selectNewTab: _showingSingleProcess,
                            needsSaving: true);
                        
                        RunningProcessViewModel.Icon = CompletedIconFromInt(RunningProcessViewModel.ID);
                        var i = 1 + ProcessViewModels.IndexOf(RunningProcessViewModel);
                        if (i < ProcessViewModels.Count)
                        {
                            if (RunningProcessViewModel == ProcessViewModel)
                                ProcessViewModel = ProcessViewModels[i];
                            RunningProcessViewModel = ProcessViewModels[i];
                            RunningProcessViewModel.Icon = new PackIconMaterial {Kind = PackIconMaterialKind.RunFast};
                            RunningProcessViewModel.StartProcess(_logger);
                        }
                        else
                        {
                            CanCurrentlyExport = true;
                            OnPropertyChanged(nameof(Name));
                            OnPropertyChanged(nameof(CanCurrentlyExport));
                        }
                    }),
                    task.Result);
            };
        }

        public void SetLogger(Action<string> logger)
        {
            _logger = logger;
        }

        private object QueuedIconFromInt(int i)
        {
            switch (i % 10)
            {
                case 0:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric0BoxOutline};
                case 1:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric1BoxOutline};
                case 2:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric2BoxOutline};
                case 3:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric3BoxOutline};
                case 4:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric4BoxOutline};
                case 5:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric5BoxOutline};
                case 6:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric6BoxOutline};
                case 7:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric7BoxOutline};
                case 8:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric8BoxOutline};
                case 9:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric9BoxOutline};
                default:
                    return null;
            }
        }

        private object CompletedIconFromInt(int i)
        {
            switch (i % 10)
            {
                case 0:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric0Box};
                case 1:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric1Box};
                case 2:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric2Box};
                case 3:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric3Box};
                case 4:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric4Box};
                case 5:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric5Box};
                case 6:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric6Box};
                case 7:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric7Box};
                case 8:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric8Box};
                case 9:
                    return new PackIconMaterial {Kind = PackIconMaterialKind.Numeric9Box};
                default:
                    return null;
            }
        }
    }
}