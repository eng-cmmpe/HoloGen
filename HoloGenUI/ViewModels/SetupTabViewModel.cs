﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.IO;
using HoloGen.IO.Convertors.Export;
using HoloGen.Options;
using HoloGen.UI.Hamburger.ViewModels;
using HoloGen.UI.Utils;
using HoloGen.Utils;
using MahApps.Metro.IconPacks;
using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using HoloGen.UI.Utils.Commands;

namespace HoloGen.UI.ViewModels
{
    /// <summary>
    /// HoloGen tab viewmodel
    /// </summary>
    public class SetupTabViewModel : AbstractTabViewModel<OptionsRoot>, ICanSave, ICanExportExcel
    {
        private FileInfo _fileLocation;

        private string _numErrors = "";
        private ICommand _resetCommand;

        private ICommand _runCommand;

        private string _runToolTip = "";

        public SetupTabViewModel(FileInfo fileLocation, OptionsRoot options)
            : base(options)
        {
            FileLocation = fileLocation;
            NeedsSaving = FileLocation != null && FileLocation.Exists;
            options.Changed += Data_Changed;
            Options = options;
            ViewModel = new HamburgerTabViewModel(Options);

            Options.PropertyChanged += Options_PropertyChanged;
            UpdateErrors();

            // Build the menus
            ViewModel.OptionsMenu.Add(new HamburgerMenuItem
            {
                Icon = new PackIconMaterial {Kind = PackIconMaterialKind.InformationVariant},
                Name = Resources.Properties.Resources.SetupViewModel_SetupViewModel_About,
                Data = null,
                Callback = ShowAboutCallback,
                ToolTip = Resources.Properties.Resources.SetupViewModel_SetupViewModel_About
            });
            ViewModel.OptionsMenu.Add(new HamburgerMenuItem
            {
                Icon = new PackIconMaterial {Kind = PackIconMaterialKind.Help},
                Name = Resources.Properties.Resources.SetupViewModel_SetupViewModel_Help,
                Data = null,
                Callback = ShowHelpCallback,
                ToolTip = Resources.Properties.Resources.SetupViewModel_SetupViewModel_Help
            });
            ViewModel.OptionsMenu.Add(new HamburgerMenuItem
            {
                Icon = new PackIconMaterial {Kind = PackIconMaterialKind.ChartDonutVariant},
                Name = Resources.Properties.Resources.BatchViewModel_BatchViewModel_TechnicalDocumentation,
                Data = null,
                Callback = ShowTechnicalDocsCallback,
                ToolTip = Resources.Properties.Resources.BatchViewModel_BatchViewModel_TechnicalDocumentation
            });
        }

        public ICommand RunCommand => _runCommand ?? (_runCommand = new SimpleCommand {CanExecuteDelegate = x => true, ExecuteDelegate = x => RunCommandFunc()});

        public ICommand ResetCommand => _resetCommand ?? (_resetCommand = new SimpleCommand {CanExecuteDelegate = x => true, ExecuteDelegate = x => ResetCommandFunc()});

        public string NumErrors
        {
            get => _numErrors;
            set => SetProperty(ref _numErrors, value);
        }

        public string RunToolTip
        {
            get => _runToolTip;
            set => SetProperty(ref _runToolTip, value);
        }

        public HamburgerTabViewModel ViewModel { get; }

        public OptionsRoot Options { get; }

        public string ExcelFileFilter => FileTypes.ExcelFileFilter;

        public bool CanCurrentlyExport => true;

        public string ExportExcel(FileInfo file)
        {
            var task = Task.Run(() =>
            {
                var exporter = new HierarchyExporter();
                var terror = exporter.Export(file, Options);
                return new Tuple<string, HierarchyExporter>(terror, exporter);
            });

            task.Wait();

            var res = task.Result;
            return res.Item1;
        }

        public override string Name => (FileLocation?.Name ?? "Empty Tab") + (NeedsSaving ? "*" : "");

        public string FileFilter => FileTypes.SetupFileFilter;

        public FileInfo FileLocation
        {
            get => _fileLocation;
            set => SetProperty(ref _fileLocation, value);
        }

        public bool NeedsSaving { get; private set; }

        public override string SaveToFile(FileInfo file)
        {
            base.SaveToFile(file);

            FileLocation = file;
            var err = new JsonOptionsFileSaver().Save(file, Options);

            NeedsSaving = false;
            OnPropertyChanged(nameof(Name));
            OnPropertyChanged(nameof(NeedsSaving));

            return err;
        }

        private void Data_Changed(object sender, EventArgs e)
        {
            if (!NeedsSaving)
            {
                NeedsSaving = true;
                OnPropertyChanged(nameof(Name));
                OnPropertyChanged(nameof(NeedsSaving));
            }
        }

        private bool ShowAboutCallback(HamburgerItem<HierarchyPage> item)
        {
            var mainWindowViewModel = TabHandlerFramework.GetActiveHandler();

            var file = new FileInfo(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + FileLocations.AboutFileName);

            if (file.Exists)
                mainWindowViewModel.NewBrowserTab(file);
            else
                LogFileHandle.Instance.Log(Time.Timestamp() + file.FullName);

            return true;
        }

        private bool ShowHelpCallback(HamburgerItem<HierarchyPage> item)
        {
            var mainWindowViewModel = TabHandlerFramework.GetActiveHandler();

            var file = new FileInfo(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + FileLocations.HelpFileName);

            if (file.Exists)
                mainWindowViewModel.NewBrowserTab(file);
            else
                LogFileHandle.Instance.Log(Time.Timestamp() + file.FullName);

            return true;
        }

        private bool ShowTechnicalDocsCallback(HamburgerItem<HierarchyPage> item)
        {
            var mainWindowViewModel = TabHandlerFramework.GetActiveHandler();

            var file = new FileInfo(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + FileLocations.TechnicalDocsFileName);

            if (file.Exists)
                mainWindowViewModel.NewBrowserTab(file);
            else
                LogFileHandle.Instance.Log(Time.Timestamp() + file.FullName);

            return true;
        }

        private void Options_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Valid") UpdateErrors();
        }

        private void UpdateErrors()
        {
            if (Options.Error.Count > 0)
            {
                NumErrors = Options.Error.Count.ToString();
                RunToolTip = string.Format(Resources.Properties.Resources.SetupViewModel_Options_PropertyChanged_SorryYouCanTRunAtTheMomentYouStillHave0Errors, NumErrors);
            }
            else
            {
                NumErrors = "";
                RunToolTip = Resources.Properties.Resources.SetupViewModel_Options_PropertyChanged_AreYouReadyToRockNRoll;
            }
        }

        private void ResetCommandFunc()
        {
            Options.Reset();
        }

        private void RunCommandFunc()
        {
            var mainWindowViewModel = TabHandlerFramework.GetActiveHandler();

            var clonedOptions = (OptionsRoot) Options.Clone();

            mainWindowViewModel.AddTab(new ProcessTabViewModel(clonedOptions));
        }
    }
}