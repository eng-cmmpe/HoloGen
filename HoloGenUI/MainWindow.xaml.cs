﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using Dragablz;
using HoloGen.UI.Common;
using HoloGen.UI.Utils;
using HoloGen.Utils;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace HoloGen.UI
{
    /// <summary>
    /// Main window.
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private readonly MainWindowViewModel _viewModel;

        public MainWindow() : this(new InterTabClient(), null)
        {
        }

        public MainWindow(IInterTabClient interTabClient, object partition)
        {
            _viewModel = new MainWindowViewModel(interTabClient, partition, DialogCoordinator.Instance);
            DataContext = _viewModel;
            _viewModel.TabViewModels.CollectionChanged += TabViewModels_CollectionChanged;

            MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;

            Activated += MainWindow_Activated;

            InitializeComponent();
        }

        private void MainWindow_Activated(object sender, EventArgs e)
        {
            TabHandlerFramework.SetActiveTabHandler(_viewModel);
        }

        private void TabViewModels_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (!(Flyouts.Items[0] is Flyout flyout)) return;
            flyout.IsOpen = false;
        }

        private void LaunchHoloGenOnGitHub(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://ee-git.eng.cam.ac.uk/CMMPE/HoloGen");
        }

        private void ToggleMenuFlyout(object sender, RoutedEventArgs e)
        {
            if (MenuFlyout == null) return;
            MenuFlyout.IsOpen = !MenuFlyout.IsOpen;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            CloseTab(_viewModel.Selected);
        }

        private void HelpButton_Click(object sender, RoutedEventArgs e)
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory;

            var file = new FileInfo(dir + FileLocations.HelpFileName);

            _viewModel.NewBrowserTab(file);
        }

        private void MenuFlyout_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!MenuFlyout.IsKeyboardFocusWithin && !MenuFlyout.IsMouseOver)
            {
                if (MenuFlyout == null) return;
                MenuFlyout.IsOpen = false;
            }
        }

        private void TextBlock_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // https://stackoverflow.com/questions/28765798/getting-clicked-but-not-selected-tabitem-in-wpf

            var dep = (DependencyObject) e.OriginalSource;
            // Traverse the visual tree looking for TabItem
            while (dep != null && !(dep is DragablzItem))
                dep = VisualTreeHelper.GetParent(dep);

            if (dep == null) return;

            if (dep is DragablzItem tabitem)
                if (tabitem.DataContext is ITabViewModel content)
                    CloseTab(content);
        }

        private void CloseTab(ITabViewModel tab, bool closeWindowIfNoTabs = true)
        {
            try
            {
                if (tab == null) return;

                if (tab is ICanSave save && save.NeedsSaving &&
                    _viewModel.ApplicationSettings.GeneralSettingsPage.WarningSettingsFolder.ShowCloseUnsavedFileWarning.Value)
                {
                    var cont = ((MainWindow) Application.Current.MainWindow).ShowModalMessageExternal(
                        HoloGen.Resources.Properties.Resources.AbstractSaveAsFileCommand_Execute_Warning,
                        HoloGen.Resources.Properties.Resources.MainWindowViewModel_ClosingTabItemHandlerImpl_ClosingTheFileWillLoseAnyChangesMadeWouldYouLikeToContinue,
                        MessageDialogStyle.AffirmativeAndNegative);
                    if (cont == MessageDialogResult.Negative) return;
                }

                if (_viewModel.TabViewModels.Count <= 1)
                {
                    if (closeWindowIfNoTabs)
                        Close();
                }
                else if (tab != _viewModel.Selected)
                {
                    _viewModel.TabViewModels.Remove(tab);
                }
                else // Special case when closing selected tab
                {
                    var store = _viewModel.Selected;

                    var i = _viewModel.TabViewModels.IndexOf(_viewModel.Selected);

                    _viewModel.Selected = i == 0 ? _viewModel.TabViewModels[1] : _viewModel.TabViewModels[i - 1];

                    _viewModel.TabViewModels.Remove(store);
                }
            }
            catch (TaskCanceledException)
            {
                LogFileHandle.Instance.Log(Time.Timestamp() + "Cancelled 1 or more running processes");
            }
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var needsSaving = false;

            foreach (var tab in _viewModel.TabViewModels)
                if (tab is ICanSave save && save.NeedsSaving)
                    needsSaving = true;

            if (needsSaving &&
                _viewModel.ApplicationSettings.GeneralSettingsPage.WarningSettingsFolder.ShowCloseUnsavedFileWarning.Value)
            {
                var cont = ((MainWindow) Application.Current.MainWindow).ShowModalMessageExternal(
                    HoloGen.Resources.Properties.Resources.AbstractSaveAsFileCommand_Execute_Warning,
                    HoloGen.Resources.Properties.Resources.MainWindow_MetroWindow_Closing_ClosingHoloGenWilResultInTheLossOfAnyUnsavedChangesWouldYouLikeToContinue,
                    MessageDialogStyle.AffirmativeAndNegative);
                if (cont == MessageDialogResult.Negative)
                {
                    e.Cancel = true;
                    return;
                }
            }

            if (WindowState == WindowState.Maximized)
            {
                // Use the RestoreBounds as the current values will be 0, 0 and the size of the screen
                Properties.Settings.Default.Top = RestoreBounds.Top;
                Properties.Settings.Default.Left = RestoreBounds.Left;
                Properties.Settings.Default.Height = RestoreBounds.Height;
                Properties.Settings.Default.Width = RestoreBounds.Width;
                Properties.Settings.Default.Maximized = true;
            }
            else
            {
                Properties.Settings.Default.Top = Top;
                Properties.Settings.Default.Left = Left;
                Properties.Settings.Default.Height = Height;
                Properties.Settings.Default.Width = Width;
                Properties.Settings.Default.Maximized = false;
            }

            Properties.Settings.Default.Save();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Top = Properties.Settings.Default.Top;
            Left = Properties.Settings.Default.Left;
            Height = Properties.Settings.Default.Height;
            Width = Properties.Settings.Default.Width;

            // Very quick and dirty - but it does the job
            if (Properties.Settings.Default.Maximized) WindowState = WindowState.Maximized;
        }
    }
}