﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.OptionTypes;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace HoloGen.Options.Hardware.Details
{
    /// <summary>
    /// Computer details option.
    /// </summary>
    public sealed class DetailsText : LargeTextOption
    {
        private string _default = default(string); // Override to avoid loops

        private string _value2 = default(string); // Override to avoid loops

        public DetailsText()
        {
            Enabled = false;
        }

        public override string Name => Resources.Properties.Resources.DetailsText_SystemDetails;

        public override string ToolTip => Resources.Properties.Resources.DetailsText_OverviewOfKeySystemDetailsThisIsSavedInTheHologramFileToAidInFutureDebugging;
        public override string Default => _default;

        public override string Watermark => "";

        public override string Value
        {
            get => _value2;
            set
            {
                SetProperty(ref _value2, value);
                _default = _value2;
            }
        } // Override to avoid loops

        [JsonIgnore]
        public override bool Valid => true; // Override to avoid loops

        [JsonIgnore]
        public override StringCollection Error { get; } = new StringCollection(); // Override to avoid loops

        public override bool CanClear => false;
    }
}