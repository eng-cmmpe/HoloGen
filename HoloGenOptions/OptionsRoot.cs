﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.Options.Algorithm;
using HoloGen.Options.Hardware;
using HoloGen.Options.Projector;
using HoloGen.Options.Target;
using HoloGen.Utils;
using Newtonsoft.Json;
using System.ComponentModel;
using HoloGen.Options.Output;

namespace HoloGen.Options
{
    /// <summary>
    /// Root of the hologram generation options tree. Versions should be updated in
    /// <see cref="OptionsRootVersion" />
    /// </summary>
    public sealed class OptionsRoot : HierarchyRoot
    {
        public OptionsRoot()
        {
            PropertyChanged += OptionsRoot_PropertyChanged;
            UpdateErrors();

            HardwarePage.DetailsFolder.DetailsText.Value = InfoUtils.GetInfo();
        }

        public AlgorithmsPage AlgorithmsPage { get; } = new AlgorithmsPage();
        public ProjectorPage ProjectorPage { get; } = new ProjectorPage();
        public TargetPage TargetPage { get; } = new TargetPage();
        public HardwarePage HardwarePage { get; } = new HardwarePage();
        public OutputPage OutputPage { get; } = new OutputPage();

        public override string Name => Resources.Properties.Resources.OptionsRoot_Setup;

        public override string ToolTip => Resources.Properties.Resources.OptionsRoot_SetupTheHolographGenerationCode;

        [JsonIgnore]
        public override bool Valid => Error.Count == 0;

        private void OptionsRoot_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Valid") UpdateErrors();
        }

        private void UpdateErrors()
        {
            var errors = "";
            var first = true;
            foreach (var error in Error)
            {
                if (!first)
                    errors += "\n";
                else
                    first = false;
                errors += error;
            }

            OutputPage.ErrorFolder.ErrorText.Value = errors;
        }
    }
}