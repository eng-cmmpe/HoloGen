﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.OptionTypes;

namespace HoloGen.Options.Algorithm.Algorithms
{
    /// <summary>
    /// Temperature coefficinet for Simulated Annealing.
    /// </summary>
    public sealed class ReferenceIterationsCount : IntegerOption
    {
        public override int Default => 1000;

        public override int MaxValue => int.MaxValue;

        public override int MinValue => 1;

        public override string Watermark => HoloGen.Resources.Properties.Resources.ReferenceIterationCountSimulatedAnnealing;

        public override string Name => HoloGen.Resources.Properties.Resources.ReferenceIterationCount;

        public override string ToolTip => 
            Watermark + 
            HoloGen.Resources.Properties.Resources._ + 
            HoloGen.Resources.Properties.Resources.TemperatureDecaysAccordingTemperature;
    }
}