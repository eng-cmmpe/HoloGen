﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.UI.Hamburger.ViewModels;
using MahApps.Metro.Controls;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using MenuItem = HoloGen.UI.Hamburger.ViewModels.HamburgerMenuItem;

namespace HoloGen.UI.Hamburger.Views
{
    /// <summary>
    /// Control for viewing <see cref="HierarchyRoot" /> elements in a hamburger view style.
    /// </summary>
    public sealed partial class HamburgerTabView : UserControl, INotifyPropertyChanged
    {
        private ContentControl _bottomPane;

        public HamburgerTabView()
        {
            InitializeComponent();

            HamburgerMenuControl.SelectedIndex = 0;

            Loaded += HamburgerTabView_Loaded;
        }

        public ContentControl BottomPane
        {
            get => _bottomPane;
            set
            {
                _bottomPane = value;
                OnPropertyChanged();
            }
        }

        public HamburgerTabViewModel HamburgerTabViewModel { get; private set; }

        /// <summary>
        /// Occurs when property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void HamburgerTabView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            // Somewhat hacky way to find the view model given WPF's idiosyncratic
            // hierarchical data template structure
            if (DataContext != null)
            {
                var model = DataContext as HamburgerTabViewModel;
                if ((model != null) & (HamburgerTabViewModel == null))
                {
                    HamburgerTabViewModel = model;

                    // We have to listen to changes in the collection because of the refresh mechanic
                    // of the hamburger control
                    HamburgerTabViewModel.HierarchyRoot.SearchedChildren.View.CollectionChanged += View_CollectionChanged;
                }
            }
        }

        private void View_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (HamburgerMenuControl.Content is HierarchyPage page && !page.SearchedChildren.View.IsEmpty)
            {
                var enumerator = HamburgerTabViewModel.HierarchyRoot.SearchedChildren.View.GetEnumerator();
                enumerator.MoveNext();
                var first = enumerator.Current;
                if (first != null) HamburgerMenuControl.Content = first;
            }
        }

        private void HamburgerMenuControl_OnItemClick(object sender, ItemClickEventArgs e)
        {
            HamburgerMenuControl.Content = e.ClickedItem;
        }

        // Another option to handle the options menu item click
        private void HamburgerMenu_OnOptionsItemClick(object sender, ItemClickEventArgs e)
        {
            if (e.ClickedItem is MenuItem menuItem)
                menuItem.Callback?.Invoke(menuItem);
        }

        /// <summary>
        /// Raises the property changed event.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        private void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;

            changed?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}