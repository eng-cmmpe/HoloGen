﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.Utils;
using System.Collections.ObjectModel;

namespace HoloGen.UI.Hamburger.ViewModels
{
    /// <summary>
    /// For the top-level of a <see cref="Option" />s hierarchy, simple databinding isn't sufficient
    /// to work with simple data binding. Instead the <see cref="HierarchyPage" />s are wrapped in
    /// a <see cref="HamburgerItem" /> or <see cref="HamburgerMenuItem" /> and the
    /// <see cref="HamburgerTabViewModel" /> used to join the two.
    /// </summary>
    public class HamburgerTabViewModel : ObservableObject
    {
        private HierarchyRoot _hierarchyRoot;

        private SearchBoxViewModel _searchBoxViewModel;

        public HamburgerTabViewModel(HierarchyRoot childSource)
        {
            HierarchyRoot = childSource;
            SearchBoxViewModel = new SearchBoxViewModel(HierarchyRoot);
        }

        public HierarchyRoot HierarchyRoot
        {
            get => _hierarchyRoot;
            set => SetProperty(ref _hierarchyRoot, value);
        }

        public SearchBoxViewModel SearchBoxViewModel
        {
            get => _searchBoxViewModel;
            set => SetProperty(ref _searchBoxViewModel, value);
        }

        public ObservableCollection<HamburgerMenuItem> OptionsMenu { get; } = new ObservableCollection<HamburgerMenuItem>();
    }
}