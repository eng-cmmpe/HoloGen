﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Alg.Base.Managed;
using HoloGen.Utils;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Numerics;

namespace HoloGen.Image
{
    /// <summary>
    /// Cache for <see cref="ComplexImage" /> objects for defined <see cref="ImageScaleType" />s,
    /// <see cref="TransformType" />s and <see cref="ColorScheme" />s
    /// </summary>
    public class ImageCache
    {
        private readonly GenericCache<TransformType, ImageViewType, ColorScheme, ImageScaleType, ComplexImage, TransformImage, FactoredImage, ColoredImage, ImageView> _cache;
        private readonly Scale _customScale;

        private readonly bool _ignoreCentrePixelsWhenScaling;

        private LoggerDelegate _loggerDelegate;

        public ImageCache(ComplexImage image) : this(image, true, null)
        {
        }

        public ImageCache(ComplexImage image, bool ignoreCentrePixelsWhenScaling, Scale customScale)
        {
            _customScale = customScale;
            _ignoreCentrePixelsWhenScaling = ignoreCentrePixelsWhenScaling;
            _cache = new GenericCache<TransformType, ImageViewType, ColorScheme, ImageScaleType, ComplexImage, TransformImage, FactoredImage, ColoredImage, ImageView>(
                image,
                CreateTransform,
                CreateImageView,
                CreateColorScheme,
                CreateScale);
        }

        public ImageView GetAppropriateImage(TransformType key1, ImageViewType key2, ColorScheme key3, ImageScaleType key4)
        {
            return _cache.GetCachedValue(key1, key2, key3, key4);
        }

        private void Log(string msg)
        {
            // Ignore logged messages
        }

        private TransformImage CreateTransform(TransformType key, ComplexImage image)
        {
            _loggerDelegate = Log;
            switch (key)
            {
                case TransformType.None:
                    return new TransformImage(image.Values);
                case TransformType.FFT:
                    return new TransformImage(FFTHandlerMan.FFT2WithShift(image.Values, _loggerDelegate, FFTDirection.Forward));
                case TransformType.IFFT:
                    return new TransformImage(FFTHandlerMan.FFT2WithShift(image.Values, _loggerDelegate, FFTDirection.Inverse));
                case TransformType.FFTShift:
                    return new TransformImage(FFTHandlerMan.FFTShift(image.Values, _loggerDelegate));
                default:
                    return new TransformImage(image.Values);
            }
        }

        private FactoredImage CreateImageView(ImageViewType key, TransformImage image)
        {
            switch (key)
            {
                case ImageViewType.Real:
                    return CreateImageView(value => value.Real, image);
                case ImageViewType.Imaginary:
                    return CreateImageView(value => value.Imaginary, image);
                case ImageViewType.Magnitude:
                    return CreateImageView(value => value.Magnitude, image);
                case ImageViewType.Phase:
                    return CreateImageView(value => value.Phase, image);
                default:
                    return CreateImageView(value => value.Real, image);
            }
        }

        private FactoredImage CreateImageView(Func<Complex, double> updater, TransformImage image)
        {
            var values = new double[image.Values.GetLength(0), image.Values.GetLength(1)];

            var min = double.MaxValue;
            var max = double.MinValue;

            var lenY = image.Values.GetLength(0);
            var lenX = image.Values.GetLength(1);

            // Optionally ignore the middle 4 - 9 pixels at the centre of the image
            var ignoreX1 = _ignoreCentrePixelsWhenScaling ? lenX / 2 - 10 : int.MaxValue;
            var ignoreX2 = _ignoreCentrePixelsWhenScaling ? lenX - lenX / 2 + 10 : int.MinValue;
            var ignoreY1 = _ignoreCentrePixelsWhenScaling ? lenY / 2 - 10 : int.MaxValue;
            var ignoreY2 = _ignoreCentrePixelsWhenScaling ? lenY - lenY / 2 + 10 : int.MinValue;

            for (var y = 0; y < image.Values.GetLength(0); y++)
            for (var x = 0; x < image.Values.GetLength(1); x++)
            {
                var value = updater(image.Values[y, x]);
                if (!((x >= ignoreX1) & (x <= ignoreX2) & (y >= ignoreY1) & (y <= ignoreY2)))
                {
                    min = Math.Min(min, value);
                    max = Math.Max(max, value);
                }

                values[y, x] = value;
            }

            return new FactoredImage(values, _customScale ?? new Scale(min, max));
        }

        private ColoredImage CreateColorScheme(ColorScheme key, FactoredImage image)
        {
            var bitmap = new Bitmap(image.Values.GetLength(1), image.Values.GetLength(0));
            var bitmapData = bitmap.LockBits(
                new Rectangle(0, 0, image.Values.GetLength(1), image.Values.GetLength(0)),
                ImageLockMode.ReadWrite,
                PixelFormat.Format24bppRgb
            );
            var bitmapBytes = new byte[Math.Abs(bitmapData.Stride) * bitmapData.Height];

            long location = 0;
            for (var y = 0; y < image.Values.GetLength(0); y++)
            for (var x = 0; x < image.Values.GetLength(1); x++)
            {
                var color = key.Interpolate((image.Values[y, x] - image.Scale.MinValue) / (image.Scale.MaxValue - image.Scale.MinValue));

                // WARNING: GDI+ Uses the order blue, green, red!
                bitmapBytes[location + 2] = color.R;
                bitmapBytes[location + 1] = color.G;
                bitmapBytes[location + 0] = color.B;
                location += 3;
            }

            System.Runtime.InteropServices.Marshal.Copy(bitmapBytes, 0, bitmapData.Scan0, Math.Abs(bitmapData.Stride) * bitmapData.Height);
            bitmap.UnlockBits(bitmapData);

            return new ColoredImage(image.Values, bitmap, image.Scale, key);
        }

        private ImageView CreateScale(ImageScaleType key, ColoredImage image)
        {
            return new ImageView(
                ScaleAdder.AddScale(
                    image.Bitmap.Clone(new Rectangle(0, 0, image.Values.GetLength(1), image.Values.GetLength(0)), image.Bitmap.PixelFormat),
                    key,
                    image.ColorScheme,
                    image.Scale),
                image.Bitmap.Clone(new Rectangle(0, 0, image.Values.GetLength(1), image.Values.GetLength(0)), image.Bitmap.PixelFormat),
                image.Values,
                new Dimension(image.Values.GetLength(1), image.Values.GetLength(0)),
                new Scale(Math.Min(image.Scale.MinValue,0), Math.Max(image.Scale.MaxValue,0)), 
                image.ColorScheme);
        }

        private class TransformImage
        {
            public TransformImage(Complex[,] values)
            {
                Values = values;
            }

            public Complex[,] Values { get; }
        }

        private class FactoredImage
        {
            public FactoredImage(double[,] values, Scale scale)
            {
                Values = values;
                Scale = scale;
            }

            public double[,] Values { get; }

            public Scale Scale { get; }
        }

        private class ColoredImage
        {
            public ColoredImage(double[,] values, Bitmap bitmap, Scale scale, ColorScheme colorScheme)
            {
                Values = values;
                Bitmap = bitmap;
                Scale = scale;
                ColorScheme = colorScheme;
            }

            public double[,] Values { get; }

            public Bitmap Bitmap { get; }

            public Scale Scale { get; }

            public ColorScheme ColorScheme { get; }
        }
    }
}