# README

_**Warning! HoloGen is a work in progress developed as a tool to aid my (Peter Christopher) PhD. It is comparitively stable in the areas that I needed it for but there are areas where it is not yet feature complete. Use this at your own risk!**_

## Getting Started
### Installation

To install HoloGen download [setup.exe](https://gitlab.com/hologen/HoloGen/raw/master/Publish/setup.exe) from the following folder

> https://gitlab.com/CMMPEOpenAccess/HoloGen/raw/master/Publish/setup.exe

Run setup.exe to install the application and follow the steps through.

* **Note:** HoloGen is currently unsigned, that means that Windows is likely to complain. I promise there aren't any viruses but if you are concerned, download and build yourself! :-)
* **Note:** You may be asked to install some pre-requisite materials. Click _Accept_ and continue.
* **Note:** HoloGen will only run on 64 bit windows machines and is unable to generate holograms without an NVidia graphics card.

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture003.PNG?raw=true "HoloGen deployment folder")
</details>

### Menus

Load HoloGen from the start menu or by double clicking on the desktop shortcut. When you load first load HoloGen you should see a screen similar to the below.

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture100.PNG?raw=true "HoloGen welcome screen")
</details>

You can load the application menu by clicking on the menu *hamburger* icon in the top left of the screen. You should see a screen similar to that below. The *File* menu contains all the necessary commands for importing and exporting data from HoloGen. Click on *New Setup File* to open a new HoloGen setup file.

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture101.PNG?raw=true "HoloGen file menu")
</details>

If you are lost at any time you can open the help documentation from the *Help* menu.

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture102.PNG?raw=true "HoloGen help menu")
</details>

You can change application settings in the *Settings* menu.

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture103.PNG?raw=true "HoloGen settings menu")
</details>

You can change the application accent colours and theme in the *Accents* and *Themes* menus.

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture104.PNG?raw=true "HoloGen accents menu - 1")
</details>
<details>
  <summary>Screenshot</summary>
  ![Home page](igures/Capture105.PNG?raw=true "HoloGen accents menu - 2")
</details>

### Setup

The computer hologram generation parameter setup file looks like this.

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture106.PNG?raw=true "HoloGen setup algorithm options")
</details>

A discussion of the individual parameters is beyond the scope of this reference though brief descriptions are given in tooltips. Refer to the published thesis titled "High Rate Additive Manufacture using Holographic Beam Shaping" which is available upon request from the author. 

Incorrect parameter values are highlighted.

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture107.PNG?raw=true "HoloGen error reporting - 1")
</details>

A full list of errors is also shown in the output tab.

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture108.PNG?raw=true "HoloGen error reporting - 2")
</details>

Parameters for the system projector, targets and hardware have their own pages.

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture109.PNG?raw=true "HoloGen setup projector options")
</details>
<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture110.PNG?raw=true "HoloGen setup target options")
</details>
<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture111.PNG?raw=true "HoloGen setup hardware options")
</details>

### Image Viewer

HoloGen has a built in image viewer for images of complex values, for example Lenna.

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture112.PNG?raw=true "HoloGen image viewer")
</details>

Simple transformations can be run directly in the view and a range of visualisation options are available. For example the phase distribution of the fast fourier transform..

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture113.PNG?raw=true "HoloGen image viewer transforms")
</details>

The color scheme can also be changed.

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture114.PNG?raw=true "HoloGen image viewer colour schemes")
</details>

Clicking on the *View Style* drop down allows you to select a 3D visualisation of the image.

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture115.PNG?raw=true "HoloGen 3D image viewer")
</details>

You can also choose regions of interest by drawing on the image.

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture116.PNG?raw=true "HoloGen image masking")
</details>

### Running a setup file

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture117.PNG?raw=true "HoloGen process view")
</details>

## How Do I?
### Change the Language

HoloGen comes with a number of built in translations including:

* **French** - *fr* - Translation courtesy of Ralf Mouthaan
* **German** - *de* - Translation courtesy of Sophia Gruber
* **Chinese** - *zh* - Translation courtesy of Daoming Dong
* **English** - *en* - Edition courtesy of Victoria Barrett

Where the operating system is already using one of these languages, HoloGen will automatically switch. If desired the application language can be chosen from the command line. To do this:

* Open the application shortcut folder.
* Run the application with appropriate command line arguments. For example, using command line argument *fr* will load the French version of HoloGen

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture002.PNG?raw=true "Loading HoloGen from the command line")
</details>

<details>
  <summary>Screenshot</summary>
  ![Home page](images/steps/step001.PNG?raw=true "Home page")
</details>

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture001.PNG?raw=true "Loading HoloGen from the command line")
</details>

### Import and Export Data

You can import and export data using the *Import/Export* submenu of the *File* menu . Hover over individual menu items when in doubt about their use.

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture119.PNG?raw=true "HoloGen data import and export")
</details>

### Generate a  Hologram?

To generate a hologram, load a *.hfimg* file. You can import this as described above. The key settings you will need to change are the *target image* on the *Targets* page and the SLM resolutions on the *Projector* page. Refer to the published thesis titled "High Rate Additive Manufacture using Holographic Beam Shaping" for details of other parameters. 

Incorrect parameter values are highlighted.

### View Metadata

Images generated using the HoloGen computer generation algorithms are saved with the generation parameters in the metadata. Clicking on the *Show Original Parameters* button to view them.

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture118.PNG?raw=true "HoloGen metadata")
</details>

### Batch Processing

Holograms can also be batch generated. The parameters on the left are all the available algorithm parameters. Using the buttons on the top-left allows for adding rows and columns to the batch processing data grid. Once complete, press *Run* to run.

<details>
  <summary>Screenshot</summary>
  ![Home page](Documentation/Figures/Capture121.PNG?raw=true "HoloGen metadata")
</details>

## Further Information

### Contributing

To contribute you can clone the repo and start building. This should be relatively straight forward but if you want to know more, then contact Peter Christopher on pjc209@cam.ac.uk or peterjchristopher@gmail.com.

Contributions to the project are greatly welcome! The project is hosted on the CMMPE GitLab at:

> https://gitlab.com/CMMPEOpenAccess/HoloGen/

## Learn More
* [Introduction to HoloGen](https://gitlab.com/hologen/HoloGen/blob/master/Documentation/Welcome.pdf)
* [HoloGen Manual](https://gitlab.com/hologen/HoloGen/blob/master/Documentation/Help.pdf)
* [Legal Stuff](https://gitlab.com/hologen/HoloGen/blob/master/LICENSE)

## FAQ

### How do I publish?

The following steps:

- Rebuild the solution. A normal build is not necessarily enough to ensure that everything links properly. Thanks Cuda!
- Update the version number in the HoloGenUI AssemblyInfo.cs
- Update the version number in the HoloGenUI properties project tab
- Click "Publish Now"
- Push changes (or a touch) to the gitlab repository

### How do I change the language?

- Add it as a command line argument to the launch command. 
- If you don't know how to do this then Google it!
- Currently supports "-de", "-en", "-es", "-fr" and "-zh" and defaults to "-en".

## Notes

### File Endings

In the Cuda code, *.h is a header compilable/linkable with a standard compiler. *.cuh has includes that need to be compiled with a Cuda compiler.

### Native code hierarchy

AlgorithmController in C# calls AlgorithmXXMan in Managed C++ (which extends AlgorithmMan) which is in a managed library. This calls AlgorithmXXWrap in C++0X which has a shared pointer to AlgorithmXXCuda which is in Cuda/C++

## License

Copyright 2019 Peter J. Christopher

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk