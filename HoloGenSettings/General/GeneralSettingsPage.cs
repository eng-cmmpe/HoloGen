﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

using System.Threading;
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.Settings.General.Folder;
using HoloGen.Settings.General.Warnings;
using MahApps.Metro.IconPacks;

namespace HoloGen.Settings.General
{
    /// <summary>
    /// HoloGen application settings
    /// </summary>
    public sealed class GeneralSettingsPage : HierarchyPage
    {
        public override object Icon { get; } = System.Threading.Thread.CurrentThread.GetApartmentState() != ApartmentState.STA ? null : new PackIconMaterial {Kind = PackIconMaterialKind.Numeric1Box};

        public override string Name => Resources.Properties.Resources.GeneralSettingsPage_Settings;

        public override string ToolTip => Resources.Properties.Resources.GeneralSettingsPage_GeneralSettings2;

        public FileSettingsFolder FileSettingsFolder { get; } = new FileSettingsFolder();
        public WarningSettingsFolder WarningSettingsFolder { get; } = new WarningSettingsFolder();
    }
}