// Copyright 2019 (C) Peter J. Christopher
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#include "../stdafx.h"
#pragma once

#include "AlgorithmMan.h"

using namespace System;
using namespace System::Numerics;
using namespace HoloGen::Alg::Base::Cuda;

// ReSharper disable CppExpressionWithoutSideEffects

array<Complex, 2>^ AlgorithmMan::Convert(std::vector<std::complex<float>> toConvert)
{
	// Todo: Make all of these reinterpretation casts.

	const size_t nx = SLMResolutionX;
	const size_t ny = SLMResolutionY;
	const size_t SIZE = toConvert.size();
	if (nx * ny != SIZE) throw gcnew Exception("Incompatible image sizes!");
	array<Complex, 2>^ tempArr = gcnew array<Complex, 2>(ny, nx);

	for (size_t y = 0; y < ny; y++)
	{
		for (size_t x = 0; x < nx; x++)
		{
			const auto i = y * nx + x;
			tempArr[y, x] = Complex(toConvert[i].real(), toConvert[i].imag());
		}
	}
	return tempArr;
}

std::vector<std::complex<float>> AlgorithmMan::Convert(array<Complex, 2>^ toConvert)
{
	// Todo: Make all of these reinterpretation casts.

	const size_t nx = SLMResolutionX;
	const size_t ny = SLMResolutionY;
	const size_t SIZE = toConvert->Length;
	if (nx * ny != SIZE) throw gcnew Exception("Incompatible image sizes!");
	auto tempVec = std::vector<std::complex<float>>(SIZE);

	for (size_t y = 0; y < ny; y++)
	{
		for (size_t x = 0; x < nx; x++)
		{
			const auto i = y * nx + x;
			tempVec[i] = std::complex<float>(toConvert[y, x].Real, toConvert[y, x].Imaginary);
		}
	}
	return tempVec;
}

array<int, 2>^ AlgorithmMan::Convert(std::vector<int> toConvert)
{
	// Todo: Make all of these reinterpretation casts.

	const size_t nx = SLMResolutionX;
	const size_t ny = SLMResolutionY;
	const size_t SIZE = toConvert.size();
	if (nx * ny != SIZE) throw gcnew Exception("Incompatible image sizes!");
	array<int, 2>^ tempArr = gcnew array<int, 2>(ny, nx);

	for (size_t y = 0; y < ny; y++)
	{
		for (size_t x = 0; x < nx; x++)
		{
			const auto i = y * nx + x;
			tempArr[y, x] = toConvert[i];
		}
	}
	return tempArr;
}

std::vector<int> AlgorithmMan::Convert(array<int, 2>^ toConvert)
{
	// Todo: Make all of these reinterpretation casts.

	const size_t nx = SLMResolutionX;
	const size_t ny = SLMResolutionY;
	const size_t SIZE = toConvert->Length;
	if (nx * ny != SIZE) throw gcnew Exception("Incompatible image sizes!");
	auto tempVec = std::vector<int>(SIZE);

	for (size_t y = 0; y < ny; y++)
	{
		for (size_t x = 0; x < nx; x++)
		{
			const auto i = y * nx + x;
			tempVec[i] = toConvert[y, x];
		}
	}
	return tempVec;
}

// ReSharper restore CppExpressionWithoutSideEffects
