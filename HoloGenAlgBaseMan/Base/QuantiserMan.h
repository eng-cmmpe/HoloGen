// Copyright 2019 (C) Peter J. Christopher
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once

#include <complex>
#include "../../HoloGenAlgBaseCuda/Base/Export/QuantiserWrap.h"
#include "../../HoloGenAlgBaseCuda/Base/Export/SLMModulationSchemeCuda.h"
#include "SLMModulationScheme.h"

using namespace System;
using namespace System::Numerics;
using namespace System::Collections::Generic;
using namespace HoloGen::Alg::Base::Cuda;
using namespace System::IO;
using namespace System::Runtime::InteropServices;

namespace HoloGen
{
	namespace Alg
	{
		namespace Base
		{
			namespace Managed
			{
				using namespace HoloGen::Alg::Base::Cuda;
				using namespace HoloGen::Alg::Base::Export;

				public delegate void LoggerDelegate(String^ msg);

				/// Top-level Quantiser wrapper in Managed C++
				public ref class QuantiserMan
				{
				private:

					QuantiserWrap* Wrap;

				protected:
					QuantiserMan(QuantiserWrap* wrap)
					{
						Wrap = wrap;
					}

				private:
					static SLMModulationSchemeCuda TranslateSLMModulationScheme(SLMModulationScheme input)
					{
						switch (input)
						{
						case HoloGen::Alg::Base::Managed::SLMModulationScheme::Amplitude:
							return SLMModulationSchemeCuda::Amplitude;
						case HoloGen::Alg::Base::Managed::SLMModulationScheme::Phase:
							return SLMModulationSchemeCuda::Phase;
						default:
							throw gcnew Exception("Unknown modulation scheme!");
						}
					}

				public:

					virtual ~QuantiserMan()
					{
						delete Wrap;
					}

					QuantiserWrap* GetNativePointer()
					{
						return Wrap;
					}

					void SetBaseParameters(
						float MaxSLMValue,
						float MinSLMValue,
						int levels,
						SLMModulationScheme ModulationScheme)
					{
						Wrap->SetBaseParameters(
							MaxSLMValue,
							MinSLMValue,
							levels,
							TranslateSLMModulationScheme(ModulationScheme));
					}

					void SetLogger(LoggerDelegate^ log)
					{
						Wrap->SetLogger(
							static_cast<LoggingCallback>(Marshal::GetFunctionPointerForDelegate(log).ToPointer()));
					}
				};
			}
		}
	}
}
