// Copyright 2019 (C) Peter J. Christopher
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once
#include <complex>
#include <vector>
#include "../../HoloGenAlgBaseCuda/Base/Export/FFTDirectionCuda.h"
#include "FFTDirection.h"
#include "AlgorithmMan.h"

using namespace System;
using namespace System::Numerics;
using namespace HoloGen::Alg::Base::Managed;
using namespace HoloGen::Alg::Base::Cuda;

namespace HoloGen
{
	namespace Alg
	{
		namespace Base
		{
			namespace Managed
			{
				/// Utility algorithm for handling Tests
				public ref class TestHandlerMan
				{
				public:

					static Int64 TestFFT(
						int resolution,
						int cycles,
						LoggerDelegate^ log,
						int testType
					);

				private:
					static array<Complex, 2>^ Convert(std::vector<std::complex<float>> toConvert, size_t nx, size_t ny);
					static std::vector<std::complex<float>> Convert(array<Complex, 2>^ toConvert);
				};
			}
		}
	}
}
