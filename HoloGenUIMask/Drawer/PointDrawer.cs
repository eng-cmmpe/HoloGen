﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Image;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using HoloGen.Utils;

namespace HoloGen.UI.Mask.Drawer
{
    /// <summary>
    /// <see cref="Drawer" /> for point shapes.
    /// </summary>
    public class PointDrawer : Drawer
    {
        public PointDrawer(Canvas canvas, MaskViewModel viewModel) : base(canvas, viewModel)
        {
            BindListBox();
        }

        public void Draw()
        {
        }

        public void BindListBox()
        {
        }

        public override void MouseMove(Point p)
        {
        }

        public override void MouseLeftButtonDown(Point p, bool doubleClick)
        {
            if (ViewModel.OperationMode == OperationMode.Drawing)
            {
                var pointDrawingBody = new Rectangle
                {
                    Fill = new SolidColorBrush(ThemeColors.AccentColor),
                    Width = 6,
                    Height = 6
                };

                pointDrawingBody.SetValue(Canvas.LeftProperty, p.X - 3);
                pointDrawingBody.SetValue(Canvas.TopProperty, p.Y - 3);

                Canvas.Children.Add(pointDrawingBody);

                ViewModel.DisplayableShapeCollection.Add(pointDrawingBody);
                ViewModel.FireShapeCollectionChanged();

                var points = new List<Point>
                {
                    p
                };
                ViewModel.SaveableShapeCollection.Add(new Tuple<ShapeType, List<Point>>(ShapeType.Point, points));

                BindListBox();
            }
        }
    }
}