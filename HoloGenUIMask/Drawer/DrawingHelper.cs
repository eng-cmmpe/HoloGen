﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows;
using HoloGen.UI.Mask.Model;

namespace HoloGen.UI.Mask.Drawer
{

    /// <summary>
    /// Modified from: https://wpfshapedemo.codeplex.com/
    /// </summary>
    public class ResourceCenter
    {
        public static DPolygon NewPolygon()
        {
            DPolygon pgBody = new DPolygon();
            pgBody.PolygonBody.Stroke = new SolidColorBrush(Colors.Black);
            pgBody.PolygonBody.StrokeThickness = 2;
            pgBody.PolygonBody.Fill = new SolidColorBrush(Colors.Blue);
            pgBody.PolygonBody.Opacity = 0.6;

            pgBody.PolygonBody.Points.Add(new Point());

            return pgBody;
        }

        public static DRect NewRect()
        {
            DRect rectBody = new DRect();
            rectBody.RectBody.Fill = new SolidColorBrush(Colors.Blue);
            rectBody.RectBody.Stroke = new SolidColorBrush(Colors.Black);
            rectBody.RectBody.StrokeThickness = 2;
            rectBody.RectBody.Opacity = 0.6;

            return rectBody;
        }

        public static DEllipse NewEllipse()
        {
            DEllipse ellipseBody = new DEllipse();
            ellipseBody.EllipseBody.Fill = new SolidColorBrush(Colors.Blue);
            ellipseBody.EllipseBody.Stroke = new SolidColorBrush(Colors.Black);
            ellipseBody.EllipseBody.StrokeThickness = 2;
            ellipseBody.EllipseBody.Opacity = 0.6;

            return ellipseBody;
        }
    }
}
