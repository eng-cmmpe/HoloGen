﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace HoloGen.UI.Mask.Model
{
    /// <summary>
    /// Modified from: https://wpfshapedemo.codeplex.com/
    /// </summary>
    public class DPolygon : DShape
    {
        public Polygon PolygonBody { get; set; }

        public DPolygon()
        {
            CommonProperty = new CommonProperty();

            PolygonBody = new Polygon();
        }
    }
}
