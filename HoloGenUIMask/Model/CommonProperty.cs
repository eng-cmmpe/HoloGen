﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoloGen.UI.Mask.Model
{
    public class CommonProperty
    {
        public string Index { get; set; }
        public string Content { get; set; }
    }
}
