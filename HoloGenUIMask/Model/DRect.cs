﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace HoloGen.UI.Mask.Model
{
    /// <summary>
    /// Modified from: https://wpfshapedemo.codeplex.com/
    /// </summary>
    public class DRect : DShape
    {
        public Rectangle RectBody { get; set; }

        public Point P1 { get; set; }               //左上角的点
        public Point P2 { get; set; }               //右下角的点

        public DRect()
        {
            CommonProperty = new CommonProperty();

            RectBody = new Rectangle();
            P1 = new Point();
            P2 = new Point();
        }
        
        public double Width
        {
            get
            {
                return Math.Abs(P1.X - P2.X);
            }
            set
            {
                RectBody.Width = value;
            }
        }
        
        public double Height
        {
            get
            {
                return Math.Abs(P1.Y - P2.Y);
            }
            set
            {
                RectBody.Height = value;
            }
        }
        
        public double Top
        {
            get
            {
                if (P1.Y < P2.Y)
                {
                    return P1.Y;
                }
                else
                {
                    return P2.Y;
                }
            }
            set
            {
                RectBody.SetValue(Canvas.TopProperty, value);
            }
        }
        
        public double Left
        {
            get
            {
                if (P1.X < P2.X)
                {
                    return P1.X;
                }
                else
                {
                    return P2.X;
                }
            }
            set
            {
                RectBody.SetValue(Canvas.LeftProperty, value);
            }
        }
    }
}
