﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Controller;
using HoloGen.Options;
using HoloGen.Process.Options;
using HoloGen.UI.Graph.ViewModels;
using HoloGen.UI.Hamburger.ViewModels;
using HoloGen.Utils;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;
using HoloGen.Alg.Base.Managed;
using HoloGen.UI.Process.Monitor.Views;

namespace HoloGen.UI.Process.Monitor.ViewModels
{
    /// <summary>
    /// ViewModel for the <see cref="ProcessView" /> control.
    /// </summary>
    public class ProcessViewModel : ObservableObject
    {
        private readonly Action<Task<Complex[,]>> _onCompletion;

        private AlgorithmController _algorithmController;

        private object _icon;

        private int _id;

        private string _message = default(string);

        private string _name = default(string);

        private ProcessOptions _options;

        private bool _queued = true;

        private SearchBoxViewModel _searchBoxViewModel;
        public List<Tuple<DateTime, Dictionary<MetricType, float>>> AllMetrics = new List<Tuple<DateTime, Dictionary<MetricType, float>>>();

        public ProcessViewModel(OptionsRoot options, Action<Task<Complex[,]>> onCompletion)
        {
            SetupOptions = options;
            _onCompletion = onCompletion;

            SearchBoxViewModel = new SearchBoxViewModel(Options);
        }

        public ProcessOptions Options => _options ?? (_options = new ProcessOptions());

        public OptionsRoot SetupOptions { get; }

        public GraphViewModel GraphViewModel { get; } = new GraphViewModel();

        public string Message
        {
            get => _message;
            set => SetProperty(ref _message, value);
        }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public object Icon
        {
            get => _icon;
            set => SetProperty(ref _icon, value);
        }

        public int ID
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        public bool Queued
        {
            get => _queued;
            set => SetProperty(ref _queued, value);
        }

        public SearchBoxViewModel SearchBoxViewModel
        {
            get => _searchBoxViewModel;
            set => SetProperty(ref _searchBoxViewModel, value);
        }

#pragma warning disable 1998
#pragma warning disable 4014
        public async void StartProcess(Action<string> logger)
        {
            Queued = false;
            try
            {
                _algorithmController = new AlgorithmController(
                    SetupOptions,
                    logger,
                    NewMetrics);
            }
            catch (Exception e)
            {
                logger(">>>>> Exception thrown! <<<<<");
                logger("Message: " + e.Message);
                return;
            }

            _algorithmController.ContinueWith(_onCompletion);
            _algorithmController.Start();
        }
#pragma warning restore 4014
#pragma warning restore 1998

        private void NewMetrics(Dictionary<MetricType, float> metrics)
        {
            var now = DateTime.Now;
            AllMetrics.Add(new Tuple<DateTime, Dictionary<MetricType, float>>(now, metrics));
            if (AllMetrics.Count == 0) GraphViewModel.SetAxisLimits(now);

            TimeSpan? diff = null;
            if (GraphViewModel.ChartValues.Count >= 2)
            {
                var old = GraphViewModel.ChartValues[GraphViewModel.ChartValues.Count - 1].DateTime;
                diff = now - old;
            }

            var item = new GraphItemViewModel
            {
                DateTime = now,
                Values = metrics
            };
            GraphViewModel.ChartValues.Add(item);
            GraphViewModel.NotifyIsHot();
            if (diff.HasValue)
                GraphViewModel.IncrementAxisLimits(diff.Value);

            //if (GraphViewModel.ChartValues.Count > 250) GraphViewModel.ChartValues.RemoveAt(0);
        }

        ~ProcessViewModel()
        {
            _algorithmController.Dispose();
            var curProcess = System.Diagnostics.Process.GetCurrentProcess();
            curProcess.StartInfo.RedirectStandardOutput = false;
        }
    }
}