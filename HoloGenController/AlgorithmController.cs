﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Alg.Base.Managed;
using HoloGen.Alg.DS.Managed;
using HoloGen.IO;
using HoloGen.Options;
using HoloGen.Options.Algorithm.Algorithms.Type;
using HoloGen.Options.Algorithm.Quant.Type;
using HoloGen.Options.Algorithm.Termination.Type;
using HoloGen.Options.Projector.Hologram;
using HoloGen.Options.Projector.Illumination.Type;
using HoloGen.Options.Target.Region.Type;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Numerics;
using System.Threading.Tasks;
using HoloGen.Alg.GS.Managed;
using HoloGen.Alg.SA.Managed;
using HoloGen.Options.Algorithm.Algorithms.Variants.DS;
using HoloGen.Options.Algorithm.Algorithms.Variants.GS;
using HoloGen.Options.Algorithm.Algorithms.Variants.SA;
using HoloGen.Utils.ExtensionMethods;

namespace HoloGen.Controller
{
    /// <summary>
    /// Main algorithm controller class. Packages the options into the correct locations
    /// and then runs the appropriate algorithms.
    /// </summary>
    public class AlgorithmController : IDisposable
    {
        private readonly Task<Complex[,]> _task;

        public readonly AlgorithmMan Alg;

        private readonly LoggerDelegate _loggerDelegate;

        public readonly QuantiserMan Quantiser;

        public Action<string> Logger;

        public Action<Dictionary<MetricType, float>> NewMetrics;

        private Random _rnd = new Random();

        private static Dictionary<string, Tuple<Complex[,], int[,]>> CachedImages = new Dictionary<string, Tuple<Complex[,], int[,]>>();

        public AlgorithmController(OptionsRoot options, Action<string> logger, Action<Dictionary<MetricType, float>> newMetrics)
        {
            Logger = logger;
            _loggerDelegate = Log;
            NewMetrics = newMetrics;
            var success = true;
            _task = new Task<Complex[,]>(() => RunFunc(options));

            Alg = CreateBaseAlgorithm(options);
            if (Alg == null)
                throw new Exception("Error creating algorithm");

            Alg.SetLogger(_loggerDelegate);

            Quantiser = SelectQuantiser(options);
            if (Quantiser == null)
                throw new Exception("Error creating quantiser");
            Alg.SetQuantiser(Quantiser);

            success &= LoadImages(Alg, options);
            if (!success)
                throw new Exception("Error loading images");
        }

        private void Log(string msg)
        {
            Logger(msg);
        }

        public void Start()
        {
            _task.Start();
        }

        public Task ContinueWith(Action<Task<Complex[,]>> action)
        {
            return _task.ContinueWith(action);
        }

        public Complex[,] RunFunc(OptionsRoot options)
        {
            var terminationOption = options.AlgorithmsPage.TerminationFolder.TerminationOption.Value;
            var terminateOnError = terminationOption is TerminationFirst || terminationOption is TerminationError;
            var terminateOnIterations = terminationOption is TerminationFirst || terminationOption is TerminationIterations;
            var terminationError = -1.0;
            terminationError = terminationOption is TerminationError ? options.AlgorithmsPage.TerminationFolder.TerminationOption.Possibilities2.TerminationError.ErrorValue.Value : terminationError;
            terminationError = terminationOption is TerminationFirst ? options.AlgorithmsPage.TerminationFolder.TerminationOption.Possibilities2.TerminationFirst.ErrorValue.Value : terminationError;
            var terminationIterations = -1;
            terminationIterations = terminationOption is TerminationIterations ? options.AlgorithmsPage.TerminationFolder.TerminationOption.Possibilities2.TerminationIterations.NumIterations.Value : terminationIterations;
            terminationIterations = terminationOption is TerminationFirst ? options.AlgorithmsPage.TerminationFolder.TerminationOption.Possibilities2.TerminationFirst.NumIterations.Value : terminationIterations;
            var reportingSteps = options.OutputPage.RunFolder.ShowResults.Value ? options.OutputPage.RunFolder.ShowResults.ResultEvery.Value : -1;
            
            var stopIterating = false;
            int currentIterationCount = 0;

            try
            {
                Logger("Running startup...");
                var success = Alg.RunStartup();
                if(success != 0)
                    Logger("Running startup... FAILED!");

                while(!stopIterating)
                {
                    Logger($"Running iteration block {currentIterationCount+1}-{currentIterationCount + reportingSteps} of {terminationIterations}...");
                    var metrics = Alg.RunIterations();
                    NewMetrics(metrics);

                    currentIterationCount += reportingSteps;
                    stopIterating = (terminateOnIterations && currentIterationCount >= terminationIterations) ||
                                    (terminateOnError && metrics[MetricType.MeanSquaredError] <= terminationError);
                }

                Logger("Running cleanup...");
                success = Alg.RunCleanup();
                if (success != 0) Logger("Running cleanup... FAILED!");

                Logger("Getting result...");
                var result = Alg.GetResult();

                return result;
            }
            catch (Exception e)
            {
                Logger(">>>>> Exception thrown! <<<<<");
                Logger("Message: " + e.Message);
                return null;
            }
        }

        /// <summary>
        /// F**k this, let's break every rule of OOP ever here!
        /// More seriously, this is a nasty class. Sorry. :-(
        /// It was the only way to maintain the distinctive levels I wanted.
        /// Basically, we load all the file references and then translate the options
        /// hierarchy into a single elvel thing that we can pass in and out of Cuda.
        /// </summary>
        private AlgorithmMan CreateBaseAlgorithm(OptionsRoot options)
        {
            Logger("Creating base algorithm object...");

            AlgorithmMan alg;

            switch (options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Value)
            {
                case DSAlgorithm option:
                    alg = CreateDSAlgorithm(option);
                    break;
                case GSAlgorithm option:
                    alg = CreateGSAlgorithm(option);
                    break;
                case SAAlgorithm option:
                    alg = CreateSAAlgorithm(option);
                    break;
                case OSPRAlgorithm _:
                    Logger(">>>>> Unsupported algorithm type <<<<<");
                    return null;
                default:
                    Logger(">>>>> Unsupported algorithm type <<<<<");
                    return null;
            }

            if (alg != null)
                SetBaseAlgorithmParameters(alg, options);

            return alg;
        }

        private AlgorithmMan CreateDSAlgorithm(DSAlgorithm options)
        {
            Logger("Creating mid-level DS algorithm object...");

            switch (options.VariantOption.VariantPossibility)
            {
                case StandardDSVariant _:
                    return CreateDSStandardAlgorithm(options);
                case FastDSVariant _:
                    return CreateDSFastAlgorithm(options);
                default:
                    Logger(">>>>> Unsupported variant type <<<<<");
                    return null;
            }
        }

        private DSAlgorithmStandardMan CreateDSStandardAlgorithm(DSAlgorithm options)
        {
            Logger("Creating variant-level DS algorithm object...");

            var alg = new DSAlgorithmStandardMan();

            alg.SetAlgorithmParameters(
                options.VariantOption2.Possibilities2.StandardDSVariant.NumPixelsToSwitch.Value);

            return alg;
        }

        private DSAlgorithmFastMan CreateDSFastAlgorithm(DSAlgorithm options)
        {
            Logger("Creating variant-level DS algorithm object...");

            var alg = new DSAlgorithmFastMan();

            alg.SetAlgorithmParameters();

            return alg;
        }
        
        private AlgorithmMan CreateGSAlgorithm(GSAlgorithm options)
        {
            Logger("Creating mid-level GS algorithm object...");

            switch (options.VariantOption.VariantPossibility)
            {
                case StandardGSVariant _:
                    return CreateGSStandardAlgorithm(options);
                default:
                    Logger(">>>>> Unsupported variant type <<<<<");
                    return null;
            }
        }

        private GSAlgorithmStandardMan CreateGSStandardAlgorithm(GSAlgorithm options)
        {
            Logger("Creating variant-level GS algorithm object...");

            var alg = new GSAlgorithmStandardMan();

            //alg.SetAlgorithmParameters(
            //    options.NumPixelsToSwitch.Value);

            return alg;
        }
        
        private AlgorithmMan CreateSAAlgorithm(SAAlgorithm options)
        {
            Logger("Creating mid-level SA algorithm object...");

            switch (options.VariantOption.VariantPossibility)
            {
                case StandardSAVariant _:
                    return CreateSAStandardAlgorithm(options);
                case FastSAVariant _:
                    return CreateSAFastAlgorithm(options);
                default:
                    Logger(">>>>> Unsupported variant type <<<<<");
                    return null;
            }
        }

        private SAAlgorithmStandardMan CreateSAStandardAlgorithm(SAAlgorithm options)
        {
            Logger("Creating variant-level SA algorithm object...");

            var alg = new SAAlgorithmStandardMan();

            alg.SetAlgorithmParameters(
                options.VariantOption2.Possibilities2.StandardSAVariant.NumPixelsToSwitch.Value,
                (float) options.StartingTemperature.Value,
                (float) options.TemperatureCoefficient.Value,
                (int) options.ReferenceIterationsCount.Value);

            return alg;
        }

        private SAAlgorithmFastMan CreateSAFastAlgorithm(SAAlgorithm options)
        {
            Logger("Creating variant-level SA algorithm object...");

            var alg = new SAAlgorithmFastMan();

            alg.SetAlgorithmParameters(
                (float) options.StartingTemperature.Value,
                (float) options.TemperatureCoefficient.Value,
                (int) options.ReferenceIterationsCount.Value);

            return alg;
        }

        private void SetBaseAlgorithmParameters(AlgorithmMan alg, OptionsRoot options)
        {
            Logger("Passing common base algorithm parameters...");
            var terminationOption = options.AlgorithmsPage.TerminationFolder.TerminationOption.Value;
            var terminationError = -1.0;
            terminationError = terminationOption is TerminationError ? options.AlgorithmsPage.TerminationFolder.TerminationOption.Possibilities2.TerminationError.ErrorValue.Value : terminationError;
            terminationError = terminationOption is TerminationFirst ? options.AlgorithmsPage.TerminationFolder.TerminationOption.Possibilities2.TerminationFirst.ErrorValue.Value : terminationError;
            var terminationIterations = -1;
            terminationIterations = terminationOption is TerminationIterations ? options.AlgorithmsPage.TerminationFolder.TerminationOption.Possibilities2.TerminationIterations.NumIterations.Value : terminationIterations;
            terminationIterations = terminationOption is TerminationFirst ? options.AlgorithmsPage.TerminationFolder.TerminationOption.Possibilities2.TerminationFirst.NumIterations.Value : terminationIterations;
            var reportingSteps = options.OutputPage.RunFolder.ShowResults.Value ? options.OutputPage.RunFolder.ShowResults.ResultEvery.Value : -1;

            alg.SetBaseParameters(
                options.ProjectorPage.HologramFolder.SLMResolutionX.Value,
                options.ProjectorPage.HologramFolder.SLMResolutionY.Value,
                terminationIterations,
                (float) terminationError,
                reportingSteps,
                options.TargetPage.TargetFolder.RandomiseTargetPhase.Value,
                options.TargetPage.TargetFolder.ScaleTargetImage.Value,
                options.TargetPage.TargetFolder.ScaleTargetImage.TieNormalisationToZero.Value,
                options.ProjectorPage.IlluminationFolder.IlluminationOption.Value.IlluminationType,
                options.ProjectorPage.HologramFolder.SeedOption.Value.InitialSeedType,
                (float) options.ProjectorPage.IlluminationFolder.IlluminationPower.Value);
        }

        private void GetSLMModulationScheme(
            OptionsRoot options,
            out float maxSlmValue,
            out float minSlmValue,
            out SLMModulationScheme modulationScheme,
            out int levels)
        {
            if (options.ProjectorPage.HologramFolder.SLMTypeOption.Value is BinaryAmpSLM)
            {
                maxSlmValue = (float) options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.BinaryAmpSLM.MaximumLevel.Value;
                minSlmValue = (float) options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.BinaryAmpSLM.MinimumLevel.Value;
                modulationScheme = SLMModulationScheme.Amplitude;
                levels = 2;
            }
            else if (options.ProjectorPage.HologramFolder.SLMTypeOption.Value is MultiAmpSLM)
            {
                maxSlmValue = (float) options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiAmpSLM.MaximumLevel.Value;
                minSlmValue = (float) options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiAmpSLM.MinimumLevel.Value;
                modulationScheme = SLMModulationScheme.Amplitude;
                levels = options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiAmpSLM.SLMLevels.Value;
            }
            else if (options.ProjectorPage.HologramFolder.SLMTypeOption.Value is ContinuousAmpSLM)
            {
                maxSlmValue = (float) options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.ContinuousAmpSLM.MaximumLevel.Value;
                minSlmValue = (float) options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.ContinuousAmpSLM.MinimumLevel.Value;
                modulationScheme = SLMModulationScheme.Amplitude;
                levels = int.MaxValue;
            }
            else if (options.ProjectorPage.HologramFolder.SLMTypeOption.Value is BinaryPhaseSLM)
            {
                maxSlmValue = (float) options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.BinaryPhaseSLM.MaximumAngle.Value;
                minSlmValue = (float) options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.BinaryPhaseSLM.MinimumAngle.Value;
                modulationScheme = SLMModulationScheme.Phase;
                levels = 2;
            }
            else if (options.ProjectorPage.HologramFolder.SLMTypeOption.Value is MultiPhaseSLM)
            {
                maxSlmValue = (float) options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiPhaseSLM.MaximumAngle.Value;
                minSlmValue = (float) options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiPhaseSLM.MinimumAngle.Value;
                modulationScheme = SLMModulationScheme.Phase;
                levels = options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiPhaseSLM.SLMLevels.Value;
            }
            else if (options.ProjectorPage.HologramFolder.SLMTypeOption.Value is ContinuousPhaseSLM)
            {
                maxSlmValue = (float) options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.ContinuousPhaseSLM.MaximumAngle.Value;
                minSlmValue = (float) options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.ContinuousPhaseSLM.MinimumAngle.Value;
                modulationScheme = SLMModulationScheme.Phase;
                levels = int.MaxValue;
            }
            else
            {
                throw new Exception("Unknown modulation type");
            }

            if (maxSlmValue < minSlmValue)
                Swap(ref maxSlmValue, ref minSlmValue);
        }
        
        private bool LoadImages(AlgorithmMan alg, OptionsRoot options)
        {
            var slmResolutionX = options.ProjectorPage.HologramFolder.SLMResolutionX.Value;
            var slmResolutionY = options.ProjectorPage.HologramFolder.SLMResolutionY.Value;

            var regionFromTarget = options.TargetPage.RegionFolder.RegionOption.Value is RegionFromTarget;
            var regionAutoExpand = options.TargetPage.RegionFolder.RegionOption.Value is RegionAutoExpand;
            var regionIntensity = options.TargetPage.RegionFolder.RegionOption.Value is RegionIntensity;
            var regionFromFile = options.TargetPage.RegionFolder.RegionOption.Value is RegionFromFile;
            var regionNone = options.TargetPage.RegionFolder.RegionOption.Value is RegionNone;

            Logger("Loading images...");

            //
            // LOAD TARGET IMAGE
            //
            Logger("Loading target image...");
            var success = LoadImage(
                options.TargetPage.TargetFolder.TargetFile.Value,
                out var targetImage,
                out var targetRegion,
                regionFromTarget);
            var origTargetX = targetImage.GetLength(1);
            var origTargetY = targetImage.GetLength(0);
            if (regionFromTarget && targetRegion == null) Logger("ERROR: Target image does not contain expected region shape data!");

            if (regionAutoExpand)
            {
                if (slmResolutionX < origTargetX || slmResolutionY < origTargetY)
                {
                    Logger("ERROR: Target image does not match the slm resolution! Did you forget to set the optimisation region to \"auto expand\"?");
                    return false;
                }

                targetImage = ExpandImage(targetImage, options);
            }
            else if (slmResolutionX != origTargetX || slmResolutionY != origTargetY)
            {
                Logger("ERROR: Target image does not match the slm resolution! With the optimisation region set to \"auto expand\", the slm resolution must be at least as large as the target image!");
                return false;
            }

            alg.SetTargetImage(targetImage);

            //
            // LOAD REGION IMAGE
            //
            if (regionFromFile)
            {
                success = LoadImage(
                    options.TargetPage.RegionFolder.RegionOption.Possibilities2.RegionFromFile.RegionFile.Value,
                    out _,
                    out targetRegion,
                    true);

                if (targetRegion == null)
                    Logger("ERROR: Region image does not contain expected region shape data!");
                else
                    alg.SetRegionImage(targetRegion);
            }
            else if (regionIntensity)
            {
                Logger("ERROR: Region from intensity is not yet implemented!");
                return false;
            }
            else if (regionNone || regionAutoExpand)
            {
                targetRegion = new int[origTargetY, origTargetX];
                for (var y = 0; y < targetRegion.GetLength(0); y++)
                for (var x = 0; x < targetRegion.GetLength(1); x++)
                    targetRegion[y, x] = 1;
                if (regionAutoExpand) targetRegion = ExpandImage(targetRegion, options);
            }

            if (!success)
                return false;
            alg.SetRegionImage(targetRegion);

            //
            // LOAD INITIAL IMAGE
            //
            Logger("Loading initial image...");
            var haveInitialImage = options.ProjectorPage.HologramFolder.SeedOption.Value is FromFileSeed;
            if (haveInitialImage)
            {
                success = LoadImage(
                    options.ProjectorPage.HologramFolder.SeedOption.Possibilities2.FromFileSeed.SeedFile.Value,
                    out var initialImage,
                    out _);
                if (slmResolutionX != initialImage.GetLength(1) || slmResolutionY != initialImage.GetLength(0))
                {
                    Logger("ERROR: Initial image does not match the slm resolution! ");
                    return false;
                }

                alg.SetInitialImage(initialImage);
                if (!success)
                    return false;
            }

            //
            // LOAD ILLUMINATION IMAGE
            //
            Logger("Loading illumination image...");
            var haveIlluminationImage = options.ProjectorPage.IlluminationFolder.IlluminationOption.Value is IlluminationFromFile;
            if (haveIlluminationImage)
            {
                success = LoadImage(
                    options.ProjectorPage.IlluminationFolder.IlluminationOption.Possibilities2.IlluminationFromFile.IlluminationFile.Value,
                    out var illuminationImage,
                    out _);
                if (slmResolutionX != illuminationImage.GetLength(1) || slmResolutionY != illuminationImage.GetLength(2))
                {
                    Logger("ERROR: Initial image does not match the slm resolution! ");
                    return false;
                }

                alg.SetIlluminationImage(illuminationImage);
                if (!success)
                    return false;
            }

            return true;
        }

        private bool LoadImage(FileInfo file, out Complex[,] image, out int[,] region, bool expectAndLoadRegion = false)
        {
            if (CachedImages.TryGetValue(file.FullName, out var cachedValue))
            {
                image = cachedValue.Item1;
                region = cachedValue.Item2;
                return true;
            }
            Logger($"Loading image file: {file}");
            image = null;
            region = null;
            var res = new JsonImageFileLoader().Load(file);
            var error = res.Item1;
            var loaded = res.Item2;
            if (expectAndLoadRegion)
                region = ShapeToImageConverter.Convert(
                    Logger,
                    loaded.Dimension.XSize,
                    loaded.Dimension.YSize,
                    loaded.ShapeCollection);
            if (error == null || error != "")
            {
                Logger($"Loading image file: {file} - FAILED!");
                return false;
            }

            Logger($"Loading image file: {file} - SUCCESS!");
            image = loaded.Values;
            cachedValue = new Tuple<Complex[,], int[,]>((Complex[,]) image.DeepCopy(true), (int[,]) region?.DeepCopy(true));
            CachedImages.Add(file.FullName, cachedValue);
            return true;
        }

        protected T[,] ExpandImage<T>(T[,] input, OptionsRoot options)
        {
            var slmResolutionX = options.ProjectorPage.HologramFolder.SLMResolutionX.Value;
            var slmResolutionY = options.ProjectorPage.HologramFolder.SLMResolutionY.Value;
            var expansionOrigin = options.TargetPage.RegionFolder.RegionOption.Possibilities2.RegionAutoExpand.ExpandOption.Value.ExpansionType;

            if (slmResolutionX == input.GetLength(1) && slmResolutionY == input.GetLength(0)) return input;

            int startX;
            int startY;
            switch (expansionOrigin)
            {
                case ExpansionType.Bottom:
                    startX = (slmResolutionX - input.GetLength(1)) / 2;
                    startY = slmResolutionY - input.GetLength(0);
                    break;
                case ExpansionType.Top:
                    startX = (slmResolutionX - input.GetLength(1)) / 2;
                    startY = 0;
                    break;
                case ExpansionType.Left:
                    startX = 0;
                    startY = (slmResolutionY - input.GetLength(0)) / 2;
                    break;
                case ExpansionType.Right:
                    startX = slmResolutionX - input.GetLength(1);
                    startY = (slmResolutionY - input.GetLength(0)) / 2;
                    break;
                case ExpansionType.Centre:
                    startX = (slmResolutionX - input.GetLength(1)) / 2;
                    startY = (slmResolutionY - input.GetLength(0)) / 2;
                    break;
                case ExpansionType.BottomLeft:
                    startX = 0;
                    startY = slmResolutionY - input.GetLength(0);
                    break;
                case ExpansionType.BottomRight:
                    startX = slmResolutionX - input.GetLength(1);
                    startY = slmResolutionY - input.GetLength(0);
                    break;
                case ExpansionType.TopLeft:
                    startX = 0;
                    startY = 0;
                    break;
                case ExpansionType.TopRight:
                    startX = slmResolutionX - input.GetLength(1);
                    startY = 0;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            var output = new T[slmResolutionY, slmResolutionX];

            for (var y = 0; y < input.GetLength(0); y++)
            for (var x = 0; x < input.GetLength(1); x++)
                output[y + startY, x + startX] = input[y, x];

            return output;
        }

        public void Swap<T>(ref T lhs, ref T rhs)
        {
            var temp = lhs;
            lhs = rhs;
            rhs = temp;
        }

        private QuantiserMan SelectQuantiser(OptionsRoot options)
        {
            Logger("Creating quantisation approach...");

            QuantiserMan quant;

            switch (options.AlgorithmsPage.QuantFolder.QuantOption.Value)
            {
                case QuantNearestNeighbour _:
                    quant = new QuantiserNearestMan();
                    quant.SetLogger(_loggerDelegate);
                    break;
                case QuantDCBalanced _:
                    Logger(">>>>> Unsupported quantisation approach <<<<<");
                    return null;
                case QuantNearestSmoothed _:
                    Logger(">>>>> Unsupported quantisation approach <<<<<");
                    return null;
                default:
                    Logger(">>>>> Unsupported quantisation approach <<<<<");
                    return null;
            }

            SetBaseQuantisationParameters(quant, options);

            return quant;
        }

        private void SetBaseQuantisationParameters(QuantiserMan quant, OptionsRoot options)
        {
            Logger("Passing common base quantisation parameters...");

            GetSLMModulationScheme(options, out var maxSlmValue, out var minSlmValue, out var modulationScheme, out var levels);

            quant.SetBaseParameters(
                maxSlmValue,
                minSlmValue,
                levels,
                modulationScheme);
        }

        public void Dispose()
        {
            //_task?.Dispose();
            Alg?.Dispose();
            Quantiser?.Dispose();
        }
    }
}