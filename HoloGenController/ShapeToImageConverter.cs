﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using HoloGen.Image;

namespace HoloGen.Controller
{
    /// <summary>
    /// Converts a list of shapes (used in masks/regions) into an array of ints that can be passed
    /// into Cuda
    /// WARNING: Uses an array of ints not an array of bools because of std::vector of bools
    /// </summary>
    public static class ShapeToImageConverter
    {
        public static int[,] Convert(Action<string> logger, int slmResolutionX, int slmResolutionY, SaveableShapeCollection shapeCollection)
        {
            logger("Loading region mask...");

            var region = new int[slmResolutionY, slmResolutionX];

            foreach (var tuple in shapeCollection)
            {
                var shape = tuple.Item1;
                var points = tuple.Item2;

                if (shape is ShapeType.Rectangle)
                    ChangeBitsInsideRectangle(ref region, points);
                else if (shape is ShapeType.Ellipse)
                    ChangeBitsInsideEllipse(ref region, points);
                else if (shape is ShapeType.Polygon)
                    ChangeBitsInsidePolygon(ref region, points);
                else if (shape is ShapeType.Point)
                    ChangeBitsInsidePoint(ref region, points);
                else
                    logger($"Unrecognised shape: {shape.GetType().Name}.");
            }

            return region;
        }

        private static void ChangeBitsInsideRectangle(ref int[,] region, List<Point> points)
        {
            Debug.Assert(region != null);
            Debug.Assert(points != null);
            Debug.Assert(points.Count >= 2);

            for (var y = (int) Math.Min(points[0].Y, points[1].Y); y <= Math.Min(points[0].Y, points[1].Y); y++)
            for (var x = (int) Math.Min(points[0].X, points[1].X); x <= Math.Min(points[0].X, points[1].X); x++)
                region[y, x] = 1;
        }

        private static void ChangeBitsInsideEllipse(ref int[,] region, List<Point> points)
        {
            Debug.Assert(region != null);
            Debug.Assert(points != null);
            Debug.Assert(points.Count >= 2);

            var rx = Math.Abs(points[0].X - points[1].X) / 2.0;
            var ry = Math.Abs(points[0].Y - points[1].Y) / 2.0;

            var ox = Math.Min(points[0].X, points[1].X) + rx;
            var oy = Math.Min(points[0].Y, points[1].Y) + ry;

            for (var y = (int) Math.Min(points[0].Y, points[1].Y); y <= Math.Min(points[0].Y, points[1].Y); y++)
            for (var x = (int) Math.Min(points[0].X, points[1].X); x <= Math.Min(points[0].X, points[1].X); x++)
                if (Math.Pow(x - ox, 2) / Math.Pow(rx, 2) + Math.Pow(y - oy, 2) / Math.Pow(ry, 2) <= 1)
                    region[y, x] = 1;
        }

        private static void ChangeBitsInsidePolygon(ref int[,] region, List<Point> points)
        {
            Debug.Assert(region != null);
            Debug.Assert(points != null);
            Debug.Assert(points.Count >= 3);

            for (var y = (int) Math.Min(points[0].Y, points[1].Y); y <= Math.Min(points[0].Y, points[1].Y); y++)
            for (var x = (int) Math.Min(points[0].X, points[1].X); x <= Math.Min(points[0].X, points[1].X); x++)
                region[y, x] = new Point(y, x).IsInPolygon(points) ? 1 : region[y, x];
        }

        private static void ChangeBitsInsidePoint(ref int[,] region, List<Point> points)
        {
            Debug.Assert(region != null);
            Debug.Assert(points != null);
            Debug.Assert(points.Count >= 1);

            region[(int) points[0].Y, (int) points[0].X] = 1;
        }

        public static bool IsInPolygon(this Point point, List<Point> points)
        {
            var isInside = false;
            var lastPoint = points[points.Count - 1];
            foreach (var vertex in points)
            {
                if (point.Y.IsBetween(lastPoint.Y, vertex.Y))
                {
                    var t = (point.Y - lastPoint.Y) / (vertex.Y - lastPoint.Y);
                    var x = t * (vertex.X - lastPoint.X) + lastPoint.X;
                    if (x >= point.X)
                        isInside = !isInside;
                }
                else
                {
                    const double tol = 10e-8;
                    if (Math.Abs(point.Y - lastPoint.Y) < tol && point.X < lastPoint.X && vertex.Y > point.Y)
                        isInside = !isInside;
                    if (Math.Abs(point.Y - vertex.Y) < tol && point.X < vertex.X && lastPoint.Y > point.Y)
                        isInside = !isInside;
                }

                lastPoint = vertex;
            }

            return isInside;
        }

        public static bool IsBetween(this double x, double a, double b)
        {
            return (x - a) * (x - b) < 0;
        }
    }
}