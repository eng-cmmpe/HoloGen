﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using System.Drawing;

namespace HoloGen.Utils
{
    /// <summary>
    /// Utility class for interpolating between two colors.
    /// Taken from answer by user Jason
    /// http://stackoverflow.com/questions/1236683/color-interpolation-between-3-colors-in-net
    /// </summary>
    /// summary>
    public class ColorInterpolator
    {
        private static readonly ComponentSelector RedSelector = color => color.R;
        private static readonly ComponentSelector GreenSelector = color => color.G;
        private static readonly ComponentSelector BlueSelector = color => color.B;

        public static Color InterpolateBetween(
            Color endPoint1,
            Color endPoint2,
            double lambda)
        {
            if (lambda < 0.0)
                return endPoint1;
            if (lambda > 1.0)
                return endPoint2;
            var color = Color.FromArgb(
                InterpolateComponent(endPoint1, endPoint2, lambda, RedSelector),
                InterpolateComponent(endPoint1, endPoint2, lambda, GreenSelector),
                InterpolateComponent(endPoint1, endPoint2, lambda, BlueSelector)
            );

            return color;
        }

        private static byte InterpolateComponent(
            Color endPoint1,
            Color endPoint2,
            double lambda,
            ComponentSelector selector)
        {
            if (lambda < 0.0)
                return selector(endPoint1);
            if (lambda > 1.0)
                return selector(endPoint2);
            return (byte) (selector(endPoint1)
                           + (selector(endPoint2) - selector(endPoint1)) * lambda);
        }

        private delegate byte ComponentSelector(Color color);
    }
}