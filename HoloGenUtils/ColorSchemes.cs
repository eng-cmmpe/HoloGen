﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

using System.Collections.ObjectModel;
using System.Drawing;

namespace HoloGen.Utils
{
    /// <summary>
    /// Singleton model collection of <see cref="ColorSchemes" />.
    /// </summary>
    public class ColorSchemes : ObservableCollection<ColorScheme>
    {
        private ColorSchemes()
        {
            var type = typeof(ColorScheme);

            foreach (var prop in GetType().GetProperties())
                if (type == prop.PropertyType && prop.DeclaringType == GetType())
                    Add((ColorScheme) prop.GetValue(this));
        }

        public static ColorSchemes Instance { get; } = new ColorSchemes();

        public ColorScheme WhiteonBlack { get; } = new ColorScheme(Color.Black, Color.White, Resources.Properties.Resources.ColorSchemes_WhiteOnBlack);
        public ColorScheme RedonBlack { get; } = new ColorScheme(Color.Black, Color.Red, Resources.Properties.Resources.ColorSchemes_RedOnBlack);
        public ColorScheme BlueonBlack { get; } = new ColorScheme(Color.Black, Color.Blue, Resources.Properties.Resources.ColorSchemes_BlueOnBlack);
        public ColorScheme GreenonBlack { get; } = new ColorScheme(Color.Black, Color.Green, Resources.Properties.Resources.ColorSchemes_GreenOnBlack);
        public ColorScheme YellowonBlack { get; } = new ColorScheme(Color.Black, Color.Yellow, Resources.Properties.Resources.ColorSchemes_YellowOnBlack);
        public ColorScheme OrangeonBlack { get; } = new ColorScheme(Color.Black, Color.Orange, Resources.Properties.Resources.ColorSchemes_OrangeOnBlack);
        public ColorScheme PurpleonBlack { get; } = new ColorScheme(Color.Black, Color.Purple, Resources.Properties.Resources.ColorSchemes_PurpleOnBlack);
        public ColorScheme BlackonWhite { get; } = new ColorScheme(Color.White, Color.Black, Resources.Properties.Resources.ColorSchemes_BlackOnWhite);
        public ColorScheme RedonWhite { get; } = new ColorScheme(Color.White, Color.Red, Resources.Properties.Resources.ColorSchemes_RedOnWhite);
        public ColorScheme BlueonWhite { get; } = new ColorScheme(Color.White, Color.Blue, Resources.Properties.Resources.ColorSchemes_BlueOnWhite);
        public ColorScheme GreenonWhite { get; } = new ColorScheme(Color.White, Color.Green, Resources.Properties.Resources.ColorSchemes_GreenOnWhite);
        public ColorScheme YellowonWhite { get; } = new ColorScheme(Color.White, Color.Yellow, Resources.Properties.Resources.ColorSchemes_YellowOnWhite);
        public ColorScheme OrangeonWhite { get; } = new ColorScheme(Color.White, Color.Orange, Resources.Properties.Resources.ColorSchemes_OrangeOnWhite);
        public ColorScheme PurpleonWhite { get; } = new ColorScheme(Color.White, Color.Purple, Resources.Properties.Resources.ColorSchemes_PurpleOnWhite);
    }
}