﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using System;
using System.Collections.Generic;

namespace HoloGen.Utils
{
    /// <summary>
    /// Generic cache object.
    /// Returns value type B for key K1. If it doesn't exist it is created with a constructor
    /// (A) => B
    /// </summary>
    public class GenericCache<TK1, TA, TB>
    {
        private readonly Func<TK1, TA, TB> _constructor;

        private readonly TA _surround;

        public GenericCache(TA surround, Func<TK1, TA, TB> constructor)
        {
            _constructor = constructor;
            _surround = surround;
        }

        private Dictionary<TK1, TB> Cache { get; } = new Dictionary<TK1, TB>();

        public TB GetCachedValue(TK1 key)
        {
            if (!Cache.ContainsKey(key)) Cache.Add(key, _constructor(key, _surround));
            return Cache[key];
        }
    }

    /// <summary>
    /// Generic cache object.
    /// Returns value type C for keys K1 and K2. If it doesn't exist it is created with a constructors
    /// (A) => B
    /// (B) => C
    /// </summary>
    public class GenericCache<TK1, TK2, TA, TB, TC>
    {
        private readonly Func<TK1, TA, TB> _constructor1;
        private readonly Func<TK2, TB, TC> _constructor2;

        private readonly TA _surround;

        public GenericCache(TA surround, Func<TK1, TA, TB> constructor1, Func<TK2, TB, TC> constructor2)
        {
            _constructor1 = constructor1;
            _constructor2 = constructor2;
            _surround = surround;
        }

        private Dictionary<TK1, GenericCache<TK2, TB, TC>> Cache { get; } = new Dictionary<TK1, GenericCache<TK2, TB, TC>>();

        public TC GetCachedValue(TK1 key1, TK2 key2)
        {
            if (!Cache.ContainsKey(key1))
                Cache.Add(
                    key1,
                    new GenericCache<TK2, TB, TC>(_constructor1(key1, _surround), _constructor2)
                );
            return Cache[key1].GetCachedValue(key2);
        }
    }

    /// <summary>
    /// Generic cache object.
    /// Returns value type C for keys K1, K2 and K3. If it doesn't exist it is created with a constructors
    /// (A) => B
    /// (B) => C
    /// (C) => D
    /// </summary>
    public class GenericCache<TK1, TK2, TK3, TA, TB, TC, TD>
    {
        private readonly Func<TK1, TA, TB> _constructor1;
        private readonly Func<TK2, TB, TC> _constructor2;
        private readonly Func<TK3, TC, TD> _constructor3;

        private readonly TA _surround;

        public GenericCache(TA surround, Func<TK1, TA, TB> constructor1, Func<TK2, TB, TC> constructor2, Func<TK3, TC, TD> constructor3)
        {
            _constructor1 = constructor1;
            _constructor2 = constructor2;
            _constructor3 = constructor3;
            _surround = surround;
        }

        private Dictionary<TK1, GenericCache<TK2, TK3, TB, TC, TD>> Cache { get; } = new Dictionary<TK1, GenericCache<TK2, TK3, TB, TC, TD>>();

        public TD GetCachedValue(TK1 key1, TK2 key2, TK3 key3)
        {
            if (!Cache.ContainsKey(key1))
                Cache.Add(
                    key1,
                    new GenericCache<TK2, TK3, TB, TC, TD>(_constructor1(key1, _surround), _constructor2, _constructor3)
                );
            return Cache[key1].GetCachedValue(key2, key3);
        }
    }

    /// <summary>
    /// Generic cache object.
    /// Returns value type C for keys K1, K2, K3 and K4. If it doesn't exist it is created with a constructors
    /// (A) => B
    /// (B) => C
    /// (C) => D
    /// (D) => E
    /// </summary>
    public class GenericCache<TK1, TK2, TK3, TK4, TA, TB, TC, TD, TE>
    {
        private readonly Func<TK1, TA, TB> _constructor1;
        private readonly Func<TK2, TB, TC> _constructor2;
        private readonly Func<TK3, TC, TD> _constructor3;
        private readonly Func<TK4, TD, TE> _constructor4;

        private readonly TA _surround;

        public GenericCache(TA surround, Func<TK1, TA, TB> constructor1, Func<TK2, TB, TC> constructor2, Func<TK3, TC, TD> constructor3, Func<TK4, TD, TE> constructor4)
        {
            _constructor1 = constructor1;
            _constructor2 = constructor2;
            _constructor3 = constructor3;
            _constructor4 = constructor4;
            _surround = surround;
        }

        private Dictionary<TK1, GenericCache<TK2, TK3, TK4, TB, TC, TD, TE>> Cache { get; } = new Dictionary<TK1, GenericCache<TK2, TK3, TK4, TB, TC, TD, TE>>();

        public TE GetCachedValue(TK1 key1, TK2 key2, TK3 key3, TK4 key4)
        {
            if (!Cache.ContainsKey(key1))
                Cache.Add(
                    key1,
                    new GenericCache<TK2, TK3, TK4, TB, TC, TD, TE>(_constructor1(key1, _surround), _constructor2, _constructor3, _constructor4)
                );
            return Cache[key1].GetCachedValue(key2, key3, key4);
        }
    }
}