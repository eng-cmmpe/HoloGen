﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using System;

namespace HoloGen.Hierarchy.Hierarchy
{
    /// <summary>
    /// Reflective hierarchy element template <typeparamref name="T" />.
    /// The constructor finds all members assignable from <typeparamref name="T" /> and
    /// uses them to populate the <see cref="Children" /> member collection.
    /// </summary>
    /// <typeparam name="T">
    /// Class/Interface that has <see cref="object.GetType()" /> called on it to get the type used to
    /// populate <see cref="Children" />
    /// </typeparam>
    [Serializable]
    public abstract class ChangingChildrenElement<T> : ReflectiveChildrenElement<T>, INotifyChanged
        where T : class, ICanError, IHasName, INotifyChanged, IHasBindingPath
    {
        protected ChangingChildrenElement()
        {
            Children.CollectionChanged += Children_CollectionChanged;

            foreach (var child in Children) child.Changed += Child_Changed;
        }

        public event EventHandler Changed;

        private void Child_Changed(object sender, EventArgs e)
        {
            Changed?.Invoke(this, null);
        }

        private void Children_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
                foreach (var child in e.OldItems)
                    if (child is INotifyChanged child2)
                        child2.Changed -= Child_Changed;

            if (e.NewItems != null)
                foreach (var child in e.NewItems)
                    if (child is INotifyChanged child2)
                        child2.Changed += Child_Changed;
        }
    }
}