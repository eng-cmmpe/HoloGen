﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Windows.Data;

namespace HoloGen.Hierarchy.Hierarchy
{
    /// <summary>
    /// Searchable collection of children
    /// </summary>
    [Serializable]
    public abstract class SearchableChildrenElement<T> : ChangingChildrenElement<T>, ICanSearch
        where T : class, ICanError, IHasName, ICanSearch, INotifyChanged, IHasBindingPath
    {
        private string _searchTerm = "";

        protected SearchableChildrenElement()
        {
            SearchedChildren.Source = Children;
            SearchedChildren.Filter += SearchedChildren_Filter;
        }

        [JsonIgnore]
        public CollectionViewSource SearchedChildren { get; } = new CollectionViewSource();

        public string SearchTerm
        {
            get => _searchTerm;
            set
            {
                var searchTerm = string.IsNullOrEmpty(value) ? "" : value;
                foreach (var child in Children)
                    child.SearchTerm = searchTerm;
                SetProperty(ref _searchTerm, searchTerm);
                SearchedChildren.View.Refresh();
            }
        }

        public bool HasResult => (from child in Children
                                  where child.HasResult
                                  select child).Any();

        protected void SearchedChildren_Filter(object sender, FilterEventArgs e)
        {
            e.Accepted = (e.Item as T)?.HasResult ?? false;
        }
    }
}