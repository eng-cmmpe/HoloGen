﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.OptionTypes;
using Newtonsoft.Json;
using System.Windows.Data;

namespace HoloGen.Hierarchy.Hierarchy
{
    /// <summary>
    /// Folder hierarchy element.
    /// </summary>
    public abstract class HierarchyFolder : HierarchyElement<INode>
    {
        private bool _isExpanded = true;

        protected HierarchyFolder()
        {
            SmallChildren.Source = Children;
            LargeChildren.Source = Children;
            SmallChildren.Filter += SmallChildren_Filter;
            LargeChildren.Filter += LargeChildren_Filter;
            PropertyChanged += HierarchyFolder_PropertyChanged;
        }

        [JsonIgnore]
        public bool IsExpanded
        {
            get => _isExpanded;
            set => SetProperty(ref _isExpanded, value);
        }

        [JsonIgnore]
        public CollectionViewSource LargeChildren { get; } = new CollectionViewSource();

        [JsonIgnore]
        public CollectionViewSource SmallChildren { get; } = new CollectionViewSource();

        private void HierarchyFolder_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SearchTerm")
            {
                SmallChildren.View.Refresh();
                LargeChildren.View.Refresh();
            }
        }

        private void LargeChildren_Filter(object sender, FilterEventArgs e)
        {
            Filter(sender, e, false);
        }

        private void SmallChildren_Filter(object sender, FilterEventArgs e)
        {
            Filter(sender, e, true);
        }

        private void Filter(object sender, FilterEventArgs e, bool small)
        {
            SearchedChildren_Filter(sender, e);

            e.Accepted &= small ^ (e.Item is ILargeOption);
        }

        public override void Reset()
        {
            // Only reset direct children
            foreach (var child in Children) child.Reset();
        }
    }
}