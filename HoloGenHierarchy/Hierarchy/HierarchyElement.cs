﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.OptionTypes;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

namespace HoloGen.Hierarchy.Hierarchy
{
    /// <summary>
    /// Root hierarchy element. Defines <see cref="Name" /> and <see cref="ToolTip" /> fields that must be implemented by
    /// any implementors.
    /// Also defines the <see cref="Valid" /> property that checks whether all the children are valid and the
    /// <see cref="Error" /> property which
    /// returns a collection of all the children error messages.
    /// <see cref="Enabled" /> defaults to true and can be overriden by any extending classes.
    /// The base class <see cref="ReflectiveChildrenElement" /> finds all members assignable from <typeparamref name="T" />
    /// and
    /// uses them to populate the <see cref="ReflectiveChildrenElement.Children" /> member collection.
    /// </summary>
    /// <typeparam name="T">
    /// Class/Interface that has <see cref="object.GetType()" /> called on it to get the type used to
    /// populate <see cref="Children" />
    /// </typeparam>
    public abstract class HierarchyElement<T> : SearchableChildrenElement<T>, IHierarchyElement, ICanFlatten
        where T : class, ICanError, IHasName, IHasBindingPath, INotifyPropertyChanged, ICanReset, ICanEnable, ICanSearch, INotifyChanged, ICanFlatten
    {
        private bool _enabled = true;

        internal HierarchyElement()
        {
            Children.CollectionChanged += Children_CollectionChanged;

            foreach (var child in Children) child.PropertyChanged += Child_PropertyChanged;
        }

        public List<IOption> Flatten()
        {
            var flattened = new List<IOption>();
            foreach (var child in OriginalChildren) flattened = flattened.Concat(child.Flatten()).ToList();
            return flattened;
        }

        [JsonIgnore]
        public abstract string ToolTip { get; }

        [JsonIgnore]
        public virtual bool Enabled
        {
            get => _enabled;
            set => SetProperty(ref _enabled, value);
        }

        [JsonIgnore]
        public virtual bool Valid
        {
            get
            {
                foreach (var child in Children)
                    if (!child.Valid)
                        return false;
                return true;
            }
        }

        [JsonIgnore]
        public virtual StringCollection Error
        {
            get
            {
                var error = new StringCollection();
                foreach (var child in Children)
                {
                    var childcol = child.Error;
                    foreach (var err in childcol) error.Add(child.Name + " >> " + err);
                }

                return error;
            }
        }

        public virtual void Reset()
        {
            foreach (var child in Children) child.Reset();

            OnPropertyChanged(nameof(Valid));
        }

        public void RecursivelySetEnabled(bool value)
        {
            Enabled = value;

            var type = typeof(T);
            if (typeof(ICanRecursivelyEnable).IsAssignableFrom(type))
                foreach (var child in Children)
                {
                    var child2 = child as ICanRecursivelyEnable;
                    Debug.Assert(child2 != null);
                    child2.RecursivelySetEnabled(value);
                }
        }

        private void Children_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
                foreach (var child in e.NewItems)
                    ((T) child).PropertyChanged += Child_PropertyChanged;
        }

        private void Child_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(Valid) || e.PropertyName == "SelectedIndex") OnPropertyChanged(nameof(Valid));
        }
    }
}