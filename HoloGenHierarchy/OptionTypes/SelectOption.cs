﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace HoloGen.Hierarchy.OptionTypes
{
    /// <summary>
    /// Extends <see cref="Option" /> wrapping a multiple-choice <see cref="PossibilityCollection" />.
    /// Due to the limits of the C# reflection interface, <see cref="SelectOption" /> defines
    /// <see cref="SetPushLocation(HierarchyFolder)" /> which must be called in the owner's constructor.
    /// <see cref="SelectOption" /> listens to changes in the selected <see cref="Possibility" /> and pushes the
    /// <see cref="ReflectiveChildrenElement{T}.Children" />
    /// of the <see cref="Possibility" /> into the <see cref="HierarchyFolder" /> owner.
    /// <see cref="Value" /> returns the selected <see cref="Possibility" /> while <see cref="SelectedIndex" /> returns the
    /// index of the selected <see cref="Possibility" />.
    /// </summary>
    public abstract class SelectOption<T> : Option<T>, ISelectOption
        where T : Possibility
    {
        private int _selectedIndex;

        [JsonIgnore] public HierarchyFolder PushLocation;

        protected SelectOption()
        {
            // ReSharper disable VirtualMemberCallInConstructor
            Possibilities.BindingName = "Possibilities";
            // ReSharper restore VirtualMemberCallInConstructor
            SetEnabledStatuses();
        }

        protected SelectOption(int selectedIndex)
        {
            // ReSharper disable VirtualMemberCallInConstructor
            Possibilities.BindingName = "Possibilities";
            // ReSharper restore VirtualMemberCallInConstructor
            SelectedIndex = selectedIndex;
            SetEnabledStatuses();
        }

        public abstract PossibilityCollection<T> Possibilities { get; }

        [JsonIgnore]
        public override T Value
        {
            get => Possibilities.Children[SelectedIndex];
            set => SelectedIndex = Possibilities.Children.IndexOf(value);
        }

        public int SelectedIndex
        {
            get => _selectedIndex;
            set
            {
                var old = Value;
                ValueChangedMethod(Value, Possibilities.Children[value]);
                SetProperty(ref _selectedIndex, value);
                NotifyChanged();
                AddOtherChangeListeners();
                InvokeValueChanged(old);
                OnPropertyChanged(nameof(Value));
                OnPropertyChanged(nameof(Valid));
                OnPropertyChanged(nameof(ValueEqualToReference));
            }
        }

        [JsonIgnore]
        public override T Default => Possibilities.Children.First();

        [JsonIgnore]
        public string ExtendedToolTip
        {
            get
            {
                var builder = new StringBuilder(ToolTip);
                builder.Append("\nOptions: ");
                foreach (var possibility in Possibilities.Children) builder.Append("\n - " + possibility.Name + ": " + possibility.ToolTip);
                return builder.ToString();
            }
        }

        [JsonIgnore]
        public override bool Valid => true;

        [JsonIgnore]
        public override StringCollection Error { get; } = new StringCollection();

        public override void Reset()
        {
            SelectedIndex = 0;

            Possibilities.Reset();
        }

        public override List<IOption> Flatten()
        {
            var flattened = new List<IOption> {this};
            foreach (var child in Possibilities.Children) flattened = flattened.Concat(child.Flatten()).ToList();
            return flattened;
        }

        public override void SetBindingPathRecursively(string baseBindingPath)
        {
            if (string.IsNullOrEmpty(BindingPath))
                BindingPath = baseBindingPath == "" ? BindingName : BindingName == "" ? baseBindingPath : baseBindingPath + "." + BindingName;
            Possibilities.SetBindingPathRecursively(BindingPath);
        }

        public override void SetLongNameRecursively(string baseLongName)
        {
            LongName = baseLongName != "" ? baseLongName + " >> " + Name : Name;
            Possibilities.SetLongNameRecursively(LongName);
        }

        public override string ConvertToString()
        {
            return Value.Name;
        }

        public override void ConvertFromString(string input)
        {
            for (var i = 0; i < Possibilities.Children.Count; i++)
                if (Possibilities.Children[i].Name == input)
                    SelectedIndex = i;
        }

        private void SetEnabledStatuses()
        {
            foreach (var possibility in Possibilities.Children)
            foreach (var option in possibility.Children)
                option.Enabled = false;
        }

        /// <summary>
        /// C# is a little stricter than some on initialisation order. We have to initiate
        /// SelectOption in the class definition otherwise the static behaviour of WPF fails.
        /// If we do that, though, then we can't pass in the reference (PushLocation) from our
        /// superconstructor because 'this' (e.g. AlgorithmsFolder) does not exist yet at that
        /// point. If this was Java or C++ we would be fine. Yeah, this is hacky. Get over it.
        /// </summary>
        /// <param name="pushlocation"></param>
        public void SetPushLocation(HierarchyFolder pushlocation)
        {
            PushLocation = pushlocation;
            foreach (var child in Value.Children)
            {
                child.Enabled = true;
                PushLocation.Children.Add(child);
            }
        }

        private void ValueChangedMethod(T oldValue, T newValue)
        {
            if (PushLocation != null)
            {
                if (oldValue != null && !oldValue.Equals(newValue))
                    foreach (var child in oldValue.Children)
                    {
                        child.Enabled = false;
                        PushLocation.Children.Remove(child);
                    }

                if (newValue != null && !newValue.Equals(oldValue))
                    foreach (var child in newValue.Children)
                    {
                        child.Enabled = true;
                        PushLocation.Children.Add(child);
                    }

                SetProperty(ref oldValue, newValue);
            }
        }

        public override bool CompareValues(T otherValue)
        {
            return Value.Name == otherValue.Name;
        }

        protected override void Reference_ValueChanged(object sender, T oldValue)
        {
            if (CompareValues(oldValue)) SelectedIndex = ((SelectOption<T>) Reference).SelectedIndex;
            OnPropertyChanged(nameof(ValueEqualToReference));
        }
    }
}