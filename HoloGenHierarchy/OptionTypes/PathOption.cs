﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using HoloGen.Utils;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.IO;

namespace HoloGen.Hierarchy.OptionTypes
{
    /// <summary>
    /// Extends <see cref="Option" /> wrapping a <see cref="FileInfo" /> parameter.
    /// Implements <see cref="ILargeOption" />. This tells <see cref="HierarchyFolder" /> that this
    /// should be made a placed in <see cref="HierarchyFolder.LargeChildren" />.
    /// Implements the <see cref="IHasWatermark" /> interface in addition.
    /// <see cref="Filter" /> defines the file selection filter used and should be compatible with the filters used by the
    /// <see cref="OpenFileDialog" />.
    /// </summary>
    public abstract class PathOption : Option<FileInfo>, IHasWatermark, ILargeOption
    {
        protected PathOption()
        {
        }

        protected PathOption(FileInfo value) : base(value)
        {
        }

        [JsonIgnore]
        public abstract string Filter { get; }

        [JsonIgnore]
        public bool IsFolder => Filter == FileTypes.DirectoryFilter;

        public abstract string Watermark { get; }

        [JsonIgnore]
        public override bool Valid => Error.Count <= 0;

        [JsonIgnore]
        public override StringCollection Error
        {
            get
            {
                var collection = new StringCollection();
                if (Value == null)
                    collection.Add(Resources.Properties.Resources.PathOption_Error_NoFileSelected);
                else if (string.IsNullOrEmpty(Value.FullName))
                    collection.Add(Resources.Properties.Resources.PathOption_Error_NoFileSelected);
                else if (!IsFolder && !Value.Exists)
                    collection.Add(Resources.Properties.Resources.PathOption_Error_FileDoesNotExist);
                else if (!IsFolder && !FileUtils.FileMatchesFilter(Value, Filter))
                    collection.Add(Resources.Properties.Resources.PathOption_Error_FileIsNotOfValidType);
                else if (Value.Directory != null && IsFolder && !Value.Directory.Exists) collection.Add(Resources.Properties.Resources.PathOption_Error_FolderDoesNotExist);
                return collection;
            }
        }

        public override string ConvertToString()
        {
            return Value.FullName;
        }

        public override void ConvertFromString(string input)
        {
            Value = new FileInfo(input);
        }

        public override bool CompareValues(FileInfo otherValue)
        {
            return Value.FullName == otherValue.FullName;
        }
    }
}