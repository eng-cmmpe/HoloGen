﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.Hierarchy;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace HoloGen.Hierarchy.OptionTypes
{
    /// <summary>
    /// Extends <see cref="Option" /> wrapping a boolean parameter.
    /// Implements the <see cref="IHasWatermark" /> interface in addition.
    /// <see cref="OnText" /> and <see cref="OffText" /> are the strings used to define this option's states.
    /// e.g. "On"/"Off", "True"/"False" or "Yes"/"No".
    /// </summary>
    public abstract class BooleanOption : Option<bool>, IHasWatermark
    {
        protected BooleanOption()
        {
        }

        protected BooleanOption(bool value) : base(value)
        {
        }

        [JsonIgnore]
        public abstract string OnText { get; }

        [JsonIgnore]
        public abstract string OffText { get; }

        [JsonIgnore]
        public override bool Valid => true;

        [JsonIgnore]
        public override StringCollection Error { get; } = new StringCollection();

        public abstract string Watermark { get; }

        public override string ConvertToString()
        {
            return System.Convert.ToString(Value);
        }

        public override void ConvertFromString(string input)
        {
            Value = System.Convert.ToBoolean(input);
        }
    }
}