﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Hierarchy.OptionTypes;
using HoloGen.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;

namespace HoloGen.Hierarchy.CommandTypes
{
    /// <summary>
    /// Root command element.
    /// </summary>
    public abstract class Command<T> : ObservableObject, ICommand, System.Windows.Input.ICommand
    {
        private T _context;

        private bool _enabled = true;

        private string _searchTerm;

        protected Command()
        {
        }

        protected Command(T context)
        {
            Context = context;
            OnPropertyChanged(nameof(Context));
        }

        [JsonIgnore]
        public T Context
        {
            get => _context;
            set => SetProperty(ref _context, value);
        }

        public T MenuWindow => Context;

        [JsonIgnore]
        public abstract string Name { get; }

        [JsonIgnore]
        public string BindingName { get; set; } = "";

        [JsonIgnore]
        public string BindingPath { get; set; } = "";

        [JsonIgnore]
        public string LongName { get; set; } = "";

        [JsonIgnore]
        public abstract string ToolTip { get; }

        public string SearchTerm
        {
            get => _searchTerm;
            set
            {
                if (_searchTerm != value) SetProperty(ref _searchTerm, value);
                HasResult = string.IsNullOrEmpty(value) || HasSearchTerm(value);
            }
        }

        public bool HasResult { get; private set; } = true;

        public event EventHandler Changed;

        [JsonIgnore]
        public bool Enabled
        {
            // Ignored
            get => _enabled;
            set => SetProperty(ref _enabled, value);
        }

        public bool Valid => true;

        public StringCollection Error { get; } = new StringCollection();

        public void Reset()
        {
            // Ignored
        }

        public event EventHandler CanExecuteChanged;

        public virtual bool CanExecute(object parameter)
        {
            return true;
        }

        public abstract void Execute(object parameter);

        public virtual List<IOption> Flatten()
        {
            return new List<IOption>();
        }

        public virtual void SetBindingPathRecursively(string baseBindingPath)
        {
            if (string.IsNullOrEmpty(BindingPath))
                BindingPath = baseBindingPath == "" ? BindingName : BindingName == "" ? baseBindingPath : baseBindingPath + "." + BindingName;
        }

        public virtual void SetLongNameRecursively(string baseLongName)
        {
            LongName = baseLongName != "" ? baseLongName + " >> " + Name : Name;
        }

        public void SetContext(T context)
        {
            Context = context;
            OnPropertyChanged(nameof(Context));
        }

        private bool HasSearchTerm(string val)
        {
            return
                CultureInfo.CurrentCulture.CompareInfo.IndexOf(Name, val, CompareOptions.IgnoreCase) >= 0 &&
                CultureInfo.CurrentCulture.CompareInfo.IndexOf(ToolTip, val, CompareOptions.IgnoreCase) >= 0;
        }

        protected void FireCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, null);
        }
    }
}