﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HoloGen.Options;
using HoloGen.Options.Algorithm;
using Newtonsoft.Json;
using Newtonsoft.Json.Schema.Generation;
using Newtonsoft.Json.Schema;
using System.Collections.Specialized;
using System.IO;

namespace HoloGen
{
    public class Program
    {
        static void Main(string[] args)
        {
            //AlgTest AlgBase = new AlgTest();
            //AlgBase.DoFFT();

            //AlgTest AlgBase = new AlgTest();

            OptionsRoot options = new OptionsRoot();

            File.WriteAllText(Path.GetTempPath()+"temp45345345.txt", JsonConvert.SerializeObject(options, Formatting.Indented));
            string output = File.ReadAllText(Path.GetTempPath()+"temp45345345.txt");
            OptionsRoot options2 = JsonConvert.DeserializeObject<OptionsRoot>(output);
            Console.WriteLine(JsonConvert.SerializeObject(options, Formatting.Indented));
            Console.ReadLine();

            //Console.WriteLine(JsonConvert.SerializeObject(options, Formatting.Indented));
            //Console.WriteLine();
            //Console.ReadLine();

            //options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Value
            //    = options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities.Children[1];
            //StringCollection errors = options.Error;
            //foreach (var error in errors)
            //{
            //    Console.WriteLine(error);
            //}
            //Console.ReadLine();

            //JSchemaGenerator generator = new JSchemaGenerator();
            //JSchema schema = generator.Generate(typeof(OptionsRoot));
            //Console.WriteLine(schema.ToString());
            //Console.ReadLine();
        }
    }
}
