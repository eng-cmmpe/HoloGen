﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.SLMControl.Options;
using HoloGen.SLMControl.Options.Params.Params;
using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace HoloGen.SLMControl.Converter
{
    public class ImageConverter
    {
        private readonly SLMControlOptions _options;
        private int _division;
        private int _lastUsedFileNo = 1;

        public ImageConverter(SLMControlOptions options)
        {
            _options = options;
        }

        public void Convert(out string error)
        {
            error = null;
            var extension = "png";

            // Check that the options are valid
            if (_options == null || !_options.Valid)
            {
                error = "Input options invalid!";
                return;
            }

            // Create the binariser
            var binariser = CreateBinariser();

            // Create the writer
            var writer = CreateWriter(binariser);


            // Write stuff
            Bitmap output;
            var hasContent = false;
            foreach (var inputFile in _options.SLMFilesPage.SLMControlFilesFolder.FilesToConvert.Value)
            {
                var inputImage = LoadInputImage(inputFile, out error);
                if (error != null)
                    return;

                writer.WriteImage(inputImage);
                hasContent = true;

                // Write intermediate image to file
                if (writer.ResetOutputIfFull(out output))
                {
                    var file = GetOutputImagePath(extension, out error);
                    if (error != null)
                        return;
                    try
                    {
                        output.Save(file.FullName, ImageFormat.Png);
                        hasContent = false;
                    }
                    catch (Exception)
                    {
                        error = "Unable to save output file using operating system functions.";
                        return;
                    }
                }
            }

            // Write final image to file
            if (hasContent)
            {
                output = writer.GetOutput();
                var file = GetOutputImagePath(extension, out error);
                if (error != null)
                    return;
                try
                {
                    output.Save(file.FullName, ImageFormat.Png);
                }
                catch (Exception)
                {
                    error = "Unable to save output file using operating system functions.";
                }
            }
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        private Func<byte, byte, byte, bool> CreateBinariser()
        {
            _division = _options.SLMParamsPage.SLMControlParamsFolder.BinarisationDivision.Value;
            var type = _options.SLMParamsPage.SLMControlParamsFolder.BinarisationTypeOption.Value;
            if (type is BinarisationGrayScale) return (R, G, B) => R * 30 + G * 59 + B * 11 > _division * 100;

            if (type is BinarisationMean) return (R, G, B) => R + G + B > _division * 3;

            if (type is BinarisationL2Norm) return (R, G, B) => R * R + G * G + B * B > _division * _division;

            if (type is BinarisationRed) return (R, G, B) => R > _division;

            if (type is BinarisationGreen) return (R, G, B) => G > _division;

            if (type is BinarisationBlue) return (R, G, B) => B > _division;

            Debug.Assert(false);

            // Return mean as default
            return (R, G, B) => R + G + B > _division * 3;
        }

        private SLMImageWriter CreateWriter(Func<byte, byte, byte, bool> binariser)
        {
            if (_options.SLMOutputPage.SLMControlOutputFolder.SeparateOutputImages.Value)
                return new SLMImageWriter(
                    _options.SLMParamsPage.SLMControlParamsFolder.BitRateTypeOption.Value.BitRate,
                    _options.SLMParamsPage.SLMControlParamsFolder.BitRateTypeOption.Value.BitOffset,
                    1,
                    (uint) _options.SLMParamsPage.SLMControlParamsFolder.SLMResolutionX.Value,
                    (uint) _options.SLMParamsPage.SLMControlParamsFolder.SLMResolutionY.Value,
                    (uint) _options.SLMOutputPage.SLMControlOutputFolder.SeparateOutputImages.OutputResolutionX.Value,
                    (uint) _options.SLMOutputPage.SLMControlOutputFolder.SeparateOutputImages.OutputResolutionY.Value,
                    binariser
                );
            return new SLMImageWriter(
                _options.SLMParamsPage.SLMControlParamsFolder.BitRateTypeOption.Value.BitRate,
                _options.SLMParamsPage.SLMControlParamsFolder.BitRateTypeOption.Value.BitOffset,
                _options.SLMParamsPage.SLMControlParamsFolder.BitRateTypeOption.Value.BitRate,
                (uint) _options.SLMParamsPage.SLMControlParamsFolder.SLMResolutionX.Value,
                (uint) _options.SLMParamsPage.SLMControlParamsFolder.SLMResolutionY.Value,
                (uint) _options.SLMParamsPage.SLMControlParamsFolder.SLMResolutionX.Value,
                (uint) _options.SLMParamsPage.SLMControlParamsFolder.SLMResolutionY.Value,
                binariser
            );
        }

        private Bitmap LoadInputImage(FileInfo file, out string error)
        {
            Bitmap bitmap;
            try
            {
                var image = Image.FromFile(file.FullName);
                bitmap = new Bitmap(image);
            }
            catch (Exception)
            {
                error = "Error while loading input image file!";
                return null;
            }

            if (bitmap.Size.Width != _options.SLMParamsPage.SLMControlParamsFolder.SLMResolutionX.Value ||
                bitmap.Size.Height != _options.SLMParamsPage.SLMControlParamsFolder.SLMResolutionY.Value)
            {
                error = "Input image resolution does not match the resolution specified!";
                return null;
            }

            error = null;
            return bitmap;
        }

        private FileInfo GetOutputImagePath(string extension, out string error)
        {
            var dir = _options.SLMOutputPage.SLMControlOutputFolder.OutputFolder.Value;
            var prefix = _options.SLMOutputPage.SLMControlOutputFolder.OutputName.Value;
            var overwrite = _options.SLMOutputPage.SLMControlOutputFolder.OutputOverwrite.Value;

            if (!IsFileNameValid(prefix))
            {
                error = "Invalid filename prefix!";
                return null;
            }

            FileInfo file = null;

            if (overwrite)
            {
                file = new FileInfo(dir.FullName + "\\" + prefix + _lastUsedFileNo.ToString("D5") + "." + extension);
                _lastUsedFileNo++;
            }
            else
            {
                while (file == null)
                {
                    file = new FileInfo(dir.FullName + "\\" + prefix + _lastUsedFileNo.ToString("D5") + "." + extension);
                    _lastUsedFileNo++;
                    if (file.Exists) file = null;
                }
            }

            error = null;
            return file;
        }

        private bool IsFileNameValid(string prefix)
        {
            return !string.IsNullOrEmpty(prefix) && prefix.IndexOfAny(Path.GetInvalidFileNameChars()) < 0;
        }
    }
}