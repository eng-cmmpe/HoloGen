// Copyright 2019 (C) Peter J. Christopher
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once

#include <complex>
#include <thrust/host_vector.h>
#include "../Cuda/FFTHandlerCuda.cuh"
#include "FFTHandlerWrap.h"

using namespace HoloGen::Alg::Base::Cuda;
using namespace HoloGen::Alg::Base::Export;

///
/// Execute a 2d Fourier Transform 
/// Equivalent to fftshift(fft2(fftshift(input))) in Matlab
///
std::vector<std::complex<float>> FFTHandlerWrap::FFT2WithShift(
	const std::vector<std::complex<float>>& image,
	const int slmResolutionX,
	const int slmResolutionY,
	LoggingCallback logger,
	const FFTDirectionCuda direction
)
{
	return FFTHandlerCuda::FFT2WithShift(
		image,
		slmResolutionX,
		slmResolutionY,
		logger,
		direction);
}

///
/// Execute a 2d Fourier Transform 
/// Equivalent to fft2(input) in Matlab
///
std::vector<std::complex<float>> FFTHandlerWrap::FFT2(
	const std::vector<std::complex<float>>& image,
	const int slmResolutionX,
	const int slmResolutionY,
	LoggingCallback logger,
	const FFTDirectionCuda direction
)
{
	return FFTHandlerCuda::FFT2(
		image,
		slmResolutionX,
		slmResolutionY,
		logger,
		direction);
}

///
/// Shift a Fourier Transform 
/// Equivalent to fftshift(input) in Matlab
///
std::vector<std::complex<float>> FFTHandlerWrap::FFTShift(
	const std::vector<std::complex<float>>& image,
	const int slmResolutionX,
	const int slmResolutionY,
	LoggingCallback logger
)
{
	return FFTHandlerCuda::FFTShift(
		image,
		slmResolutionX,
		slmResolutionY,
		logger);
}
