// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once

#include "QuantiserNearestCuda.cuh"

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/complex.h>
#include <cuda_runtime.h>
#include <cufft.h>
#include <thrust/random.h>
#include <limits>
#include "../Export/Globals.h"

constexpr float PI = 3.14159265359;
constexpr float PI2 = 6.28318530718;

using namespace HoloGen::Alg::Base::Cuda;
using namespace HoloGen::Alg::Base::Export;

struct quantiseDiscreteAmp
{
	const float MinSLMAmplitude;
	const float MaxSLMAmplitude;
	const int Levels;

	quantiseDiscreteAmp(const float minSLMAmplitude, const float maxSLMAmplitude, const int levels) :
		MinSLMAmplitude(minSLMAmplitude),
		MaxSLMAmplitude(maxSLMAmplitude),
		Levels(levels)
	{
		// TODO: Assert on the starting conditions
	}

	__device__ const thrust::complex<float> operator()(
		const thrust::complex<float>& input,
		const thrust::tuple<thrust::complex<float>, float>& illumination)
	{
		const auto illuminationUnitVector = thrust::get<0>(illumination);
		const auto illuminationMagnitude = thrust::get<1>(illumination);
		const auto inputMagnitude = input.real(); //sqrtf(input.imag() * input.imag() + input.real() * input.real());
		const auto minIllumination = illuminationMagnitude * MinSLMAmplitude;
		const auto maxIllumination = illuminationMagnitude * MaxSLMAmplitude;
		const auto section = (maxIllumination - minIllumination) / (Levels - 1);
		const auto discretisedMagnitude = minIllumination + section * roundf((inputMagnitude - minIllumination) / section);
		const auto cappedMagnitude = discretisedMagnitude <= minIllumination
			? minIllumination
			: discretisedMagnitude >= maxIllumination
			? maxIllumination
			: discretisedMagnitude;
		return thrust::complex<float>(
			illuminationUnitVector.real() * cappedMagnitude,
			illuminationUnitVector.imag() * cappedMagnitude);
	}
};

struct quantiseContAmp
{
	const float MinSLMAmplitude;
	const float MaxSLMAmplitude;

	quantiseContAmp(const float minSLMAmplitude, const float maxSLMAmplitude) :
		MinSLMAmplitude(minSLMAmplitude),
		MaxSLMAmplitude(maxSLMAmplitude)
	{
		// TODO: Assert on the starting conditions
	};

	__device__ const thrust::complex<float> operator()(
		const thrust::complex<float>& input,
		const thrust::tuple<thrust::complex<float>, float>& illumination)
	{
		const auto illuminationUnitVector = thrust::get<0>(illumination);
		const auto illuminationMagnitude = thrust::get<1>(illumination);
		const auto inputMagnitude = input.real(); //sqrtf(input.imag() * input.imag() + input.real() * input.real());
		const auto minIllumination = illuminationMagnitude * MinSLMAmplitude;
		const auto maxIllumination = illuminationMagnitude * MaxSLMAmplitude;
		const auto cappedMagnitude = inputMagnitude <= minIllumination
			                             ? minIllumination
			                             : inputMagnitude >= maxIllumination
			                             ? maxIllumination
			                             : inputMagnitude;
		return thrust::complex<float>(
			illuminationUnitVector.real() * cappedMagnitude,
			illuminationUnitVector.imag() * cappedMagnitude);
	}
};

struct quantiseDiscretePhase
{
	const float MinSLMAngle;
	const float MaxSLMAngle;
	const int Levels;
	const float Section;
	const float WrapMinSLMAngle;
	const float WrapMaxSLMAngle;

	quantiseDiscretePhase(const float minSLMAngle, const float maxSLMAngle, const int levels) :
		MinSLMAngle(minSLMAngle),
		MaxSLMAngle(maxSLMAngle),
		Levels(levels),
		Section((MaxSLMAngle - MinSLMAngle) / (Levels - 1)),
		WrapMinSLMAngle(maxSLMAngle + fmod((maxSLMAngle - minSLMAngle), PI2) / 2.0),
		WrapMaxSLMAngle(minSLMAngle - fmod((maxSLMAngle - minSLMAngle), PI2) / 2.0)
	{
		// TODO: Assert on the starting conditions
	};

	__device__ const thrust::complex<float> operator()(
		const thrust::complex<float>& input,
		const thrust::tuple<float, float>& illumination)
	{
		const auto illuminationAngle = thrust::get<0>(illumination);
		const auto illuminationMagnitude = thrust::get<1>(illumination);
		const auto inputAngle = Globals::Arg(input.real(), input.imag());
		auto differenceAngle = fmod(inputAngle - illuminationAngle, PI2);
		while ((differenceAngle - MaxSLMAngle) > PI2)
			differenceAngle -= PI2;
		while ((differenceAngle - MinSLMAngle) < -PI2)
			differenceAngle += PI2;
		if (differenceAngle > MaxSLMAngle) 
			return thrust::polar<float>(illuminationMagnitude, illuminationAngle + (differenceAngle < WrapMaxSLMAngle ? MaxSLMAngle : MinSLMAngle));
		if (differenceAngle < MinSLMAngle) 
			return thrust::polar<float>(illuminationMagnitude, illuminationAngle + (differenceAngle > WrapMinSLMAngle ? MinSLMAngle : MaxSLMAngle));
		const auto discretisedAngle = illuminationAngle + MinSLMAngle + Section * roundf((differenceAngle - MinSLMAngle) / Section);
		return thrust::polar<float>(illuminationMagnitude, discretisedAngle);
	}
};

struct quantiseContPhase
{
	const float MinSLMAngle;
	const float MaxSLMAngle;
	const float WrapMinSLMAngle;
	const float WrapMaxSLMAngle;

	quantiseContPhase(const float minSLMAngle, const float maxSLMAngle) :
		MinSLMAngle(minSLMAngle),
		MaxSLMAngle(maxSLMAngle),
		WrapMinSLMAngle(maxSLMAngle + fmod((maxSLMAngle - minSLMAngle), PI2) / 2.0),
		WrapMaxSLMAngle(minSLMAngle - fmod((maxSLMAngle - minSLMAngle), PI2) / 2.0)
	{
		// TODO: Assert on the starting conditions
	};

	__device__ const thrust::complex<float> operator()(
		const thrust::complex<float>& input,
		const thrust::tuple<float, float>& illumination)
	{
		const auto illuminationAngle = thrust::get<0>(illumination);
		const auto illuminationMagnitude = thrust::get<1>(illumination);
		const auto inputAngle = Globals::Arg(input.real(), input.imag());
		auto differenceAngle = fmod(inputAngle - illuminationAngle, PI2);
		while ((differenceAngle - MaxSLMAngle) > PI2)
			differenceAngle -= PI2;
		while ((differenceAngle - MinSLMAngle) < -PI2)
			differenceAngle += PI2;
		if (differenceAngle > MaxSLMAngle) 
			return thrust::polar<float>(illuminationMagnitude, illuminationAngle + (differenceAngle < WrapMaxSLMAngle ? MaxSLMAngle : MinSLMAngle));
		if (differenceAngle < MinSLMAngle) 
			return thrust::polar<float>(illuminationMagnitude, illuminationAngle + (differenceAngle > WrapMinSLMAngle ? MinSLMAngle : MaxSLMAngle));
		return thrust::polar<float>(illuminationMagnitude, differenceAngle + illuminationAngle);
	}
};

int QuantiserNearestCuda::InPlaceQuantise(
	thrust::device_vector<thrust::complex<float>>& input
)
{
	// Quantise with discrete levels
	if (Levels != std::numeric_limits<int>::max())
	{
		if (ModulationScheme == SLMModulationSchemeCuda::Amplitude)
		{
			thrust::transform(
				input.begin(),
				input.end(),
				thrust::make_zip_iterator(
					thrust::make_tuple(
						IlluminationUnitVectors->begin(),
						IlluminationMagnitudes->begin())),
				input.begin(),
				quantiseDiscreteAmp(MinSLMValue, MaxSLMValue, Levels));
		}
		else
		{
			thrust::transform(
				input.begin(),
				input.end(),
				thrust::make_zip_iterator(
					thrust::make_tuple(
						IlluminationPhases->begin(),
						IlluminationMagnitudes->begin())),
				input.begin(),
				quantiseDiscretePhase(MinSLMValue, MaxSLMValue, Levels));
		}
	}
	else
	{
		if (ModulationScheme == SLMModulationSchemeCuda::Amplitude)
		{
			thrust::transform(
				input.begin(),
				input.end(),
				thrust::make_zip_iterator(
					thrust::make_tuple(
						IlluminationUnitVectors->begin(),
						IlluminationMagnitudes->begin())),
				input.begin(),
				quantiseContAmp(MinSLMValue, MaxSLMValue));
		}
		else
		{
			thrust::transform(
				input.begin(),
				input.end(),
				thrust::make_zip_iterator(
					thrust::make_tuple(
						IlluminationPhases->begin(),
						IlluminationMagnitudes->begin())),
				input.begin(),
				quantiseContPhase(MinSLMValue, MaxSLMValue));
		}
	}

	return 0;
}
