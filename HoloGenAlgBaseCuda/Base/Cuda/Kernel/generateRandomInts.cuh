// Copyright 2019 (C) Peter J. Christopher
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once

#include <thrust/complex.h>
#include <thrust/random.h>
#include <thrust/detail/mpl/math.h>

namespace HoloGen
{
	namespace Alg
	{
		namespace Base
		{
			namespace Cuda
			{
				namespace Kernel
				{
					struct generateRandomInts
					{
						float Min;
						float Max;
						int StartingSeed;

						generateRandomInts(const float min, const float max, int startingSeed) :
							Min(min),
							Max(max),
							StartingSeed(startingSeed)
						{
						};

						__device__ int operator()(const unsigned int n)
						{
							thrust::default_random_engine rng;
							thrust::uniform_int_distribution<int> dist(Min, Max);
							rng.discard(n + StartingSeed);
							return dist(rng);
						}
					};
				}
			}
		}
	}
}
