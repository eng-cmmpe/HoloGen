// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once

#include <complex>
#include <vector>
#include <map>
#include "QuantiserCuda.cuh"
#include <functional>
#include "../Export/IlluminationTypeCuda.h"
#include "../Export/InitialSeedTypeCuda.h"
#include "../Export/LoggingCallback.h"
#include <cufft.h>
#include <thrust/complex.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include "../Export/MetricTypeCuda.h"

namespace HoloGen
{
	namespace Alg
	{
		namespace Base
		{
			namespace Cuda
			{
				using namespace HoloGen::Alg::Base::Cuda;
				using namespace HoloGen::Alg::Base::Export;

				class AlgorithmCuda
				{
				protected:

					cufftHandle Plan;

					thrust::device_vector<thrust::complex<float>> Target{};
					thrust::device_vector<thrust::complex<float>> Illumination{};
					thrust::device_vector<thrust::complex<float>> DiffractionField{};
					thrust::device_vector<thrust::complex<float>> ReplayField{};
					thrust::device_vector<int> Region{};

					// Cached decompositions of the target and illumination
					thrust::device_vector<thrust::complex<float>> TargetUnitVectors{};
					thrust::device_vector<float> TargetMagnitudes{};
					thrust::device_vector<float> TargetPhases{};

					thrust::device_vector<thrust::complex<float>> IlluminationUnitVectors{};
					thrust::device_vector<float> IlluminationMagnitudes{};
					thrust::device_vector<float> IlluminationPhases{};
					
					float TargetMagnitudeSumInRegion;

					QuantiserCuda* Quantiser = nullptr;

					int MaxIterations;
					float MaxError;
					int ReportingSteps;
					bool RandomisePhase;
					bool NormaliseTarget;
					bool TieNormalisationToZero;
					IlluminationTypeCuda IlluminationType;
					InitialSeedTypeCuda InitialSeedType;
					float IlluminationPower;

					LoggingCallback Logger = nullptr;

					// ReSharper disable once CppPossiblyUninitializedMember
					AlgorithmCuda() :
						MaxIterations(0),
						MaxError(0),
						ReportingSteps(0),
						RandomisePhase(false),
						NormaliseTarget(false),
						TieNormalisationToZero(false),
						IlluminationType(),
						InitialSeedType(),
						IlluminationPower(0),
						SLMResolutionX(0),
						SLMResolutionY(0)
					{
					}

					int RunBaseStartup();

					int RunBaseCleanup();

					std::map<MetricTypeCuda, float> GetStandardMetrics();
					
					float GetMSE();

				private:
					int BackProjectInProgressImage();

					void DecomposeTarget();
					void DecomposeIllumination();

					void RescaleTarget();

				public:

					virtual ~AlgorithmCuda()
					{
					}

					size_t SLMResolutionX;
					size_t SLMResolutionY;
					size_t NumSLMPixels;
					float NumSLMPixelsReciprocal;
					size_t NumRegionPixels;
					float NumRegionPixelsReciprocal;

					void SetBaseParameters(
						const size_t slmResolutionX,
						const size_t slmResolutionY,
						const int maxIterations,
						const float maxError,
						const int reportingSteps,
						const bool randomisePhase,
						const bool normaliseTarget,
						const bool tieNormalisationToZero,
						const IlluminationTypeCuda illuminationType,
						const InitialSeedTypeCuda initialSeedType,
						const float illuminationPower)
					{
						this->SLMResolutionX = slmResolutionX;
						this->SLMResolutionY = slmResolutionY;
						this->MaxIterations = maxIterations;
						this->MaxError = maxError;
						this->ReportingSteps = reportingSteps;
						this->RandomisePhase = randomisePhase;
						this->NormaliseTarget = normaliseTarget;
						this->TieNormalisationToZero = tieNormalisationToZero;
						this->IlluminationType = illuminationType;
						this->InitialSeedType = initialSeedType;
						this->IlluminationPower = illuminationPower;
						this->NumSLMPixels = slmResolutionX * slmResolutionY;
						this->NumSLMPixelsReciprocal = 1 / static_cast<float>(NumSLMPixels);
					}

					void __stdcall SetLogger(LoggingCallback cb);

					void SetTargetImage(const std::vector<std::complex<float>>& image);

					void SetRegionImage(const std::vector<int>& image);

					void SetIlluminationImage(const std::vector<std::complex<float>>& image);

					void SetInitialImage(const std::vector<std::complex<float>>& image);

					void SetQuantiser(QuantiserCuda* quantiser);

					std::vector<std::complex<float>> GetResult();
				};
			}
		}
	}
}
