// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/complex.h>
#include <thrust/device_ptr.h>
#include "Normaliser.cuh"
#include <cufft.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/extrema.h>
#include "../Export/Globals.h"
#include "Kernel/abs2.cuh"

using namespace HoloGen::Alg::Base::Cuda;
using namespace HoloGen::Alg::Base::Cuda::Kernel;

struct normalise
{
	//float _shift;
	float Scale;

	normalise(float scale/*, float shift*/) :
		//_shift(shift),
		Scale(scale)
	{
	};

	__device__ thrust::complex<float> operator()(const thrust::complex<float>& n)
	{
		return thrust::complex<float>(n.real() * Scale, n.imag() * Scale);
	}
};

int Normaliser::Normalise(
	thrust::device_vector<thrust::complex<float>>* input,
	const float desiredMax,
	const float desiredMin,
	LoggingCallback logger
)
{
	// Get the vector of amplitudes
	thrust::device_vector<float> absValues(input->size(), 1.0);
	thrust::transform(
		input->begin(),
		input->end(),
		absValues.begin(),
		abs2());

	// Get actual minmax values
	const auto minmax = thrust::minmax_element(
		absValues.begin(),
		absValues.end());
	const auto actualminptr = minmax.first;
	const auto actualmaxptr = minmax.second;
	const float actualmin = *actualminptr;
	const float actualmax = *actualmaxptr;

	// Calculate the scale and shift
	//const float shift = ((desiredMax + desiredMin) / 2) - ((actualmax + actualmin) / 2);
	const float scale = (desiredMax - desiredMin) / (actualmax - actualmin);

	// Apply the scale and shift
	thrust::transform(
		input->begin(),
		input->end(),
		input->begin(),
		normalise(scale/*, shift*/));

	thrust::host_vector<thrust::complex<float>> test1 = *input;

	return 0;
}
