// Copyright 2019 (C) Peter J. Christopher
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/functional.h>
#include <thrust/copy.h>

// Adapted From: https://github.com/thrust/thrust/blob/master/examples/strided_range.cu 
// and https://stackoverflow.com/questions/42230488/how-make-a-stride-chunk-iterator-thrust-cuda
// this example illustrates how to make strided-chunk access to a range of values
// examples:
//   strided_chunk_range([0, 1, 2, 3, 4, 5, 6], 1,1) -> [0, 1, 2, 3, 4, 5, 6]
//   strided_chunk_range([0, 1, 2, 3, 4, 5, 6], 2,1) -> [0, 2, 4, 6]
//   strided_chunk_range([0, 1, 2, 3, 4, 5, 6], 3,2) -> [0 ,1, 3, 4, 6]
//   ...
	// ReSharper disable CppInconsistentNaming

template <typename Iterator>
class StridedChunkRange
{
public:

	typedef typename thrust::iterator_difference<Iterator>::type difference_type;

	struct stride_functor : public thrust::unary_function<difference_type, difference_type>
	{
		difference_type stride;
		int chunk;

		stride_functor(difference_type stride, int chunk)
			: stride(stride), chunk(chunk)
		{
		}

		__host__ __device__

		difference_type operator()(const difference_type& i) const
		{
			int pos = i / chunk;
			return ((pos * stride) + (i - (pos * chunk)));
		}
	};

	typedef thrust::counting_iterator<difference_type> CountingIterator;
	typedef thrust::transform_iterator<stride_functor, CountingIterator> TransformIterator;
	typedef thrust::permutation_iterator<Iterator, TransformIterator> PermutationIterator;

	// type of the strided_range iterator
	typedef PermutationIterator iterator;

	// construct strided_range for the range [first,last)
	StridedChunkRange(Iterator first, Iterator last, difference_type stride, int chunk)
		: first(first), last(last), stride(stride), chunk(chunk)
	{
		assert(chunk<=stride);
	}

	iterator begin() const
	{
		return PermutationIterator(first, TransformIterator(CountingIterator(0), stride_functor(stride, chunk)));
	}

	iterator end() const
	{
		const int lmf = last - first;
		const int nfs = lmf / stride;
		const int rem = lmf - (nfs * stride);
		return begin() + (nfs * chunk) + ((rem < chunk) ? rem : chunk);
	}

protected:
	Iterator first;
	Iterator last;
	difference_type stride;
	int chunk;
};
	// ReSharper restore CppInconsistentNaming
