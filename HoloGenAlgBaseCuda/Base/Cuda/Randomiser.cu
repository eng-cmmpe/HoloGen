// Copyright 2019 (C) Peter J. Christopher
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/complex.h>
#include "Randomiser.cuh"
#include <cuda_runtime.h>
#include <cufft.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/random.h>
#include <random>
#include "../Export/SLMModulationSchemeCuda.h"
#include "../Export/Globals.h"

using namespace HoloGen::Alg::Base::Cuda;
using namespace HoloGen::Alg::Base::Export;

static int StartingSeed = 0;

struct switchBinaryPhase
{
	float Min;
	float Max;

	switchBinaryPhase(const float min, const float max) :
		Min(min),
		Max(max)
	{
	};

	__host__ __device__ thrust::complex<float> operator()(const unsigned int n, const thrust::complex<float>& in)
	{
		return thrust::polar<float>(thrust::abs<float>(in), Min+Max-Globals::Arg(in.real(), in.imag()));
	}
};

struct switchBinaryAmplitude
{
	float Min;
	float Max;

	switchBinaryAmplitude(const float min, const float max) :
		Min(min),
		Max(max)
	{
	};

	__host__ __device__ thrust::complex<float> operator()(const unsigned int n, const thrust::complex<float>& in)
	{
		return thrust::polar<float>(Min+Max-thrust::abs<float>(in), 0);
	}
};

struct randomisePhase
{
	float Min;
	float Max;
	int StartingSeed;

	randomisePhase(const float min, const float max, int startingSeed) :
		Min(min),
		Max(max),
		StartingSeed(startingSeed)
	{
	};

	__host__ __device__ thrust::complex<float> operator()(const unsigned int n, const thrust::complex<float>& in)
	{
		thrust::default_random_engine rng;
		thrust::uniform_real_distribution<float> dist(Min, Max);
		rng.discard(n + StartingSeed);
		return thrust::polar<float>(thrust::abs<float>(in), dist(rng));
	}
};

struct randomiseAmplitude
{
	float Min;
	float Max;
	int StartingSeed;

	randomiseAmplitude(float min, float max, int startingSeed) :
		Min(min),
		Max(max),
		StartingSeed(startingSeed)
	{
	};

	__host__ __device__ thrust::complex<float> operator()(const unsigned int n, const thrust::complex<float>& in)
	{
		thrust::default_random_engine rng;
		thrust::uniform_real_distribution<float> dist(Min, Max);
		rng.discard(n + StartingSeed);
		return thrust::complex<float>(dist(rng), 0);
	}
};

struct randomiseDiscretePhase
{
	const float Min;
	const float Max;
	const int StartingSeed;
	const int Levels;
	const float Section;
	const float OneOverSection;

	randomiseDiscretePhase(const float min, const float max, int startingSeed, int levels) :
		Min(min),
		Max(max),
		StartingSeed(startingSeed),
		Levels(levels),
		Section((max - min) / (Levels - 1)),
		OneOverSection(1 / Section)
	{
	};

	__host__ __device__ thrust::complex<float> operator()(const unsigned int n, const thrust::complex<float>& in)
	{
		thrust::default_random_engine rng;
		thrust::uniform_real_distribution<float> dist(Min, Max);
		rng.discard(n + StartingSeed);
		const auto discretisedValue = Min + Section * roundf((dist(rng) - Min) * OneOverSection);
		return thrust::polar<float>(thrust::abs<float>(in), discretisedValue);
	}
};

struct randomiseDiscreteAmplitude
{
	const float Min;
	const float Max;
	const int StartingSeed;
	const int Levels;
	const float Section;
	const float OneOverSection;

	randomiseDiscreteAmplitude(const float min, const float max, int startingSeed, int levels) :
		Min(min),
		Max(max),
		StartingSeed(startingSeed),
		Levels(levels),
		Section((max - min) / (Levels - 1)),
		OneOverSection(1 / Section)
	{
	};

	__host__ __device__ thrust::complex<float> operator()(const unsigned int n, const thrust::complex<float>& in)
	{
		thrust::default_random_engine rng;
		thrust::uniform_real_distribution<float> dist(Min, Max);
		rng.discard(n + StartingSeed);
		const auto discretisedValue = Min + Section * roundf((dist(rng) - Min) * OneOverSection);
		return thrust::polar<float>(discretisedValue, 0);
	}
};

int Randomiser::RandomisePhase(
	thrust::device_vector<thrust::complex<float>>& input,
	const float min,
	const float max
)
{
	// Randomise the phase of the input vector
	const thrust::counting_iterator<unsigned int> indexSequence(0);
	thrust::transform(
		indexSequence,
		indexSequence + input.size(),
		input.begin(),
		input.begin(),
		randomisePhase(min, max, StartingSeed));
	StartingSeed += input.size();

	return 0;
}

int Randomiser::RandomAmplitude(
	thrust::device_vector<thrust::complex<float>>& input,
	const float min,
	const float max
)
{
	// Randomise the phase of the input vector
	const thrust::counting_iterator<unsigned int> indexSequence(0);
	thrust::transform(
		indexSequence,
		indexSequence + input.size(),
		input.begin(),
		input.begin(),
		randomiseAmplitude(min, max, StartingSeed));
	StartingSeed += input.size();

	return 0;
}

int Randomiser::RandomiseSelectedElements(
	ComplexPermutationIterator& input,
	const int size,
	const SLMModulationSchemeCuda modulationScheme,
	const float maxSLMValue,
	const float minSLMValue,
	const int levels)
{
	if (levels == 2)
	{
		if (modulationScheme == SLMModulationSchemeCuda::Amplitude)
		{
			const thrust::counting_iterator<unsigned int> indexSequence(0);
			thrust::transform(
				indexSequence,
				indexSequence + size,
				input,
				input,
				switchBinaryAmplitude(minSLMValue, maxSLMValue));
		}
		else
		{
			const thrust::counting_iterator<unsigned int> indexSequence(0);
			thrust::transform(
				indexSequence,
				indexSequence + size,
				input,
				input,
				switchBinaryPhase(minSLMValue, maxSLMValue));
		}
	}
	else if (levels != std::numeric_limits<int>::max())
	{
		if (modulationScheme == SLMModulationSchemeCuda::Amplitude)
		{
			const thrust::counting_iterator<unsigned int> indexSequence(0);
			thrust::transform(
				indexSequence,
				indexSequence + size,
				input,
				input,
				randomiseDiscreteAmplitude(minSLMValue, maxSLMValue, StartingSeed, levels));
		}
		else
		{
			const thrust::counting_iterator<unsigned int> indexSequence(0);
			thrust::transform(
				indexSequence,
				indexSequence + size,
				input,
				input,
				randomiseDiscretePhase(minSLMValue, maxSLMValue, StartingSeed, levels));
		}
	}
	else
	{
		if (modulationScheme == SLMModulationSchemeCuda::Amplitude)
		{
			const thrust::counting_iterator<unsigned int> indexSequence(0);
			thrust::transform(
				indexSequence,
				indexSequence + size,
				input,
				input,
				randomiseAmplitude(minSLMValue, maxSLMValue, StartingSeed));
		}
		else
		{
			const thrust::counting_iterator<unsigned int> indexSequence(0);
			thrust::transform(
				indexSequence,
				indexSequence + size,
				input,
				input,
				randomisePhase(minSLMValue, maxSLMValue, StartingSeed));
		}
	}
	
	StartingSeed += size;
	return 0;
}

thrust::complex<float> Randomiser::RandomiseSelectedElement(
	const thrust::complex<float>& input,
	const SLMModulationSchemeCuda modulationScheme,
	const float maxSLMValue,
	const float minSLMValue,
	const int levels)
{
	if (levels == 2)
	{
		if (modulationScheme == SLMModulationSchemeCuda::Amplitude)
		{
			return switchBinaryAmplitude(minSLMValue, maxSLMValue).operator()(1, input);
		}
		else
		{
			return switchBinaryPhase(minSLMValue, maxSLMValue).operator()(1, input);
		}
	}
	else if (levels != std::numeric_limits<int>::max())
	{
		if (modulationScheme == SLMModulationSchemeCuda::Amplitude)
		{
			return randomiseDiscreteAmplitude(minSLMValue, maxSLMValue, StartingSeed++, levels).operator()(1, input);
		}
		else
		{
			return randomiseDiscretePhase(minSLMValue, maxSLMValue, StartingSeed++, levels).operator()(1, input);
		}
	}
	else
	{
		if (modulationScheme == SLMModulationSchemeCuda::Amplitude)
		{
			return randomiseAmplitude(minSLMValue, maxSLMValue, StartingSeed++).operator()(1, input);
		}
		else
		{
			return randomisePhase(minSLMValue, maxSLMValue, StartingSeed++).operator()(1, input);
		}
	}
}
