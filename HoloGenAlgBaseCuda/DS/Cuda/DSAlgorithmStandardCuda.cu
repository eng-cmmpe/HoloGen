// Copyright 2019 (C) Peter J. Christopher
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once
#include "DSAlgorithmStandardCuda.cuh"
#include <thrust/inner_product.h>
#include <thrust/transform.h>
#include <thrust/complex.h>
#include <thrust/copy.h> 
#include "../../Base/Cuda/FFTHandlerCuda.cuh"
#include "../../Base/Export/FFTScaleType.h"
#include "../../Base/Export/FFTDirectionCuda.h"
#include "../../Base/Export/Globals.h"
#include <thrust/execution_policy.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/random.h>
#include "../../Base/Cuda/Kernel/abs2.cuh"
#include "../../Base/Cuda/Kernel/generateRandomInts.cuh"
#include "../../Base/Cuda/Randomiser.cuh"

using namespace HoloGen::Alg::Base::Cuda;
using namespace HoloGen::Alg::Base::Cuda::Kernel;
using namespace HoloGen::Alg::DS::Cuda;
using namespace thrust::placeholders;

constexpr float TOL = 10e-8;

static int StartingSeed = 0;

struct boolToComplex {

    __device__ const thrust::complex<float> operator() (
        const int& input) {

        return input == 0 ? thrust::complex<float>(0.0, 0.0) : thrust::complex<float>(1.0, 0.0);
    }
};

DSAlgorithmStandardCuda::DSAlgorithmStandardCuda() : PreviousMSE(0),
                                                     NumPixelsToSwitch(0)
{
}

int DSAlgorithmStandardCuda::RunAlgorithmStartup()
{
    Globals::Log(Logger, "Beginning variant-level setup...");

    auto result = RunBaseStartup();
    if (result != 0) return result;

    // Warning: Be careful about extracting bits of this into the parent class. 
    // The dynamic linking required by the Managed C++/Cuda C interface means that
    // whole program optimisation is limited over library boundaries. Extracting
    // operations like data copying may result in a reduction in efficiency.
    // For more info, we compile with -rdc=true and -dlink

    // Plan the FFT operation
    result = FFTHandlerCuda::PlanFFT2(Plan, SLMResolutionX, SLMResolutionY, Logger);
    if (result != 0) return result;

    // Set the previous MSE value
    PreviousMSE = std::numeric_limits<float>::max();

    // Set the previous MSE value
    PreviousMSE = std::numeric_limits<float>::max();
    
    // Quantise the image because we don't do it during the iterations
    result = Quantiser->InPlaceQuantise(DiffractionField);
    if (result != 0)
    {
        Globals::Log(Logger, "ERROR during quantisation!");
        return 1;
    }
    
    // Forward project the in progress image, scaling the result to account for the inherant fft scaling
    result = FFTHandlerCuda::FFT2WithShift(
        Plan,
        DiffractionField,
        ReplayField,
        SLMResolutionX,
        SLMResolutionY,
        Logger,
        FFTDirectionCuda::Forward,
        FFTScaleType::ScaleResult);
    if (result != 0)
    {
        Globals::Log(Logger, "ERROR during forward FFT!");
        return result;
    }

    // Return success indicator
    return 0;
}

std::map<MetricTypeCuda, float> DSAlgorithmStandardCuda::RunIterations()
{
    Globals::Log(Logger, "Running algorithm iterations...");

    std::map<MetricTypeCuda, float> metrics;

    // Warning: Be careful about extracting bits of this into the parent class. 
    // The dynamic linking required by the Managed C++/Cuda C interface means that
    // whole program optimisation is limited over library boundaries. Extracting
    // operations like data copying may result in a reduction in efficiency.

    // Debug messages like this:
    // Globals::DebugLog(iteration, Logger, "Post Quant", XXX->front());

    thrust::device_vector<thrust::complex<float>> cachedPixels(NumPixelsToSwitch);
    thrust::device_vector<int> indices(NumPixelsToSwitch);
    const thrust::counting_iterator<unsigned int> indexSequence(0);

    try
    {
        int result;
        for (auto iteration = 0; iteration < this->ReportingSteps; ++iteration)
        {
            // Select, cache and modify random elements
            thrust::transform(
                indexSequence,
                indexSequence + NumPixelsToSwitch,
                indices.begin(),
                generateRandomInts(0, DiffractionField.size() - 1, StartingSeed));
            auto randomElements = thrust::make_permutation_iterator(DiffractionField.begin(), indices.begin());
            thrust::copy_n(randomElements, NumPixelsToSwitch, cachedPixels.begin());
            Randomiser::RandomiseSelectedElements(randomElements, NumPixelsToSwitch, Quantiser->ModulationScheme, Quantiser->MaxSLMValue, Quantiser->MinSLMValue, Quantiser->Levels);
            
            // Forward project the in progress image, scaling the result to account for the inherant fft scaling
            result = FFTHandlerCuda::FFT2WithShift(
                Plan,
                DiffractionField,
                ReplayField,
                SLMResolutionX,
                SLMResolutionY,
                Logger,
                FFTDirectionCuda::Forward,
                FFTScaleType::ScaleResult);

            // Calculate MSE
            const float MSE = GetMSE();

            // If the MSE is worse, swap our cached values back in
            if (MSE >= PreviousMSE)
            {
                thrust::copy_n(cachedPixels.begin(), NumPixelsToSwitch, randomElements);
            }
            else
            {
                PreviousMSE = MSE;
            }
        }

        // Get the error metrics
        return GetStandardMetrics();
    }
    catch (...)
    {
        Globals::Log(Logger, "ERROR in variant level algorithm!");
        return metrics;
    }

}

int DSAlgorithmStandardCuda::RunAlgorithmCleanup()
{

    Globals::Log(Logger, "Beginning variant-level cleanup...");

    auto result = RunBaseCleanup();
    result &= FFTHandlerCuda::CleanUp(Plan);
    return result;
}
