// Copyright 2019 (C) Peter J. Christopher
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

#pragma once
#include "GSAlgorithmStandardCuda.cuh"
#include <thrust/transform.h>
#include <thrust/complex.h>
#include <thrust/copy.h> 
#include "../../Base/Cuda/FFTHandlerCuda.cuh"
#include "../../Base/Export/FFTScaleType.h"
#include "../../Base/Export/FFTDirectionCuda.h"
#include "../../Base/Export/Globals.h"
#include <thrust/execution_policy.h>

using namespace HoloGen::Alg::Base::Cuda;
using namespace HoloGen::Alg::GS::Cuda;
using namespace thrust::placeholders;

constexpr float TOL = 10e-8;

// Combine target amplitude and reconstruction phase for elements that are in the 
// region of interest, otherwise leaving them as is
struct combineTargetAmplitudeAndReconstructionPhase
{
	__device__ thrust::complex<float> operator() (
		const int& inRegion,
		const thrust::tuple<thrust::complex<float>, float>& values)
	{
		const auto reconstuction = thrust::get<0>(values);
		if (inRegion == 0)
			return reconstuction;
		const auto targetMagnitude = thrust::get<1>(values);
		const auto reconstructionPhase = Globals::Arg(reconstuction.real(), reconstuction.imag());
		return thrust::polar<float>(targetMagnitude, reconstructionPhase);
	}
};

GSAlgorithmStandardCuda::GSAlgorithmStandardCuda() : 
DummyValue(0)
{
}

int GSAlgorithmStandardCuda::RunAlgorithmStartup()
{
	Globals::Log(Logger, "Beginning variant-level setup...");

	auto result = RunBaseStartup();
	if (result != 0) return result;

	// Warning: Be careful about extracting bits of this into the parent class. 
	// The dynamic linking required by the Managed C++/Cuda C interface means that
	// whole program optimisation is limited over library boundaries. Extracting
	// operations like data copying may result in a reduction in efficiency.
	// For more info, we compile with -rdc=true and -dlink

	// Plan the FFT operation
	result = FFTHandlerCuda::PlanFFT2(Plan, SLMResolutionX, SLMResolutionY, Logger);
	if (result != 0) return result;

	
	// Forward project the in progress image, scaling the result to account for the inherant fft scaling
	result = FFTHandlerCuda::FFT2WithShift(
		Plan,
		DiffractionField,
		ReplayField,
		SLMResolutionX,
		SLMResolutionY,
		Logger,
		FFTDirectionCuda::Forward,
		FFTScaleType::ScaleResult);
	if (result != 0)
	{
		Globals::Log(Logger, "ERROR during forward FFT!");
		return result;
	}

	// Return success indicator
	return 0;
}

std::map<MetricTypeCuda, float> GSAlgorithmStandardCuda::RunIterations()
{
	Globals::Log(Logger, "Running algorithm iterations...");

	std::map<MetricTypeCuda, float> metrics;

	// Warning: Be careful about extracting bits of this into the parent class. 
	// The dynamic linking required by the Managed C++/Cuda C interface means that
	// whole program optimisation is limited over library boundaries. Extracting
	// operations like data copying may result in a reduction in efficiency.

	// Debug messages like this:
	// Globals::DebugLog(iteration, Logger, "Post Quant", XXX->front());

	try
	{
		int result;
		for (auto iteration = 0; iteration < this->ReportingSteps; ++iteration)
		{

			// Create best guess image
			thrust::transform(
				Region.begin(),
				Region.end(),
				thrust::make_zip_iterator(
					thrust::make_tuple(
						ReplayField.begin(),
						TargetMagnitudes.begin())),
				ReplayField.begin(),
				combineTargetAmplitudeAndReconstructionPhase());

			// Back project
			result = FFTHandlerCuda::FFT2WithShift(
				Plan,
				ReplayField,
				DiffractionField,
				SLMResolutionX,
				SLMResolutionY,
				Logger,
				FFTDirectionCuda::Inverse,
				FFTScaleType::ScaleResult);
			if (result != 0)
			{
				Globals::Log(Logger, "ERROR during inverse FFT!");
				return metrics;
			}

			// Quantise the image
			result = Quantiser->InPlaceQuantise(DiffractionField);
			if (result != 0)
			{
				Globals::Log(Logger, "ERROR during quantisation!");
				return metrics;
			}

			// Forward project the in progress image, scaling the result to account for the inherant fft scaling
			result = FFTHandlerCuda::FFT2WithShift(
				Plan,
				DiffractionField,
				ReplayField,
				SLMResolutionX,
				SLMResolutionY,
				Logger,
				FFTDirectionCuda::Forward,
				FFTScaleType::ScaleResult);
			if (result != 0)
			{
				Globals::Log(Logger, "ERROR during forward FFT!");
				return metrics;
			}
		}

		// Get the error metrics
		return GetStandardMetrics();
	}
	catch (...)
	{
		Globals::Log(Logger, "ERROR in variant level algorithm!");
		return metrics;
	}

}

int GSAlgorithmStandardCuda::RunAlgorithmCleanup()
{

	Globals::Log(Logger, "Beginning variant-level cleanup...");

	auto result = RunBaseCleanup();
	result &= FFTHandlerCuda::CleanUp(Plan);
	return result;
}
