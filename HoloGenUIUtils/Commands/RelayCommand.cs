﻿// Copyright 2019 Peter J. Christopher
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, 
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to the following 
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using System;
using System.Windows.Input;

namespace HoloGen.UI.Utils.Commands
{
    /// <summary>
    /// Command that can handle an async connection. Necessary for interaction with the process framework.
    /// From: https://cisart.wordpress.com/2013/10/10/wpf-commands-and-async-commands/
    /// </summary>
    public class RelayCommand : ICommand
    {
        protected readonly Func<bool> CanExecuteLambda;

        protected readonly Action ExecuteLambda;

        public RelayCommand(Action executeLambda, Func<bool> canExecuteLambda)
        {
            ExecuteLambda = executeLambda ?? throw new ArgumentNullException(nameof(executeLambda));
            CanExecuteLambda = canExecuteLambda;
        }

        public RelayCommand(Action execute)
            : this(execute, null)
        {
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                if (CanExecuteLambda != null) CommandManager.RequerySuggested += value;
            }

            remove
            {
                if (CanExecuteLambda != null) CommandManager.RequerySuggested -= value;
            }
        }

        public virtual bool CanExecute(object parameter)
        {
            return CanExecuteLambda == null || CanExecuteLambda();
        }

        public virtual void Execute(object parameter)
        {
            ExecuteLambda();
        }
    }
}